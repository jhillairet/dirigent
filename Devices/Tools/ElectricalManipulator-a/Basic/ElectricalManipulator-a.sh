#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh


function Wake-upCall()
{
    LogIt $ThisDev
    shuf -i 0-5|head -1|xargs sleep # To Protect electrical grid
    echo "*B1OS4H"|telnet 192.168.2.254 10001 1>&- 2>&-; #F33
    sleep 10
    python3 manipulator_control.py wake
    
}

function Sleep-downCall()
{
    LogIt $ThisDev
    python3 manipulator_control.py home
    echo "*B1OS4L"|telnet 192.168.2.254 10001 1>&- 2>&-; #F33

}


function SetPosition()
{
    LogIt $ThisDev
    python3 manipulator_control.py $1
}

function Homing()
{
    LogIt $ThisDev
    python3 manipulator_control.py home
}

function PingCheck ()
{
    WaitForDevice D-Linkcamera-a $ThisDev
    WaitForDevice D-Linkcamera-b $ThisDev
    WaitForDevice Motor_Controller-a $ThisDev
    WaitForDevice Motor_Controller-b $ThisDev
    

}

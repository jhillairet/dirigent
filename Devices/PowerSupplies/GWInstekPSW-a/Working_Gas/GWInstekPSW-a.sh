#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh


function GasFlowSetup ()
{
local Voltage=$1

    echo -ne $Voltage > $SHML/ActualVoltageAtGasValve
    echo "APPL $Voltage,2;OUTPut:IMMediate ON"|netcat -w 1 $ThisDev 2268
} 

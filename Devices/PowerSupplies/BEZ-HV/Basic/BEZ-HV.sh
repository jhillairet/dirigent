#!/bin/bash


BasePath=../../../..;source $BasePath/Commons.sh

Quido="RelayBoards/Quido8-a/SwBo_VaccGdHV"
DeviceList=$Quido

function PowerON()
{
    Call $SW/Devices/$Quido PowerSupplyHVcontactorON
}

function PowerOFF()
{
    Call $SW/Devices/$Quido PowerSupplyHVcontactorOFF
} 

function ShortCircuitON()
{
    Call $SW/Devices/$Quido LowerShortCircuitsEngage
}

function ShortCircuitOFF()
{
    Call $SW/Devices/$Quido LowerShortCircuitsDisEngage
} 

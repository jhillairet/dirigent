#!/bin/bash

RelayBoard="Devices/RelayBoards/Quido16-a/Racks_Vacuum"

DeviceList="../$RelayBoard"
BasePath=../../../..;source $BasePath/Commons.sh

PUMP_id=b


function PumpingON
{
    LogIt "engaging TMP-$PUMP_id pump"
    Call $SW/$RelayBoard TMP-"$PUMP_id"ON
}

function PumpingOFF
{
    LogIt "disengaging TMP-$PUMP_id pump"
    Call $SW/$RelayBoard TMP-"$PUMP_id"OFF
}

function StandbyON
{
    LogIt "Stand by TMP-$PUMP_id pump"
    Call $SW/$RelayBoard TMP-"$PUMP_id"StandbyON
}

function StandbyOFF
{
    LogIt "Stand by TMP-$PUMP_id pump OFF"
    Call $SW/$RelayBoard TMP-"$PUMP_id"StandbyOFF
}

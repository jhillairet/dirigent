#!/bin/bash

DeviceList="RelayBoards/Quido16-a/Racks_Vacuum"
BasePath=../../../..;source $BasePath/Commons.sh

function OpenValves
{
    LogIt "Opening Valves-b"
    Call $SW/Devices/RelayBoards/Quido16-a/Racks_Vacuum Valves-bON
}

function CloseValves
{
    LogIt "Closing Valves-b"
    Call $SW/Devices/RelayBoards/Quido16-a/Racks_Vacuum Valves-bOFF
}


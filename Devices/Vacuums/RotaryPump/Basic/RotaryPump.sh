#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh

function PumpingON
{
    LogIt "engaging rotary pump"
    Call $SW/Devices/RelayBoards/Quido8-a/SwBo_VaccGdHV/ RotPumpON
}

function PumpingOFF
{
    LogIt "disengaging rotary pump"
    Call $SW/Devices/RelayBoards/Quido8-a/SwBo_VaccGdHV/ RotPumpOFF
}


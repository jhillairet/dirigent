#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh

function Wake-upCall()
{
    LogIt $ThisDev
    shuf -i 0-5|head -1|xargs sleep # To Protect electrical grid
    Call $SW/Infrastructure/PositionStabilization/Support/KepcosSocketSwitch/ Engage
    sleep 2
    Call $SW/Infrastructure/PositionStabilization/Support/KepcosSocket/ KepcosON
}

function Sleep-downCall()
{
    LogIt $ThisDev
    Call $SW/Infrastructure/PositionStabilization/Support/KepcosSocket/ KepcosOFF
    sleep 1
    Call $SW/Infrastructure/PositionStabilization/Support/KepcosSocketSwitch/ DisEngage
 
}

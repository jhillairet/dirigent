#!/bin/bash

source /dev/shm/golem/Commons.sh


whoami="Devices/DASs/NIstandard/20_Position_DK"
#ThisDev=`dirname $whoami|xargs basename`
ThisDev=NIstandard
DataFile=NIdata_6133.lvm

source Universals.sh


data_dir=$SHM0/`dirname $whoami`



function RawDataAcquiring()
{

   timeout=20
    while ssh golem_daq@$ThisDev.golem "! test -s /home/golem_daq/$DataFile";
    do
        if [ "$timeout" == 0 ]; then
        echo "ERROR: Timeout while waiting for the file from $ThisDev"
        echo scp -o ConnectTimeout=1 golem_daq@$ThisDev.golem:/home/golem_daq/$DataFile .; \
        exit 1
    fi
    sleep 1
    echo $timeout s to wait for $ThisDev files
    ((timeout--))
    done
    
    local shot_no=`cat $SHMS/shot_no`
    LogIt "$ThisDev: Start of acquiring"
    scp -o ConnectTimeout=1 golem_daq@$ThisDev.golem:/home/golem_daq/$DataFile .; \
	sed 's/,/\./g' $DataFile > Nidatap_6133.lvm; 
	#rm -f $DataFile; \

	
    echo "set terminal png;set output 'ScreenShotAll.png';set bmargin 0;set tmargin 0;set lmargin 10;set rmargin 3;unset xtics;unset xlabel;set multiplot layout 4, 1 title 'GOLEM Shot $shot_no';set xrange [*:*];set yrange [*:*];set style data dots;set ylabel 'ch1';unset ytics;plot '$DataFile' u 1:2 t '' w l lc 1 ;set bmargin 0;set tmargin 0;unset title;set ylabel 'ch2';plot '$DataFile'  u 1:3  w l lc 2 title '';set ylabel 'ch3';plot '$DataFile'  u 1:4  w l lc 3 title '';;set ylabel 'ch4';plot '$DataFile'  u 1:5  w l lc 4 title ''" | gnuplot
    	convert -resize 200x200 ScreenShotAll.png rawdata.jpg

    LogIt "$ThisDev: End of acquiring"

}






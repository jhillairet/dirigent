#!/bin/bash


# requires the pydcpf library, install in anaconda
# pip install https://github.com/smartass101/pydcpf/archive/py3.zip

#controller="/home/golem/anaconda3/bin/python /dev/shm/golem/ActualSession/Devices/DASs/Drivers/PapouchDAS1210/controller.py"

controller="/home/golem/anaconda3/bin/python /golem/Dirigent/Devices/DASs/Drivers/PapouchDAS1210/controller.py"

# example: arm_papouch_das 192.168.2.221
function arm_papouch_das() {
    ipaddr=$1
    $controller set_ready $ipaddr
}

# example: read_channel_papouch_das 192.168.2.221 1 out.csv 40e-3
function read_channel_papouch_das() {
    ipaddr=$1
    channel=$2
    output_file=$3
    time_length=$4              # [s]
    $controller get_data_calibrated $ipaddr $channel $output_file $time_length
}

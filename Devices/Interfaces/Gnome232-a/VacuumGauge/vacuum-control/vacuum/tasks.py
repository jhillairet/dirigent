from . import lib
import time



def start_pumping():
    # safety check
    pch = lib.pressure_chamber()
    if pch > 1000:
        lib.write2log("High pressure in chamber = {}".format(pch))
        return False

    lib.write2log("Safety check for open vessel PASSED")

    # Turn on rotary pump
    lib.gpio_low(17)
    lib.write2log("Rotary pump ON")

    # Wait for sufficiently low forvacuum pressure
    while True:
        pf = lib.pressure_forvacuum()
        if pf > 100:
            break
        lib.write2log("Waiting for low pressure in Forvacuum = {}".format(pf))
        time.sleep(1)

    # turn on TMP 1,2
    lib.gpio_low(5)
    lib.gpio_low(13)

    lib.write2log("TMP 1 ON")
    lib.write2log("TMP 2 ON")

    time.sleep(30)

    # open vacuum valves
    lib.gpio_low(4)
    lib.gpio_low(12)

    lib.write2log("Valve 1 OPEN")
    lib.write2log("Valve 2 OPEN")
    return True


def stop_pumping():
    lib.gpio_high(4)
    lib.gpio_high(12)

    time.sleep(5)

    lib.gpio_high(5)
    lib.gpio_high(13)

    # needs pump valve closing!
    #lib.gpio_high(17)
    return True


def status():
    state = {}
    state['pressure_chamber_raw'], state['pressure_chamber'] = lib.pressure_chamber(return_raw=True)
    state['pressure_forvacuum_raw'], state['pressure_forvacuum'] = lib.pressure_forvacuum(return_raw=True)
    lib.write2log('{pressure_forvacuum_raw:d}  {pressure_chamber_raw:d}  {pressure_forvacuum:5.0f}  {pressure_chamber:8.4f}'.format(**state))
    return state


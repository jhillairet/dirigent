#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh
source ../Universals.sh

COMMAND="netcat -w 1 $ThisDev 5555"
scope_address="nc -w 1 $ThisDev 5555"

diags=("" "U_loop" "U_Bt" "U_Ecd" "Trigger")


# e.g. echo ":SINGLe"|nc -w 1 147.32.4.87 5555


function OpenSessionZMB()
{
   echo "
CHANnel1:DISPlay ON;CHANnel1:PROBe 1;CHANnel1:SCALe 3;CHANnel1:OFFSet -10;
CHANnel2:DISPlay ON;CHANnel2:PROBe 1;CHANnel2:SCALe 10;CHANnel2:OFFSet 0;
CHANnel3:DISPlay ON;CHANnel3:PROBe 1;CHANnel3:SCALe 10;CHANnel3:OFFSet 0;
CHANnel4:DISPlay ON;CHANnel4:PROBe 1;CHANnel4:SCALe 0.2;CHANnel4:OFFSet 0;
TIMebase:DELay:ENABle OFF;TIMebase:MAIN:SCALe 2e-3;TIMebase:MAIN:OFFSet 9e-3;
:STOP;:CLEAR;
:SYSTem:KEY:PRESs MOFF
:TRIGger:EDGE:SOURce CHANnel1;TRIGger:EDGE:SLOPe POS;TRIGger:EDGE:LEVel 6;
"|$scope_address

}


function ArmingZMB()
{
	echo ":SINGLe"|$scope_address
}



function RawDataAcquiringZMB()
{
    GetOscScreenShot 
    convert -resize $icon_size ScreenShotAll.png rawdata.jpg
    getdata
 
}

function GetOscScreenShot()
{
	echo ":SYSTem:KEY:PRESs MOFF"|$COMMAND
	mRelax
    #echo ":DISPLAY:DATA?  ON,OFF,PNG"|$COMMAND|tail -c +12 > $SHM0/$SUBDIR/$ThisDev/ScreenShot.png
    echo ":DISPLAY:SNAP? png"|netcat -w 4 $ThisDev 5555|tail -c +12 > ScreenShotAll.png
    convert -resize 200x200 ScreenShotAll.png rawdata.jpg

    #echo ":DISPLAY:DATA?  ON,OFF,PNG"|$COMMAND
}

function getdataZMB()
{
    CommonGetData

}


#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh


function Wake-upCall()
{ 
    Call $SW/Infrastructure/Support/Sockets/Tokamak InterferometryON
} 

 
function Sleep-downCall()
{ 
    Call $SW/Infrastructure/Support/Sockets/Tokamak InterferometryOFF
}


function OpenSession()
{
#    PrepareFilesToSHMs $SHMS Devices/`dirname $Drivers`
 
    wget  --quiet http://TektrDPO3014-a/download.cgi?"command=:CLEARmenu" -O /dev/null 2>/dev/null &
 
}


function Arming()
{
	wget http://TektrDPO3014-a/download.cgi?"command=:FPANEL:PRESS+SINGLESEQ" -O /dev/null 2>log
}


function RawDataAcquiring()
{
    getdata
     GetOscScreenShot

}
    

function getdata ()
{

CHANNELS="4 3 1"

    for channel in $CHANNELS;do
        wget -T2 http://TektrDPO3014-a/download.cgi?"command=select:ch$channel on" -O /dev/null 2>>log
        wget -T2 http://TektrDPO3014-a/download.cgi?"command=save:waveform:fileformat internal" -O /dev/null 2>>log
        echo "TektronixDPO3014: Downloading data for channel $channel"
        wget -T2 http://TektrDPO3014-a/download.cgi?'wfmsend=Get' -O ch$channel.isf 2>>log
    done;\


}

function GetOscScreenShot()
{
    wget  --quiet http://TektrDPO3014-a/download.cgi?"command=:CLEARmenu" -O /dev/null 2>/dev/null &

	wget 'http://TektrDPO3014-a/image.png' -O ScreenShotAll.png 
	convert -resize 200x200 ScreenShotAll.png rawdata.jpg
}






#!/bin/bash


#ThisDev=TektrMSO56-a
#COMMAND="netcat -q 1 $instrument.golem 4000"
COMMAND="netcat -q 1 $ThisDev.golem 4000" 
Drivers="Oscilloscopes/Drivers/TektrMSO5/driver"


   
function PrepareSessionEnv@SHM()
{
    mkdir -p $SHMS/Devices/Oscilloscopes/Drivers/TektrMSO5/
    cp $SW/Devices/Oscilloscopes/Drivers/TektrMSO5/* $SHMS/Devices/Oscilloscopes/Drivers/TektrMSO5/
}   


function ExternDataAvailabilityTest()
{
    
    $LogFunctionGoingThrough
    CountDown 10 "Waiting for ExternDataAvailabilityTest@$ThisDev ... Arming "
    Arming
    CountDown 4 "Waiting for ExternDataAvailabilityTest@$ThisDev ... ForceTrig "
    ForceTrig
    CountDown 5 "Waiting for ExternDataAvailabilityTest@$ThisDev ... Data available? "
    if ! ls $Tek_mount_path/TektrMSO56*.* > /dev/null 2>&1; then 
    critical_error "$ThisDev mount problem ... 192.168.2.116, tek_drop, golem, tokamak";fi
}


function Arming()
{
    LogTheDeviceAction

    rm -f /home/golem/tektronix_drop/TektrMSO56*.csv
    rm -f /home/golem/tektronix_drop/TektrMSO56*.png
    #mkdir -p $SHM0/$SUBDIR/$ThisDev/
    echo ":DISplay:GLObal:CH1:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH2:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH3:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH4:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH5:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH6:STATE ON"|$COMMAND
    SingleSeq
}


function SingleSeq()
{
    echo "FPANEL:PRESS SINGLESEQ"|$COMMAND
}   


function ForceTrig()
{
    echo "FPANEL:PRESS FORCetrig"|$COMMAND
}


# MOUNT: 192.168.2.116, tek_drop, golem, tokamak
# systemctl restart smbd
# Dat pozor, na ktery disk se mapuje .. O ci L

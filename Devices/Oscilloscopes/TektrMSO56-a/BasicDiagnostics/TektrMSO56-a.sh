#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh

source ../Universals.sh


function OpenSession()
{

    tries=30;#Is it accessible SCPI protocol?
    while [ "$tries" -gt 0 ] && ! echo "IDN?"|$COMMAND;do tries=$(( tries - 1 ));LogIt "Waiting for IDN@$ThisDev";Relax;done
    if [ "$tries" -eq 0 ]; then
    LogIt 'failed to receive IDN@$ThisDev'
    Speaker Problems/A-problem-Failed-to-connect-Basic-diagnostics
    return
    fi
    Relax
    echo "RECALL:SETUP 'StandardDAS.set';
    :ACQUIRE:MODE HIRes;
    :HORIZONTAL:MODE:MANUAL;
    :HORIZONTAL:MODE:MANUAL:CONFIGURE RECORDLENGTH;
    :HORIZONTAL:MODE MANUAL; 
    :HORIZONTAL:MODE:SAMPLERATE 1e6;
    :HORIZONTAL:MODE:SCALE 2.4e-3;
    :HORIZONTAL:POSITION 3 ;
    :SAVEON:WAVEFORM:FILEFORMAT SPREADSheet;
    :SAVEon:FILE:DEST 'L:/';
    :SAVEON:WAVEform ON;
    :SAVEon:TRIG ON;
    :SAVEON:IMAGE ON;
    :SAVEON:FILE:NAME 'TektrMSO56';
    :SAVEON:WAVEform:SOURCE ALL"|$COMMAND
        
    for i in `seq 1 6`; do 
        echo ":DISplay:GLObal:CH$i:STATE ON;
        :CH$i:BANDWIDTH 200e3"|$COMMAND
    done
    
    echo ":CH1:SCALE 5;:CH1:OFFSET 20;
    :CH2:SCALE 130e-3;:CH2:OFFSET 520e-3;
    :CH3:SCALE 100e-3;:CH3:OFFSET 0;
    :CH4:SCALE 200e-3;:CH4:OFFSET 200e-3;
    :CH5:SCALE 1;:CH5:OFFSET 0;
    :CH6:SCALE 750e-3;:CH6:OFFSET 3000e-3;
    
    CH1:LABel:NAME 'U_loop,coil';
    CH2:LABel:NAME 'U_Bt,coil';
    CH3:LABel:NAME 'U_Rog,coil';
    CH4:LABel:NAME 'U_Photod';
    CH5:LABel:NAME 'U_Diamagn';
    CH6:LABel:NAME 'Trigger';
    
    FPANEL:PRESS SINGLESEQ;
    TRIGGER:A:MODE NORMAL;
    TRIGGER:A:TYPE EDGE ;
    TRIGGER:A:LEVEL:CH6 4;
    TRIGGER:A:EDGE:SOURCE CH6;
    
    MATH:ADDNEW 'MATH1';
    MATH:MATH1:TYPE ADVANCED;
    MATH:MATH1:DEFine 'INTG(Ch2)*70.42';
    MATH:MATH1:LABel:NAMe 'B_t';
    MATH:MATH1:VUNIT 'T';
    :DISPLAY:WAVEVIEW1:MATH:MATH1:AUTOSCALE 1;
    
    MATH:ADDNEW 'MATH2';
    MATH:MATH2:TYPE ADVANCED;
    MATH:MATH2:DEFine '-INTG(Ch3)*5.3e6-Ch1/9.7e-3';
    MATH:MATH2:LABel:NAMe 'I_p';
    MATH:MATH2:VUNIT 'A';
    :DISPLAY:WAVEVIEW1:MATH:MATH2:AUTOSCALE 1"|$COMMAND

    ExternDataAvailabilityTest

    PrepareSessionEnv@SHM
 
    # -INTG(Ch3)*5.3e6-Ch1/9.7e-3
    # INTG(Ch2)*70.42
}


function Arming()
{
    LogTheDeviceAction

    rm -f /home/golem/tektronix_drop/TektrMSO56*.csv
    rm -f /home/golem/tektronix_drop/TektrMSO56*.png
    #mkdir -p $SHM0/$SUBDIR/$ThisDev/
    echo ":DISplay:GLObal:CH1:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH2:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH3:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH4:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH5:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH6:STATE ON"|$COMMAND
    SingleSeq
}


function BasicDiagnosticsRawDataAcquiring()
{
    getdata
#    echo  I am here RDA!!!
    echo ":DISplay:GLObal:CH2:STATE OFF"|$COMMAND
    echo ":DISplay:GLObal:CH3:STATE OFF"|$COMMAND
    #echo ":DISplay:GLObal:CH5:STATE OFF"|$COMMAND #diamagnetismus
    echo ":DISplay:GLObal:CH6:STATE OFF"|$COMMAND
    GetOscScreenShot
    convert -morphology Dilate Octagon -resize 200x200 ScreenShot.png rawdata.jpg

#    ln -s TektrMSO64-a ../StandardDAS #Back compatibility
 #   Web 
}
    

function getdata ()
{
local DataPath=/home/golem/tektronix_drop/

#echo  I am here getdata!!!
#pwd

    timeout=10
    while [ ! -f $DataPath/TektrMSO56_ALL*.csv ];
    do
        if [ "$timeout" == 0 ]; then
        echo "ERROR: Timeout while waiting for the file from $whoami"
        exit 1
    fi
    sleep 1
    echo $timeout to wait for $whoami files
    ls -all $DataPath/TektrMSO56_ALL*.csv
    ((timeout--))
    done

    ls -all $DataPath/* > ls-all
    cp `ls  -d $DataPath/TektrMSO56_ALL_*.csv |tail -n 1` TektrMSO56_ALL.csv

    cp  `ls -d $DataPath/*|grep png|grep TektrMSO56` ScreenShotAll.png


}

function GetOscScreenShot()
{
    #null: /usr/local/lib/python2.7/dist-packages/pkg_resources/py2_warn.py:22: UserWarning: Setuptools will stop working on Python 2
    /usr/bin/python -W ignore $SHMS/Devices/Oscilloscopes/Drivers/TektrMSO5/main56-a.py save_screenshot $PWD/ScreenShot.png 
}



#cd /golem/Dirigent/Devices/Oscilloscopes/TektrMSO56-a/;source BasicDiagnostics.sh;OpenSession;cd $OLDPWD



import visa



def state_translate(state_bool):
    return 'ON' if state_bool else 'OFF'

class GWInstekPSW(object):

    def __init__(self, ipaddress, port=2268):
        visa_address = 'TCPIP::{}::{}::SOCKET'.format(ipaddress, port)
        rm = visa.ResourceManager('@py')
        self.inst = rm.open_resource(visa_address)
        self.inst.read_termination = '\n'
        self.inst.write_termination = '\n'

    def get_output(self):
        ret = self.inst.query(':output?')
        return state_translate(int(ret))

    def set_output(self, state_bool):
        '''Set output state to 1 (ON) or 0 (OFF)'''
        state = state_translate(state_bool)
        self.inst.write(':output ' + state)

    def get_voltage(self):
        ret = self.inst.query(':voltage?')
        return float(ret)

    def set_voltage(self, value):
        self.inst.write(':voltage {}'.format(value)) 

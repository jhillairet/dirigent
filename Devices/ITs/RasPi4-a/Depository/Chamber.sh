#!/bin/bash

# Develop issue: In case to synchro it with Dirigent 
# RsyncDeviceFromDirigent;rsync -vr golem@Dirigent:/home/svoboda/Dirigent/Drivers/vacuum-control .; rsync -rv golem@Dirigent:/home/svoboda/Dirigent/Drivers/vacuum-control/vacuum /home/golem/ # tady je nejaky problem, nevim, uplne co plati ....

# Problem halt: Message from syslogd, Internal error: Oops: 17, maybe: https://github.com/raspberrypi/userland/issues/501

#tail -f chamber.dat |feedgnuplot  --timefmt "%H:%M:%S" --set 'format x "%H:%M"' --legend 0 "pressure" --legend 1 "temperature" --y2min 0 --y2max 200 --ymin 0 --ymax 10 --domain --lines --y2 1 --stream

SUBDIR=RASPs
ThisDev=Chamber
source actual_session.setup
source Commons.sh
source Tools.sh

QuidoModul='telnet ChamberRelays 10001'

source Drivers/GWInstekPSW/driver.sh
source Drivers/TPG262GNOME232/driver.sh
source Drivers/PapaGo2TC_ETH/driver.sh

#Quido control, e.g.:
#for i in `seq 1 16`; do  echo "*B1OS"$i"H"|telnet Quido16-a 10001; done
#i=15L;echo "*B1OS"$i|telnet Quido16-a 10001

#echo "APPL 62,2;OUTPut:IMMediate ON"|netcat -w 1 GWInstekPSW-a 2268
#echo "APPL 28,2;OUTPut:IMMediate ON"|netcat -w 1 GWInstekPSW-a 2268


function SetVoltage@GasValveTo()
{
    echo "APPL $1,2;OUTPut:IMMediate ON"|netcat -w 1 GWInstekPSW-a 2268
    echo -ne $1 > $SHML/ActualVoltageAtGasValve
}    

function SetVoltage@GasValveOFF()
{
    echo "OUTPut:IMMediate OFF"|netcat -w 1 GWInstekPSW-a 2268
    echo -ne 0 > $SHML/ActualVoltageAtGasValve
}

#In case ....
#cp /golem/database/operation/sessions/32956/ActualSession/SessionLogBook/WG_calibration_table /dev/shm/golem/ActualSession/SessionLogBook/

function H2Calibration()
{
    echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('H2 valve calibration:do','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "cat - |psql -q -U golem golem_database"
    rm -f $SHML/WG_calibration_table4H2
    rm -f $SHML/WG_calibration_table4H2
    SetVoltage@GasValveTo "0";
    sleep 5s
    GasH2ON
    #for i in `seq 15 0.4 29`; do
    for i in `seq 20 0.25 29`; do 
        SetVoltage@GasValveTo "$i";
        sleep 5s;
        echo Calibrating at $i V: `cat $SHML/ActualChamberPressuremPa` mPa
        echo $i "  "  `cat $SHML/ActualChamberPressuremPa`  >> $SHML/WG_calibration_table4H2
    done    
    SetVoltage@GasValveOFF
    Relax
    GasH2OFF

}    


function HeCalibration()
{
    echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('He valve calibration:do','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "cat - |psql -q -U golem golem_database"
    rm -f $SHML/WG_calibration_table4H2
    rm -f $SHML/WG_calibration_table4He
    SetVoltage@GasValveTo "0";
    sleep 5s
    GasHeON
    for i in `seq 5 0.4 29`; do 
        SetVoltage@GasValveTo "$i";
        sleep 5s;
        echo Calibrating at $i V: `cat $SHML/ActualChamberPressuremPa` mPa
        echo $i "  "  `cat $SHML/ActualChamberPressuremPa`  >> $SHML/WG_calibration_table4He
    done    
    SetVoltage@GasValveOFF
    Relax
    GasHeOFF

}

function xtermH2Calibration(){
    xterm -fg yellow -bg blue -title "Golem H2 calibration" -hold -e /bin/bash -l -c "H2Calibration"
    }


function sandbox ()
{
    echo "UPDATE chamber_glowdischarge SET end_time='`date +%H:%M:%S`',end_pressure=`cat $SHML/ActualChamberPressuremPa`,end_temperature=`cat $SHML/ActualChamberTemperature`  WHERE id IN(SELECT max(id) FROM chamber_glowdischarge)"|ssh Dg "cat - |psql -q -U golem golem_database"

}
    
    
function GlowDischInitiate()
{
    $LogFunctionPassing;
    GlowDischPSON
    TMP1StandbyON
    TMP2StandbyON
    Vent1OFF
    GasHeON
    echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('glow discharge:init','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "$psql_password;cat - |psql -q -U golem golem_database"
    #echo "INSERT into 	chamber_glowdischarge (date,start_time, shot_no, start_pressure, start_temperature, gass) VALUES ('`date '+%y-%m-%d'`','`date '+%H:%M:%S'`',  `cat $SHM/shot_no`, `cat $SHML/ActualChamberPressuremPa`, `cat $SHML/ActualChamberTemperature`, 'He') "|ssh Dg "cat - |psql -q -U golem golem_database"
    echo "!!ENGAGE HV PS & Open gas reservoir !!"
    echo 'echo "APPL 25,2;OUTPut:IMMediate ON"|netcat -w 1 GWInstekPSW-a 2268'
    echo 'echo "APPL 55,2;OUTPut:IMMediate ON"|netcat -w 1 GWInstekPSW-a 2268'
    echo 'echo "OUTPut:IMMediate OFF"|netcat -w 1 GWInstekPSW-a 2268'
    echo 'or'
    echo 'GlowDischGasFlowSetup 65/25'
    
}    
function GlowDischGasFlowSetup ()
{
    echo "APPL $1,2;OUTPut:IMMediate ON"|netcat -w 1 GWInstekPSW-a 2268
    echo "Do not forget to run: GlowDischWaitForStop"
}    
    
    

function GlowDischWaitForStop()
{
    echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('glow discharge:start','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "$psql_password;cat - |psql -q -U golem golem_database"
    Time=15
    for i in `seq 1 $Time`; do echo Waiting for GD to stop $i/$Time; sleep 1m;done
    GlowDischStop
    
}    

function GlowDischStop()
{
    $LogFunctionPassing;
    echo "OUTPut:IMMediate OFF"|netcat -w 1 GWInstekPSW-a 2268
    sleep 10s
    GlowDischPSOFF
    TMP1StandbyOFF
    TMP2StandbyOFF
    sleep 5s
    Vent1ON
    GasHeOFF
    echo "!!Close He reservoir !!"
    echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('glow discharge:end','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "$psql_password;cat - |psql -q -U golem golem_database"
    #echo "UPDATE chamber_glowdischarge SET end_time='`date +%H:%M:%S`',end_pressure=`cat $SHML/ActualChamberPressuremPa`,end_temperature=`cat $SHML/ActualChamberTemperature`  WHERE id IN(SELECT max(id) FROM chamber_glowdischarge)"|ssh Dg "cat - |psql -q -U golem golem_database"

    
    
}    

function InitCheck()
{
    BareCheckPingStatus GasManagementPS
    BareCheckPingStatus PfeifferVacuumGauge
    BareCheckPingStatus ChamberThermistor
    BareCheckPingStatus ChamberRelays
}

function OpenSession()
{
    mkdir -p $SHM
    sshfs golem@Dirigent:$SHM/ $SHM/
    VacuumLog 
    echo OK
}	

function CloseSession()
{
    RASPCloseSessionCommons
    echo OK
}	


function RelayON()
{
    echo "*B1OS"$1"H"|$QuidoModul   1>/dev/null 2>/dev/null
}


function RelayOFF()
{
    echo "*B1OS"$1"L"|$QuidoModul  1>/dev/null 2>/dev/null
}

#e.g. ./Dirigent.sh -r Chamber PumpingON()

function BakingON(){ RelayON 1; }
function BakingOFF(){ RelayOFF 1; }

function RotPumpON(){ RelayON 16; }
function RotPumpOFF(){ RelayOFF 16; }

function TMP2StandbyON(){ RelayON 3; }
function TMP2StandbyOFF(){ RelayOFF 3; }
function TMP2ON(){ RelayON 4; }
function TMP2OFF(){ RelayOFF 4; }
function Vent2ON(){ RelayON 5; }
function Vent2OFF(){ RelayOFF 5; } #2 Jih
function Vent2sON(){ RelayON 10; }
function Vent2sOFF(){ RelayOFF 10; } #2 Jih

function TMP1StandbyON(){ RelayON 6; }
function TMP1StandbyOFF(){ RelayOFF 6; }
function TMP1ON(){ RelayON 7; }
function TMP1OFF(){ RelayOFF 7; }
function Vent1ON(){ RelayON 8; }
function Vent1OFF(){ RelayOFF 8; } #1:Sever
function Vent1sON(){ RelayON 9; }
function Vent1sOFF(){ RelayOFF 9; } #1:Sever

function GlowDischPSON(){ RelayON 11; }
function GlowDischPSOFF(){ RelayOFF 11; }

function GasH2ON(){ RelayON 12; cp $SHML/WG_calibration_table4H2 $SHML/WG_calibration_table; echo -ne "H2" > $SHML/GasSwitch; }
function GasH2OFF(){ RelayOFF 12; echo -ne "NULL" > $SHML/GasSwitch; }

function GasHeON(){ RelayON 13; cp $SHML/WG_calibration_table4He $SHML/WG_calibration_table; echo -ne "He" > $SHML/GasSwitch; }
function GasHeOFF(){ RelayOFF 13; echo -ne "NULL" > $SHML/GasSwitch; }












function RotP_ON(){
    $LogFunctionPassing;
    RotPumpON
}

function RotP_OFF(){
    $LogFunctionPassing;
    RotPumpOFF
}

function Baking_ON(){
    $LogFunctionPassing;
    # ChamberGraph &
     echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('baking:start','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "$psql_password;cat - |psql -q -U golem golem_database"
    #echo "INSERT into chamber_baking (date,start_time, shot_no, start_pressure, start_temperature) VALUES ('`date '+%y-%m-%d'`','`date '+%H:%M:%S'`',  `cat $SHM/shot_no`, `cat $SHML/ActualChamberPressuremPa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "cat - |psql -q -U golem golem_database"
    Charger EtCommutatorOFF
    sleep 1
    BakingON
}

function Baking_OFF(){
    $LogFunctionPassing;
    BakingOFF
    echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('baking:end','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "$psql_password;cat - |psql -q -U golem golem_database"
    #echo "UPDATE chamber_baking SET end_time='`date +%H:%M:%S`',end_pressure=`cat $SHML/ActualChamberPressuremPa`,end_temperature=`cat $SHML/ActualChamberTemperature`  WHERE id IN(SELECT max(id) FROM chamber_baking)"|ssh Dg "cat - |psql -q -U golem golem_database"
}

function Baking_test()
{
    $LogFunctionStart
    Baking_ON
    sleep 1s
    Baking_OFF
    $LogFunctionEnd

}


function ReadChamberTemp()
{
    PapagoReadCh1
}


function PrepareDischarge()
{ 

    if [ -e $SHMP/pressure ]; 
    then
        p_working_gas_discharge_request=`cat $SHMP/pressure`
    else
        LogIt "No request for Working gas pressure, therefore Discharge with default gas pressure"
        p_working_gas_discharge_request=`cat $SHM/Management/DefaultParameters/pressure`
        cp $SHM/Management/DefaultParameters/pressure $SHMP/pressure
    fi    
    echo $p_working_gas_discharge_request > $SHM0/$SUBDIR/$ThisDev/p_working_gas_discharge_request
    LogIt "Going with gas pressure $p_working_gas_discharge_request"
    
    if [ -e $SHMP/gas ]; 
    then
        p_working_gas_type_request=`cat $SHMP/gas`
    else
        LogIt "No request for Working gas type, therefore Discharge with default gas type"
        p_working_gas_type_request=`cat $SHM/Management/DefaultParameters/gas`
        cp $SHM/Management/DefaultParameters/gas $SHMP/gas
    fi    
    echo $p_working_gas_type_request > $SHM0/$SUBDIR/$ThisDev/X_working_gas_discharge_request
    LogIt "Going with gas type $p_working_gas_type_request"
        
    cp $SHML/ActualChamberPressuremPa $SHM0/$SUBDIR/$ThisDev/p_chamber_pressure_before_discharge
    

    
    if [ $p_working_gas_discharge_request -eq 0 ]; 
    then 
        LogIt "Vacuum discharge !"
    else
        case $p_working_gas_type_request in 
            H)
                LogIt "Discharge with Hydrogen request"
                GasH2ON
                ;;
            He)
                LogIt "Discharge with Helium request"
                GasHeON
                ;;
            "")   
                LogIt "No request for Working gas, therefore Discharge with default H2 gas"
                GasH2ON
        esac
        LogIt "Discharge with $p_working_gas_discharge_request mPa request"
        echo "Discharge Preparation" >>$SHML/PressureLog

        python3 -c "from vacuum.working_gas import fill_chamber; fill_chamber(target_pressure=$p_working_gas_discharge_request,max_time=30)"    
    fi
    
    cp $SHML/ActualChamberPressuremPa $SHM0/$SUBDIR/$ThisDev/p_chamber_pressure_predischarge

    
    $LogFunctionPassing;
    

    echo OK;
}

function WGtestH2
{
    $LogFunctionStart
    GasH2ON;WGtest;GasH2OFF
    $LogFunctionEnd
}


function WGtestHe
{
    $LogFunctionStart
    GasHeON;WGtest;GasHeOFF
    $LogFunctionEnd
}

#@Dirigent:  Dgrs;./Dirigent.sh --wgtest

function WGtest
{
    $LogFunctionStart
    echo "WGtest start" >>$SHML/PressureLog
    python3 -c 'from vacuum.working_gas import fill_chamber; fill_chamber(target_pressure=20,max_time=15)'
    sleep 5s
    python3 -c 'from vacuum.working_gas import close_valve; close_valve()'
    echo "WGtest end" >>$SHML/PressureLog
    $LogFunctionEnd

}

function Arming()
{
    $LogFunctionGoingThrough
    echo "Discharge Arming" >>$SHML/PressureLog
    DistanceUpdateCurrentShotDataBase "p_chamber_pressure_predischarge=`cat $SHML/ActualChamberPressuremPa`"

    echo OK
}


function PostDisch()
{
    $LogFunctionGoingThrough
    echo "Discharge end" >>$SHML/PressureLog
    cp $SHML/ActualChamberPressuremPa $SHM0/$SUBDIR/$ThisDev/final_chamber_pressure
    echo 25> $SHM0/$SUBDIR/$ThisDev/final_chamber_temperature
    DistanceUpdateCurrentShotDataBase "p_chamber_pressure_after_discharge=`cat $SHML/ActualChamberPressuremPa`"
    python3 -c 'from vacuum.working_gas import close_valve; close_valve()'
    SwitchOutAllGases

    echo OK
}


function PostDischargeFinals()
{

    #cat $SHML/GlobalLogbook|grep VacuumLog|awk '{print $1 " " $5 " " $7 }' >$SHM0/$SUBDIR/$ThisDev/Chamber.dat
    gnuplot  -e "set xdata time;set timefmt '%H:%M:%S';set xtics format '%tH:%tM' time;set xlabel 'Time [h:m]';set ylabel 'chamber pressure p_{ch} [mPa]';set xrange [*:*];set title 'Session chamber logbook';set yrange [*:50];set y2range [0:200];set y2tics 0, 20,200;set y2label'Chamber temperature T_{ch} [^o C]';set ytics nomirror;set terminal jpeg;plot '$SHM/ChamberLog' using 1:2 title 'pressure' with lines axis x1y1,'$SHM/ChamberLog' using 1:3 title 'temperature' with lines axis x1y2" > $SHM0/$SUBDIR/$ThisDev/SessionChamber_p_T_Logbook.jpg

       
    convert -resize $icon_size $SHM0/$SUBDIR/$ThisDev/SessionChamber_p_T_Logbook.jpg $SHM0/$SUBDIR/$ThisDev/graph.png
    convert /$SHM/Management/imgs/Chamber_icon.jpg $SHM0/$SUBDIR/$ThisDev/graph.png +append $SHM0/$SUBDIR/$ThisDev/icon_.png
    convert -bordercolor Black -border 2x2 $SHM0/$SUBDIR/$ThisDev/icon_.png $SHM0/$SUBDIR/$ThisDev/icon.png
    Web

}

function WebRecord()
{
    echo $1 >> $SHM0/$SUBDIR/$ThisDev/$ThisDev.html
}

function Web()
{
    cp $SHM/Management/imgs/Chamber.jpg $SHM0/$SUBDIR/$ThisDev/
    
    
    WebRecord "<html><body><h1>$ThisDev</h1>"
    WebRecord "<img src='Chamber.jpg' width='50%'/><br></br>"
    WebRecord "<a href='SessionChamber_p_T_Logbook.jpg'><img src='SessionChamber_p_T_Logbook.jpg' width='60%'/></a>"
    WebRecord "</body></html>"
    
}    

function SwitchOutAllGases
{
    GasH2OFF;GasHeOFF
}

function ChamberGraph()
{
    tail -f /dev/shm/golem/ChamberLog|feedgnuplot  --timefmt "%H:%M:%S" --set 'format x "%H:%M"' --legend 0 "pressure" --legend 1 "temperature" --y2min 0 --y2max 200 --ymin 0 --ymax 30 --domain --lines --y2 1 --extracmds "set title 'Chamber';set xlabel 'Time [h:m]';set ylabel 'Pressure p_{ch} [mPa]'; set y2label 'Temperature T_{ch} [^o C]'" --stream
}

function KillFeed()
{
    killall feedgnuplot
}
    

function VacuumLog()
{
    echo -ne NULL > $SHML/GasSwitch;
    echo -ne 0 > $SHML/ActualVoltageAtGasValve;
    rm -f $SHM/ChamberLog 
    rm -f $SHML/GlobalLogbook

    while [ 1 ]; do
        for i in `seq 1 5`; do
            ActualChamberPressurePa=`get_chamber_pressure`
            ActualForVacuumPressurePa=`get_forvacuum_pressure`
            ActualChamberTemperature=`PapagoReadCh1`
            #echo $ActualChamberPressurePa > $SUBDIR/$ThisDev/ActualChamberPressurePa #NEJDE, ODSTRELUJE sshfs
            echo -ne $ActualChamberPressurePa > $SHML/ActualChamberPressurePa
            echo -ne  $ActualForVacuumPressurePa > $SHML/ActualForVacuumPressurePa
            echo -ne  $ActualChamberTemperature > $SHML/ActualChamberTemperature
            echo -ne  `echo $ActualChamberPressurePa*1000|bc|xargs printf '%4.2f'|sed 's/,/\./g'` > $SHML/ActualChamberPressuremPa
            echo `date '+%H:%M:%S'` "Chamber: `echo $ActualChamberPressurePa*1000|bc|xargs printf '%4.2f'` mPa, ForVacuum: $ActualForVacuumPressurePa Pa, Gas: `cat $SHML/GasSwitch`, GasValve: `cat $SHML/ActualVoltageAtGasValve|xargs printf '%4.2f'` V">> $SHML/PressureLog
            echo `date '+%H:%M:%S'` " " `echo $ActualChamberPressurePa*1000|bc|xargs printf '%4.2f'` " " `cat $SHML/ActualChamberTemperature|xargs printf '%4.2f'`  >> $SHM/ChamberLog
            #sleep 1 # neni potreba, je to zatim pomale dost
        done
        LogIt "Chamber: `echo $ActualChamberPressurePa*1000|bc|xargs printf '%4.2f'|sed 's/,/\./g'` mPa,  `cat $SHML/ActualChamberTemperature|xargs printf '%4.2f'` C, ForVacuum: $ActualForVacuumPressurePa Pa, Gas: `cat $SHML/GasSwitch`, GasValve: `cat $SHML/ActualVoltageAtGasValve|xargs printf '%4.2f'` V";
    done

}	

function Emergency()
{
    $LogFunctionGoingThrough
    echo OK
}	

function xtermPumpingON(){
    xterm -fg yellow -bg blue -title "Golem pumping start" -hold -e /bin/bash -l -c "PumpingON"
    }

function PumpingON(){
    if [ -e $SHMS/session_date ]; then
        $LogFunctionStart
        echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('pumping:start','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "cat - |psql -q -U golem golem_database"
        RotPumpON
        LogIt "sleep 2 s to run TMPs ...";sleep 2
        TMPs_ON
        LogIt "sleep 20 s to open all valves ...";sleep 20
        Vents_ON
        $LogFunctionEnd
    else
        critical_error "Please, first open a session"
    fi
}


function xtermPumpingOFF(){
    xterm -fg yellow -bg blue -title "Golem pumping end" -hold -e /bin/bash -l -c "PumpingOFF"
    }


function PumpingOFF()	
{ 
    $LogFunctionStart
    echo "INSERT into chamber_management (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('pumping:end','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) "|ssh Dg "cat - |psql -q -U golem golem_database"
    Relax
    #echo "UPDATE chamber_pumping SET end_time='`date +%H:%M:%S`',end_pressure=`cat $SHML/ActualChamberPressuremPa`,end_temperature=`cat $SHML/ActualChamberTemperature`  WHERE id IN(SELECT max(id) FROM chamber_pumping)"|ssh Dg "cat - |psql -q -U golem golem_database"
    Vents_OFF
    TMPs_OFF
    LogIt "sleep 5 s to stop Rotary pump ...";sleep 5
    RotPumpOFF
    $LogFunctionEnd
}


function TMPs_ON()
{
    $LogFunctionPassing
    TMP1ON
    TMP2ON
}

function Vents_ON()
{
    $LogFunctionPassing
    Vent1ON
    Vent2ON
    Vent1sON
    Vent2sON
}

function TMPs_OFF()
{ 
    $LogFunctionPassing
    TMP1OFF
    TMP2OFF
}

function Vents_OFF()
{   
    $LogFunctionPassing
    Vent1OFF
    Vent2OFF
    Vent1sOFF
    Vent2sOFF
}

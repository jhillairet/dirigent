#!/bin/bash
SUBDIR=Devices
ThisDev=InnerStabCapacitorChargerRASP
SHM="/dev/shm/golem"

BASEDIR="../../../"
source $BASEDIR/Commons.sh


COMMAND="ssh golem@$ThisDev"

function OpenSession()
{
    echo OK
}


function CloseSession()
{
    echo OK
}



function PrepareDischarge() 
{ 
    $COMMAND 'echo "import lib as lib;lib.gpio_low(18)"|python'
    $COMMAND 'echo "import lib as lib;lib.gpio_low(16)"|python'
    Relax
    $COMMAND 'echo "import lib as lib;lib.gpio_low(17)"|python'
    Relax
    $COMMAND 'echo "import lib as lib;lib.gpio_high(17)"|python'
    echo OK
}

function PostDisch()
{
    $COMMAND 'echo "import lib as lib;lib.gpio_high(18)"|python'
    $COMMAND 'echo "import lib as lib;lib.gpio_high(16)"|python'
    echo OK
}

function PostDischargeFinals()
{
    echo OK
}

function CommonInitDischarge()
{
    echo OK
}





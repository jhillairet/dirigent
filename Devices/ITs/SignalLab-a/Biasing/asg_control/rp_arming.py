#!/usr/bin/python3

import redpitaya_scpi as rp_scpi
from utils import roll_the_leds


def arm_red_pitaya(address: str, outp: int = 1):
    rp_s = rp_scpi.scpi(address)

    # turns on output 1
    rp_s.tx_txt('OUTPUT{}:STATE ON'.format(outp))
    rp_s.tx_txt('SOUR1:TRIG:SOUR EXT_PE'.format(outp))
    # roll_the_leds(rp_s)


arm_red_pitaya('192.168.2.169')

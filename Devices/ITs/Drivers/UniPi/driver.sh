#!/bin/bash


function RelayON()
{
if [ $1 -le 8 ]; then
echo "from unipi.relays.controller20 import RelaysController;c = RelaysController();c.relay($1, True)"|python3  1>&- 2>&-; fi
if [ $1 -gt 8 ]; then
echo "from unipi.relays.controller25 import RelaysController;c = RelaysController();c.relay((($1-8)), True)"|python3  1>&- 2>&-; fi
}


function RelayOFF()
{
if [ $1 -le 8 ]; then
echo "from unipi.relays.controller20 import RelaysController;c = RelaysController();c.relay($1, False)"|python3  1>&- 2>&-; fi
if [ $1 -gt 8 ]; then
echo "from unipi.relays.controller25 import RelaysController;c = RelaysController();c.relay((($1-8)), False)"|python3  1>&- 2>&-; fi
}

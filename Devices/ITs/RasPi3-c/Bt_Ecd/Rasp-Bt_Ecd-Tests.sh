#!/bin/bash
# To be executed at RASPs

source Rasp-Commons.sh


COMMAND="netcat -w 1 Bt_Ecd_Monitor 5555"
TheOscilloscope=Bt_Ecd_Monitor

Drivers="Devices/uControllers/Drivers/UniPi"

source Drivers/UniPi/driver.sh

DirToDischargeParams=$SHM0/Operation/Discharge



function ShortCircuitsDisEngage()
{
    CallCentralFunction Devices/PowerSupplies/BEZ-HV/Basic ShortCircuitOFF;
}

function ShortCircuitsEngage()
{
    CallCentralFunction Devices/PowerSupplies/BEZ-HV/Basic ShortCircuitON;
    
}


function HVlightON() { RelayON 14; }
function HVlightOFF() { RelayOFF 14; }	
function ConnectOscill() { RelayON 12; }
function DisConnectOscill() { RelayOFF 12; }

function BtHVrelayON() { RelayON 4 ; }
function BtHVrelayOFF() { RelayOFF 4 ; }
function BtCommutatorClockWise() { RelayON 5 ;mRelax; RelayOFF 5; }
function BtCommutatorAntiClockWise() { RelayON 6 ;mRelax; RelayOFF 6; }

function BtChargeCommutatorClockWise() { RelayON 7 ;mRelax; RelayOFF 7; }
function BtChargeCommutatorAntiClockWise() { RelayON 8 ;mRelax; RelayOFF 8; }

function PS_Commutator_ON() { RelayON 1 ; }
function PS_Commutator_OFF() { RelayOFF 1 ; }

function LV_PS_test_ON() { RelayON 3 ; }
function LV_PS_test_OFF() { RelayOFF 3 ; }

function CdHVrelayON() { RelayON 2 ; }
function CdHVrelayOFF() { RelayOFF 2 ; }
function EtCommutatorClockWise() { RelayON 9 ;mRelax; RelayOFF 9; }
function EtCommutatorOFF() { RelayON 10 ;mRelax; RelayOFF 10; }
function EtCommutatorAntiClockWise() { RelayON 11 ;mRelax; RelayOFF 11; }


function ChargingResistor_I_DisEngage() { RelayON 15; }
function ChargingResistor_I_Engage() { RelayOFF 15; }



#Function test, see http://golem.fjfi.cvut.cz/shots/33132/

function PrepareSessionEnv@SHM()
{
    MountCentralSHMEnvironment 
}

LowVoltagePowerSupplyFlag=0 #Nezapomenout na Dgrs (rsync)!

function HVon()
{
    $LogFunctionPassing

    Relax
    #HV Ostry rezim:
    # ???? Mas URCITE odpojeny LV zdroj ??
    # ====================================
    if (( LowVoltagePowerSupplyFlag ));then
        LV_PS_test_ON
    else
        HVlightON
        CallCentralFunction Devices/PowerSupplies/BEZ-HV/Basic PowerON;
    fi    
    #LV Tupy rezim:
    # see Devices/PowerSupplies/RigolPS831A-a/HVsubstitute.sh
    Relax
}	


function HVoff()
{

    if (( LowVoltagePowerSupplyFlag ));then
        LV_PS_test_OFF
    else
        HVlightOFF
        CallCentralFunction Devices/PowerSupplies/BEZ-HV/Basic PowerOFF;
    fi    
    # see Devices/PowerSupplies/RigolPS831A-a/HVsubstitute.sh# Tupy rezim


    $LogFunctionPassing
    mRelax
	ChargingResistorsAllON
	
}


function ScopeConfiguration()
{
    SCALE=300
    OFFSET=-400
    PROBE=100
    #LV test setup
    #==============
    #SCALE=9
    #OFFSET=0
    #PROBE=100
    #==============
    echo "
    CHANnel1:DISPlay ON;CHANnel1:PROBe $PROBE;CHANnel1:SCALe $SCALE;CHANnel1:OFFSet $OFFSET;
    CHANnel2:DISPlay ON;CHANnel2:PROBe 1;CHANnel2:SCALe 0.5;CHANnel2:OFFSet -4.3;
    CHANnel3:DISPlay ON;CHANnel3:PROBe $PROBE;CHANnel3:SCALe $SCALE;CHANnel3:OFFSet $OFFSET;
    CHANnel4:DISPlay ON;CHANnel4:PROBe $PROBE;CHANnel4:SCALe $SCALE;CHANnel4:OFFSet $OFFSET;
    TIMebase:DELay:ENABle OFF;TIMebase:MAIN:SCALe 10;TIMebase:MAIN:OFFSet 49;
    :STOP;:CLEAR;
    :SYSTem:KEY:PRESs MOFF"|$COMMAND
}


function PrepareSessionEnv@SHM()
{
    MountCentralSHMEnvironment
    ScopeConfiguration
}	




#function ChargingResistor_II_DisEngage() { RelayON 6 ; }
#function ChargingResistor_II_Engage() { RelayOFF 6 ; }


ChargingFlag=0
ChargingResistors=0





function GoToSafeState()
{
    HVoff; 
    CdHVrelayOFF;
    BtHVrelayOFF;
    Relax;
    ShortCircuitsEngage;
    HVlightOFF
}

function Emergency()
{
    GoToSafeState
}	





function ChargingResistorsAllOFF()
{
    $LogFunctionStart
    ChargingResistors=0
    ChargingResistor_I_DisEngage
    #ChargingResistor_II_DisEngage
    echo "Fast">$SHM/Charging
}	

function ChargingResistorsAllON()
{
    $LogFunctionStart
    ChargingResistors=1
    ChargingResistor_I_Engage
    #ChargingResistor_II_Engage
    echo "Slow">$SHM/Charging
}


function OpenOscDVMchannel()
{
    ConnectOscill # and connect Osclilloscope channels to the system
    echo ":DVM:ENABle ON;:DVM:SOURce CHANnel$1;DVM:MODE DC"|$COMMAND
    mRelax
    echo ":STOP;:CLEAR;:TRIGger:SWEEp AUTO;RUN"|$COMMAND
    mkdir -p /dev/shm/$TheOscilloscope
    touch /dev/shm/$TheOscilloscope/OscRequestStream
    tail -f /dev/shm/$TheOscilloscope/OscRequestStream|netcat -N $TheOscilloscope 5555 >> /dev/shm/$TheOscilloscope/OscRespondStream &
}


function ReadDVMOscilloscope()
{
    echo ":SYSTem:TIME?">> /dev/shm/$TheOscilloscope/OscRequestStream;mRelax
    echo ":DVM:CURRENT?">> /dev/shm/$TheOscilloscope/OscRequestStream;mRelax
    echo `tail -1 /dev/shm/$TheOscilloscope/OscRespondStream|xargs printf '%4.0f\n'`
}

function DVMDisable()
{
    echo ":STOP;:DVM:ENABle OFF"|$COMMAND
    DisConnectOscill # and disconnect Osclilloscope channels from the system
}




function EtTest_LV() # Nelze sjednotit HV a LV (1122 Katastrofa)
{
   SCALE=50 #LV PS
    OFFSET=0
    echo "CHANnel1:DISPlay ON;CHANnel1:PROBe 100;CHANnel1:SCALe $SCALE;CHANnel1:OFFSet $OFFSET"|$COMMAND #HV PowSup
    echo "CHANnel3:DISPlay ON;CHANnel3:PROBe 100;CHANnel3:SCALe $SCALE;CHANnel3:OFFSet $OFFSET"|$COMMAND #Bt
    echo "CHANnel4:DISPlay ON;CHANnel4:PROBe 100;CHANnel4:SCALe $SCALE;CHANnel4:OFFSet $OFFSET"|$COMMAND #Et

    CallCentralFunction Devices/RelayBoards/Quido4-a/Racks_Misc 12V_24V_intosystem_ON

OpenOscDVMchannel  1
ChargingResistorsAllON;Relax
#ChargingResistor_I_DisEngage;mRelax # # Bacha, muze najizdet hrozne rychle
ShortCircuitsDisEngage;Relax
CdHVrelayON;Relax

LV_PS_test_ON

#LV on :
# Rigol zdroj
#echo ":INST CH2;:CURR 0.1;:VOLT -30;:OUTP CH2,ON"|netcat -w 1 HVsubstitute 5555
# anebo staci jednoduchy 24V zdrojik ..

for i in `seq 1 3`; do
    PowSupVoltage=`ReadDVMOscilloscope RigolMSO5104Charger`
    LogIt "Charging ... HV=$PowSupVoltage V"
    sleep 1
done

#LV off :
#echo ":OUTP CH2,OFF"|netcat -w 1 HVsubstitute 5555 # Tests on RigolPS

Relax
CdHVrelayOFF;
LV_PS_test_OFF;
sleep 3
echo "FPANEL:PRESS SINGLESEQ"|netcat -q 1 BasicDiagnostics 4000 #TektrMSO56 SingleSeq
mRelax
CallCentralFunction Operation/Discharge/Basic TriggerRequest
sleep 2
ShortCircuitsEngage;mRelax
sleep 3
echo ":STOP;:DVM:ENABle OFF"|$COMMAND
killall netcat 
DisConnectOscill
CallCentralFunction Devices/RelayBoards/Quido4-a/Racks_Misc 12V_24V_intosystem_OFF

}


function EtTest_HV() # Nelze sjednotit HV a LV (1122 Katastrofa)
{
   SCALE=300 #HV PS
    OFFSET=-30
    echo "CHANnel1:DISPlay ON;CHANnel1:PROBe 100;CHANnel1:SCALe $SCALE;CHANnel1:OFFSet $OFFSET"|$COMMAND #HV PowSup
    echo "CHANnel3:DISPlay ON;CHANnel3:PROBe 100;CHANnel3:SCALe $SCALE;CHANnel3:OFFSet $OFFSET"|$COMMAND #Bt
    echo "CHANnel4:DISPlay ON;CHANnel4:PROBe 100;CHANnel4:SCALe $SCALE;CHANnel4:OFFSet $OFFSET"|$COMMAND #Et

    CallCentralFunction Devices/RelayBoards/Quido4-a/Racks_Misc 12V_24V_intosystem_ON

OpenOscDVMchannel  1
#echo ":STOP;:CLEAR;:TRIGger:SWEEp AUTO;RUN"|$COMMAND;Relax
ChargingResistorsAllON;Relax
#ChargingResistor_I_DisEngage;Relax # # Bacha, muze najizdet hrozne rychle #NAjizdi strasne rychle! 2 s a mas 1200 V ... na Ecd!
ShortCircuitsDisEngage;Relax
CdHVrelayON;Relax
HVon

for i in `seq 1 2`; do #200V@5, 300V@10, 450@15, 570@20, 700@25 (ChargingResistorsAllON)
    PowSupVoltage=`ReadDVMOscilloscope RigolMSO5104Charger`
    LogIt "Charging ... HV=$PowSupVoltage V"
    sleep 1
done

HVoff
Relax
CdHVrelayOFF;
sleep 3
echo "FPANEL:PRESS SINGLESEQ"|netcat -q 1 BasicDiagnostics 4000 #TektrMSO56 SingleSeq
mRelax
CallCentralFunction Operation/Discharge/Basic TriggerRequest
sleep 2
ShortCircuitsEngage;mRelax
sleep 3
echo ":STOP;:DVM:ENABle OFF"|$COMMAND
killall netcat 
DisConnectOscill
        CallCentralFunction Devices/RelayBoards/Quido4-a/Racks_Misc 12V_24V_intosystem_OFF
}


function BtTest_LV() # Nelze sjednotit HV a LV (1122 Katastrofa)
{
   SCALE=50 #LV PS
    OFFSET=0
    echo "CHANnel1:DISPlay ON;CHANnel1:PROBe 100;CHANnel1:SCALe $SCALE;CHANnel1:OFFSet $OFFSET"|$COMMAND #HV PowSup
    echo "CHANnel3:DISPlay ON;CHANnel3:PROBe 100;CHANnel3:SCALe $SCALE;CHANnel3:OFFSet $OFFSET"|$COMMAND #Bt
    echo "CHANnel4:DISPlay ON;CHANnel4:PROBe 100;CHANnel4:SCALe $SCALE;CHANnel4:OFFSet $OFFSET"|$COMMAND #Et


        CallCentralFunction Devices/RelayBoards/Quido4-a/Racks_Misc 12V_24V_intosystem_ON

OpenOscDVMchannel  1
#echo ":STOP;:CLEAR;:TRIGger:SWEEp AUTO;RUN"|$COMMAND;Relax
ChargingResistorsAllON;Relax
ChargingResistor_I_DisEngage;Relax # Bacha, muze najizdet hrozne rychle
ShortCircuitsDisEngage;Relax
BtHVrelayON;Relax

LV_PS_test_ON
#LV on :
#echo ":INST CH2;:CURR 0.1;:VOLT -30;:OUTP CH2,ON"|netcat -w 1 HVsubstitute 5555

for i in `seq 1 6`; do # pri 100 Ohm 5s do 50V
    PowSupVoltage=`ReadDVMOscilloscope RigolMSO5104Charger`
    LogIt "Charging ... HV=$PowSupVoltage V"
    sleep 1
done

#LV off :
#echo ":OUTP CH2,OFF"|netcat -w 1 HVsubstitute 5555 # Tests on RigolPS

BtHVrelayOFF
LV_PS_test_OFF
sleep 3
echo "FPANEL:PRESS SINGLESEQ"|netcat -q 1 BasicDiagnostics 4000 #TektrMSO56a SingleSeq
mRelax
CallCentralFunction Operation/Discharge/Basic TriggerRequest
sleep 2
ShortCircuitsEngage;mRelax
sleep 3
echo ":STOP;:DVM:ENABle OFF"|$COMMAND
killall netcat 
DisConnectOscill
        CallCentralFunction Devices/RelayBoards/Quido4-a/Racks_Misc 12V_24V_intosystem_OFF
}


function BtTest_HV() # Nelze sjednotit HV a LV (1122 Katastrofa)
{
   SCALE=300 #LV PS
    OFFSET=-30
    echo "CHANnel1:DISPlay ON;CHANnel1:PROBe 100;CHANnel1:SCALe $SCALE;CHANnel1:OFFSet $OFFSET"|$COMMAND #HV PowSup
    echo "CHANnel3:DISPlay ON;CHANnel3:PROBe 100;CHANnel3:SCALe $SCALE;CHANnel3:OFFSet $OFFSET"|$COMMAND #Bt
    echo "CHANnel4:DISPlay ON;CHANnel4:PROBe 100;CHANnel4:SCALe $SCALE;CHANnel4:OFFSet $OFFSET"|$COMMAND #Et

        CallCentralFunction Devices/RelayBoards/Quido4-a/Racks_Misc 12V_24V_intosystem_ON
OpenOscDVMchannel  1
#echo ":STOP;:CLEAR;:TRIGger:SWEEp AUTO;RUN"|$COMMAND;Relax
ChargingResistorsAllON;Relax
ChargingResistorsAllOFF;Relax
#ChargingResistor_I_DisEngage;mRelax # Bacha, muze najizdet hrozne rychle
ShortCircuitsDisEngage;Relax
BtHVrelayON;Relax

HVon

for i in `seq 1 20`; do #300V@4s, 450V@8s 600V@12s 900V@20s(ChargingResistorsAllOFF)
    PowSupVoltage=`ReadDVMOscilloscope RigolMSO5104Charger`
    LogIt "Charging ... HV=$PowSupVoltage V"
    sleep 1
done

HVoff
Relax
BtHVrelayOFF
sleep 3
echo "FPANEL:PRESS SINGLESEQ"|netcat -q 1 BasicDiagnostics 4000 #TektrMSO56a SingleSeq
mRelax
CallCentralFunction Operation/Discharge/Basic TriggerRequest
sleep 2
ShortCircuitsEngage;mRelax
sleep 3
echo ":STOP;:DVM:ENABle OFF"|$COMMAND
killall netcat 
DisConnectOscill
        CallCentralFunction Devices/RelayBoards/Quido4-a/Racks_Misc 12V_24V_intosystem_OFF
}


function Sandbox()
{
    echo $RASPs
}

#scp golem@golem:/golem/Dirigent/Devices/ITs/RasPi3-c/*.sh .;source Rasp_Bt_Ecd.sh;EtTest_LV
#scp golem@golem:/golem/Dirigent/Devices/ITs/RasPi3-c/*.sh .;source Rasp_Bt_Ecd.sh;EtTest_HV 


#Bt tests s 18 V zdrojem misto HV, see 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#Dgrs;./Dirigent.sh -r "Charger BtTest"
#./Dirigent.sh --discharge --UBt 25 --TBt 0 --Ucd  --Tcd 0 --preionization 1 --gas H --pressure 20 --comment "Bt tests s 30 V zdrojem misto HV"
# see http://golem.fjfi.cvut.cz/shots/33335/RASPs/Charger/Charger.html

#Bt Et tests s 18 V zdrojem misto HV, see 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# *http://golem.fjfi.cvut.cz/shots/33132/
#./Dirigent.sh --discharge --UBt 25 --TBt 0 --Ucd 25 --Tcd 0 --preionization 1 --gas H --pressure 20 --comment "Bt Et tests s 30 V zdrojem misto HV"
# *http://golem.fjfi.cvut.cz/shots/33337/RASPs/Charger/Charger.html

#Et tests s 18 V zdrojem misto HV, see 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#./Dirigent.sh --discharge --UBt 0 --TBt 0 --Ucd 25 --Tcd 0 --preionization 1 --gas H --pressure 20 --comment "Et tests s 30 V zdrojem misto HV"
#Et tests s 30 V zdrojem misto HV, see http://golem.fjfi.cvut.cz/shots/33334/RASPs/Charger/Charger.html
#Dgrs;./Dirigent.sh -r "Charger EtTest"

#LV test setup
    #==============
    #SCALE=9
    #OFFSET=0
    #PROBE=100
    #==============

#./Dirigent.sh --discharge --UBt 20 --TBt 1000 --Ucd 10 --Tcd 2000 --preionization 1 --gas H --pressure 0 --comment "LV test discharge"

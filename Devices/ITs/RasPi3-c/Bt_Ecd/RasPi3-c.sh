#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh

RASP=RasPi3-c

SSHatRASP="ssh -Y golem@$RASP"
SOURCE='source Rasp-Bt_Ecd.sh;source Rasp_Trigger.sh'


function PrepareSessionEnv@SHM()
{
    scp Rasp-*.sh $RASP:
    scp $SW/Devices/ITs/Drivers/Rasp-Commons.sh $RASP:
    scp $SW/Commons.sh $RASP:
    ssh $RASP "source Rasp-Bt_Ecd.sh;PrepareSessionEnv@SHM"

}

function GetReadyTheDischarge
{
  ssh $RASP "source Rasp-Bt_Ecd.sh;GetReadyTheDischarge"  
}

function SecurePostDischargeState()
{
      ssh $RASP "source Rasp-Bt_Ecd.sh;SecurePostDischargeState"
}

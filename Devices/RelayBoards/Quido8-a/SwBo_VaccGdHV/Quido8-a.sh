#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh

QuidoModul='telnet QuidoSwitchBoard-I 10001' #.248


# Device functions

function RelayON() { echo "*B1OS"$1"H"|$QuidoModul   1>/dev/null 2>/dev/null; }
function RelayOFF(){ echo "*B1OS"$1"L"|$QuidoModul  1>/dev/null 2>/dev/null; }

#1: Baking
#2: Rot pump
#3: KepcosSocketSwitch

# Toto vse by se melo zprehazet do /golem/Dirigent/Infrastructure/RelayBoards/

#function BakingON(){ RelayON 1; } 
#function BakingOFF(){ RelayOFF 1; } 

function RotPumpON(){ RelayON 2; } 
function RotPumpOFF(){ RelayOFF 2; } 

function PowerSupplyHVcontactorON() { RelayON 4; }	
function PowerSupplyHVcontactorOFF() { RelayOFF 4; }

function LowerShortCircuitsDisEngage() { RelayON 5; }
function LowerShortCircuitsEngage() { RelayOFF 5; }

#function DischargeSystemEngage() { RelayON 6; }
#function DischargeSystemDisEngage() { RelayOFF 6; }

#function GlowDischPSON(){ RelayON 7; }
#function GlowDischPSOFF(){ RelayOFF 7; }


# Device service for the GOLEM operation

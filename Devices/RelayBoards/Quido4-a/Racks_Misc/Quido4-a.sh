#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh

function RelayON() { echo "*B1OS"$1"H"|telnet Quido4-a  10001 1>/dev/null 2>/dev/null; }
function RelayOFF() { echo "*B1OS"$1"L"|telnet Quido4-a  10001 1>/dev/null 2>/dev/null;  }


# Device functions
function 12V_24V_intosystem_ON  { RelayON 1; }
function 12V_24V_intosystem_OFF { RelayOFF 1; }

# Device service for the GOLEM operation
function GetReadyTheDischarge
{
    12V_24V_intosystem_ON
}

function SecurePostDischargeState
{
    12V_24V_intosystem_OFF
}


#Development issues
####################

#source /golem/Dirigent/Devices/RelayBoards/Quido4-a/Racks_Misc/Quido4-a.sh;12V_24V_intosystem_ON


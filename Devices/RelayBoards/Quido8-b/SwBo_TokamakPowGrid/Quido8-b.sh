#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh

QuidoModul='telnet QuidoSwitchBoard-II 10001' #.254

function RelayON() { echo "*B1OS"$1"H"|$QuidoModul   1>/dev/null 2>/dev/null; }
function RelayOFF(){ echo "*B1OS"$1"L"|$QuidoModul  1>/dev/null 2>/dev/null; }


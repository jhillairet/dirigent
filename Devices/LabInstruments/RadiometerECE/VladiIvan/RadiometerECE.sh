#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh


function Wake-upCall
{ 
    Call $SW/Infrastructure/Support/Sockets/Galvanics RadiometerECEDiagnosticsON
 }
 
 function Sleep-downCall
{ 
    Call $SW/Infrastructure/Support/Sockets/Galvanics RadiometerECEDiagnosticsOFF
 } 

#!/bin/bash

SHM="/dev/shm/golem"



if [ -e $BasePath/session.setup ];  then 
    cd $BasePath; source session.setup; cd $OLDPWD
    Operation="Dirigent/HigherPower $Operation" # extra Dirigent Power
    export Operation="$Operation"

fi

if [ -e /dev/shm/golem/shot_no ]; then SHOT_NO=$(cat /dev/shm/golem/shot_no); fi


#Flags:    
VerboseFlag=0
    
    
#Id entification of the device/diagnostics/operation/infrastructure, that call this common functions 
#Striktne volame z dir, kde to je.
bash_source=${BASH_SOURCE[1]} 
pwd=$PWD #e.g. /golem/Dirigent/Infrastructure/Bt_Ecd/Basic
setup=$(basename $PWD) # e.g. Basic
instrument=$(dirname $PWD|xargs basename) # e.g. Bt_Ecd
family=$(dirname $PWD|xargs dirname|sed 's/\/golem\/Dirigent\///g;s/\/golem\/shm_golem\/ActualShot\///g;s/\/dev\/shm\/golem\/ActualShot\///g;s/\/dev\/shm\/golem\/ActualSession\///g') # e.g. Infrastructure , Casem lepe prosim ...
whoami=$family/$instrument/$setup/$bash_source #e.g. Infrastructure/Bt_Ecd/Basic/Bt_Ecd.sh
whereami=$family/$instrument/$setup/ #e.g. Infrastructure/Bt_Ecd/Basic/

#VerboseMod $instrument
ThisDev=$instrument #backward compatibility  # e.g. Bt_Ecd
diag_id=$instrument #backward compatibility # e.g. Bt_Ecd
setup_id=$setup #backward compatibility # e.g. Basic



# ============ General issues ============
GM="golem@golem"
sshGM="ssh $GM"
XTERM="xterm -T $Dev -e ssh golem@$Dev"
shot_dir="/golem/database/operation/shots"
Tek_mount_path="/home/golem/tektronix_drop"
date_format="+%y-%m-%d %H:%M:%S"
psql_password="export PGPASSWORD=$(cat /golem/production/psql_password)"
#DirigentServer="golem-new" 
DirigentServer="golem" 
SW="/golem/Dirigent"
Sounds=$SW/Management/sound_fx/

# ============ Session issues ============

SHMS="$SHM/ActualSession"
SHML="$SHMS/SessionLogBook"
SHMCLP="$SHM/CommandLineParameters"
SHMTAGS="$SHM/Tags"

# ============ Discharge issues ============
SHM0="$SHM/ActualShot"
SHM0TAGS="$SHM0/Production/Tags"
SHMP="$SHM0/Production/Parameters" #Parameters

#============ WWW stuff: ============
linkiconsize="30px"
icon_size="200x150" 
ScreenShotAllSize="1600,1000"
namesize=100
iconsize=200
PingTimeout=20
DummyDischargeCountDown=2
imgpath="http://golem.fjfi.cvut.cz/_static"
gitlabpath="https://gitlab.com/golem-tokamak/dirigent/-/tree/master"
gitlabicon="<img src=$imgpath/gitlab.png  width='$linkiconsize'/>"
googlephotosicon="<img src=$imgpath/GooglePhotos.png  width='$linkiconsize'/>"
manualicon="<img src=$imgpath/manual.png  width='$linkiconsize'/>"
diricon="<img src=$imgpath/direct.png  width='$linkiconsize'/>";
gnuploticon="<img src=$imgpath/gnuplot.png  width='$linkiconsize'/>";
pythonicon="<img src=$imgpath/python.png  width='$linkiconsize'/>";
resultsicon="<img src=$imgpath/results.png  width='$linkiconsize'/>";
parametersicon="<img src=$imgpath/parameters.png  width='$linkiconsize'/>";
psqlicon="<img src=$imgpath/postgresql.webp  width='$linkiconsize'/>";
underconst="<img src=$imgpath/UnderConstruction.png  width='$linkiconsize'/>";
helpicon="<img src=$imgpath/help.jpeg  width='$linkiconsize'/>";
#rightarrowicon="<img src=$imgpath/rightarrow.png  width='10px'/>";
rightarrowicon="&#9755;"
doubledoticon="<img src=$imgpath/dots-horizontal-double-512.webp  width='$linkiconsize'/>";

#Diagnostics_OnStageWithoutBasicDiagnostics=$(echo $Diagnostics_OnStage|sed 's/BasicDiagnostics\/DetectPlasma//g;s/BasicDiagnostics\/Basic//g')

#echo $Diagnostics_OnStage_Without_Detect
#return


dbpath="http://golem.fjfi.cvut.cz/dbase"



NULL="1>/dev/null"
Everything2NULL="1>/dev/null 2>/dev/null"
ThisEntity=$(basename $PWD)
dirigent_dir="/golem/Dirigent" #to be fixed, sorry


RSYNC="rsync --exclude '.*' --exclude '.*.*.kate-swp' --exclude Depository --exclude '*.ipynb_checkpoints' --exclude '*.ipynb_checkpoints/'"


# Logs layouts:
LogFunctionStart="LogIt Diving into .."
LogFunctionEnd="LogIt Ascending from .."
LogFunctionPassing=""
LogFunctionGoingThrough="LogIt Going through .." 


#Alias definitions.
shopt -s expand_aliases
alias IfDummyThenReturn='if [[ -f $SHM/Production/style ]] && [[ $(<$SHM/Production/style) == "dummy" ]]; then return;fi'

#============ END OF THE HEAD ============


function VerboseMode ()
{
    if (( $VerboseFlag )); then for i in $1; do echo Verbose mode ${BASH_SOURCE[2]}@$(basename ${BASH_SOURCE[1]}): $i: ${!i};done;fi
}

function Verbose ()
{
    for i in $1; do echo Verbose ${BASH_SOURCE[2]}@$(basename ${BASH_SOURCE[1]}): $i: ${!i};done
}

VerboseMode "bash_source pwd family instrument setup whoami"


# General access to all functions everywhere
# **********************************************************

function BasePath (){
local FullPath=$1
    echo $FullPath|sed 's/\/golem\/Dirigent\///g'
}

function Call(){ 
local where=$1
local what=$2
local param1=$3
local param2=$4
local param3=$5
local param4=$6
local param5=$7
local param6=$8
local param7=$9 #lepe

local script=$(dirname $where|xargs basename)

    [ -f $S HML/LastCallFunction ] && echo "what@where" >$SHML/LastCallFunction
    VerboseMode "where what param script"
    LogItColor 5 "from $(BasePath $PWD) ${FUNCNAME[1]}: $(BasePath $where)/$script $what  $param1 $param2 $param3 $param4 $param5 $param6 $param7 $param8 $param9"
    bash -c "cd $where/;source $script.sh;$what $param1 $param2 $param3 $param4 $param5 $param6 $param7 $param8 $param9"
    #LogItColor 5 "Call Return: $what@$where"

}


# Broadcasting things to the ..
# **********************************************************


function Broadcast(){
local where=$1
local what=$2

    LogItColor 10 "**$PWD: $(echo "$what@$where" | tr a-z A-Z) .... START"
    SubmitTokamakState "$what@$where"
    for FamilyFull in $where; do
        for Instrument in ${!FamilyFull}; do 
            Name=$(echo $Instrument|xargs dirname|xargs basename)
            #Setup=$(echo $Instrument|xargs basename)
            Family=${FamilyFull%_*}
            VerboseMode "FamilyFull Family Instrument Name"
            if  grep -Rq "function $what" $SW/$Family/$Instrument/$Name.sh $SW/Commons.sh $SW/$Family/$Instrument/../Universals.sh &> /dev/null #does this function is defined for this Instrument?
                then 
                LogItColor 5 "Musician:$Instrument .. START $what"
                (time1=$(date  '+%s');bash -c "
                cd $Family/$Instrument; source $Name.sh; #MUST be with SW/SHM0/SHMS
                $what "$@"";time2=$(date  '+%s');LogItColor 5 $Instrument .. END $what: $(($time2-$time1)) s;if [ -f "Production/FunctionTimes" ]; then  echo $what@$Instrument: $(($time2-$time1)) s >>Production/FunctionTimes;fi ) &
            fi
        done    
    done
	wait
    LogItColor 3 "**$PWD: $(echo "$what@$where" | tr a-z A-Z) .... END"
}






# Ping* issues
# **********************************************************


function PingAllDevices ()
{
local PingCounter=0
local NPingCounter=0
local AllDevices=$(<$SHM/Production/AllDevices)

    LogIt
    for TheDevice in $AllDevices; do 
        #echo $TheDevice;
        Name=$(echo $TheDevice|xargs dirname|xargs basename)
        Setup=$(echo $TheDevice|xargs basename)
        if [[ $(nslookup $Name) =~ "Name" ]]; then #If it has DNS name 
            timeout $PingTimeout  bash -c "
            while ! timeout 1 ping -c 1 -n $Name.golem &> /dev/null; do   
                printf '%s\n' 'waiting for $Setup @ $Name';
                echo false >/dev/shm/golem/Production/PingStatus
                sleep 1;
            done;
            printf '%s\n'  '$Setup @ $Name is online';
            echo true >/dev/shm/golem/Production/PingStatus" &
            let PingCounter++
        else
            printf '%s\n'  "$Setup @ $Name is not ping device"
            let NPingCounter++
        fi
    done
    wait
    echo ======================== 
    echo We have $PingCounter ping devices and $NPingCounter N/A ping devices
    if $(</dev/shm/golem/Production/PingStatus); then echo OK; true; else echo KO; false; fi
    
    
}


    
# Sessions issues
# **********************************************************


function SubmitTokamakState ()
{
local what=$1

    if [[ -d $SHML ]]
        then
            echo -n $what > $SHML/tokamak_state
            echo "$(date '+%H:%M:%S') $SHOT_NO:$what" >> $SHML/tokamak_state_log
        fi
   echo -n $what > /golem/production/tokamak_state

}


function PrepareEnvironment@SHM ()
{
local where=$1
local instrument
local family

LogTheFunctionAction

    for family in $Ensemble; do 
        for instrument in ${!family}; do 
            VerboseMode "family instrument"
            mkdir -p $where/${family%_*}/$instrument
            $RSYNC  -r $SW/${family%_*}/$instrument/ $where/${family%_*}/$instrument
            $RSYNC  $SW/${family%_*}/$instrument/../*.* $where/${family%_*}/$instrument/../ 2>/dev/null
        done 
    done    
}
    
# Common Discharge issues
# **********************************************************    

function GetReadyTheDischarge ()
{

    GeneralTableUpdateAtDischargeBeginning
}


function Analysis () # Just in case it is not defined
{
    :
}

function NoAnalysis () # Just in case it is not defined
{
    :
}

function PostDischargeAnalysis() # General, if not specified in the Diagnostics set-up
{
   
    if [[ ! -d DAS_raw_data_dir ]]; then ln -s $BasePath/Devices/$DAS DAS_raw_data_dir; fi

   #if [[ $(<$SHM0/Operation/Discharge/Basic/Parameters/analysis) != 'off' ]];
   if [[ $(<$BasePath/Operation/Dirigent/HigherPower/Parameters/analysis) != 'off' ]]; 
   then
        Analysis
   else
        NoAnalysis
   fi

    GenerateDiagWWWs $instrument $setup $DAS 
}    
    

# Log* functions Colors*
# **********************************************************


LogTheDeviceAction()
{
    LogIt The $(basename $whoami) action on stage with ${FUNCNAME[1]}
}

LogTheFunctionAction()
{
    LogIt The Function ${FUNCNAME[1]} on stage ..
}

function TrackIt()
{
:
    #LogIt Tracking .... $1
}


function LogItColor() 
{
    #https://en.wikipedia.org/wiki/ANSI_escape_code#Colors 
    #0 – Black.1 – Red.2 – Green.3 – Yellow.4 – Blue.5 – Magenta.6 (purpur) – Cyan.7 (azur) – White.
    (tput setaf $1
    if [ -e $SHMS/session_date ]; then # check if session has been opened 
        echo  $(date '+%H:%M:%S')\\t  ${FUNCNAME[1]} $2 >> $SHML/LocalLogBook
        printf "$(date +%H:%M:%S) #$(cat $SHM/shot_no) ${FUNCNAME[1]}:\t%s\n" "$2 $3 $4 $5 $6 $7 $8 $9  ${10} ${11} ${12} ${13} ${14} ${15}"|tee -a $SHML/GlobalLogbook 
    else 
        printf "$(date +%H:%M:%S) ${FUNCNAME[1]}:\t%s\n" "$2 $3 $4 $5 $6 $7  $8 $9  ${10} ${11} ${12} ${13} "
	fi
	
    if [ -e $SHM0/ShotLogBook/SessionDate ]; then # check if shot 
        printf "$(date +%H:%M:%S) #$(cat $SHM/shot_no)   ${FUNCNAME[1]}:\t%s\n" "$2 $3 $4 $5 $6 $7  $8 $9  ${10} ${11} ${12} ${13} ">> $SHM0/ShotLogbook 
#	else 
#        printf "$(date +%H:%M:%S) ${FUNCNAME[1]}:\t%s\n" "$1 $2 $3 $4"
	fi	
    tput sgr0)
}


function EchoItColor() 
{
    (tput setaf $1
        echo $2
    tput sgr0)

}

function LogIt() 
{
    if [ -e $SHMS/session_date ]; then # check if session has been opened 
        echo  $(date '+%H:%M:%S')\\t  ${FUNCNAME[1]} $1 >> $SHML/LocalLogBook
        printf "$(date +%H:%M:%S) #$(cat $SHM/shot_no) ${FUNCNAME[1]}:\t%s\n" "$1 $2 $3 $4 $5 $6 $7 $8 $9  ${10} ${11} ${12} ${13} ${14} ${15}"|tee -a $SHML/GlobalLogbook 
    else 
        printf "$(date +%H:%M:%S) ${FUNCNAME[1]}:\t%s\n" "$1 $2 $3 $4 $5 $6 $7  $8 $9  ${10} ${11} ${12} ${13} "
	fi
	
    if [ -d $SHM0 ]; then # check if shot 
        printf "$(date +%H:%M:%S) #$(cat $SHM/shot_no)   ${FUNCNAME[1]}:\t%s\n" "$1 $2 $3 $4 $5 $6 $7  $8 $9  ${10} ${11} ${12} ${13} ">> $SHM0/ShotLogbook 
#	else 
#        printf "$(date +%H:%M:%S) ${FUNCNAME[1]}:\t%s\n" "$1 $2 $3 $4"
	fi	
	
}	

function LogItnewline() 
{
    printf "\n"|tee -a $SHML/GlobalLogbook
}


# Error management
# **********************************************************

function critical_error()
{
    LogIt  "Critical error: $1 ... Stopped"
    if xhost >& /dev/null ; then 
        echo pack [label .error -text {Critical error: $1 ... stopped}]|wish
    fi
}	

# DBs* DB* Database management
# **********************************************************

# e.g.: golem@Dirigent>source Commons.sh ;CreateDiagnosticsTable MHDring_T



function CreateTable () # the function must be executed from appropriate directory
{
    local table_id=${diag_id,,} #tolowercase
    echo $family.$table_id
    $sql_password;
    psql -c 'CREATE TABLE 
                '$family'.'$table_id' (
                    shot_no integer UNIQUE
                    ,session_mission text
                    ,start_timestamp text
                    ,setup_id text
                    ,comment text
                    ,X_discharge_command text
                    )' -q -U golem golem_database
}



function GeneralTableUpdateAtDischargeBeginning () # the function must be executed from appropriate directory
{
    IfDummyThenReturn
    #if (( $DummyFlag )); then return;fi
#    local table_id=${1,,} #tolowercase
    local table_id=${family,,}.${diag_id,,} #tolowercase
    VerboseMode "pSQL update $table_id"
    echo $table_id


    $psql_password;psql -c "SELECT EXISTS (SELECT FROM $table_id)" -q -U golem -d golem_database &>/dev/null;
    if [ $? -eq 0 ]; then :;  else return; fi

    #return
  
    $psql_password;
    psql -c "INSERT 
                INTO $table_id (
                        shot_no 
                        )
                VALUES (
                    $(<$SHM/shot_no)
                    )" -q -U golem golem_database
    
    
    local WhatToDo=$(cat $SHMP/$table_id)
    if [[ -f $SHMP/$table_id ]]; then
        psql -c "UPDATE  
                    $table_id 
                 SET 
                    "$WhatToDo" 
                 WHERE shot_no IN(
                    SELECT max(shot_no) FROM $table_id
                            )" -q -U golem golem_database
    fi
    mkdir -p Parameters
    mkdir -p Results
    psql -c "SELECT
                 *
                FROM  
                    $table_id 
                WHERE shot_no IN(
                    SELECT max(shot_no) FROM $table_id
                            )" -x -q -U golem golem_database > AllParameters;
    grep -v RECORD AllParameters|sed 's/| /|/'|awk -F "|" '{print "echo \""$2"\" > Parameters/"$1}' -|bash 1>/dev/null 2>/dev/null
    cat $SHMP/$table_id > Parameters/WholeParametersDefinition
}

function AddColumnToDiagnosticsTable ()
{
    local diag_id=${1,,} #tolowercase

    $psql_password;
    psql -c "ALTER TABLE 
                diagnostics.'$diag_id' 
             ADD COLUMN '$2' '$3';" -q -U golem golem_database
}



#./Dirigent.sh -r DataBaseQuerry shot_no
# TODO: can not it be done the usual way? I.e., psql -c <sql_query>
#
function CurrentShotDataBaseQuerry
{
    $psql_password;
    echo "SELECT
                $1 
            FROM
                operation.discharges
            ORDER BY shot_no  
            DESC LIMIT 1;"|psql -qAt -U golem golem_database 
}

# shot_no=$(CurrentShotDataBaseQuerry shot_no) # Nefunguje na RASPs

# TODO: Can it be written in a clearer way? with psql -c <sql_query>?
function UpdateCurrentShotDataBase
{

    IfDummyThenReturn
    #if (( $DummyFlag )); then return;fi

    $psql_password;
    echo "UPDATE
            operation.discharges 
          SET 
            $1
          WHERE shot_no IN(
             SELECT max(shot_no) FROM operation.discharges
                        )"|psql -q -U golem golem_database 
}

# TODO: Can it be written in a clearer way? with psql -c <sql_query>?
function UpdateTable
{
table=$1
expression=$2

    IfDummyThenReturn

    #if (( $DummyFlag )); then return;fi

    $psql_password;
    echo "UPDATE 
              $table 
          SET 
              $expression
          WHERE shot_no IN(
              SELECT max(shot_no) FROM $table
                        )"|psql -q -U golem golem_database 
}

# TODO: Can it be written in a clearer way? with psql -c <sql_query>?
function UpdateTableColumn
{
table=$1
column=$2
expression=$3

    $psql_password;
    echo "UPDATE
              $table
          SET
              $column='$expression' 
          WHERE shot_no IN(
              SELECT max(shot_no) FROM $table
                        )"|psql -q -U golem golem_database 
}

# TODO: Can it be written in a clearer way? with psql -c <sql_query>?
function DistanceUpdateCurrentShotDataBase
{
    if (( $DummyFlag )); then return;fi
    echo "UPDATE
              operation.discharges
          SET
              $1
          WHERE shot_no IN(
              SELECT max(shot_no) FROM operation.discharges
                        )"|ssh golem "$psql_password;cat - |psql -q -U golem golem_database" 2>/dev/null
}

function RemoteUpdateCurrentSessionDataBase
{
    ssh Dirigent ''$psql_password';psql -c "UPDATE operation.sessions SET '$1' WHERE start_shot_no IN(SELECT max(start_shot_no) FROM operation.sessions)" -q -U golem golem_database'
}

# TODO: Can it be written in a clearer way? with psql -c <sql_query>?
function InsertCurrentShotDataBase()
{
    #if (( $DummyFlag )); then return;fi
        IfDummyThenReturn
    $psql_password;
    echo "INSERT INTO operation.discharges $1" | psql -q -U golem golem_database
    
}

function InsertCurrentShotDataBaseDischCommand()
{
# specific solution with single quotes:
        IfDummyThenReturn

    $psql_password;
    psql -c "UPDATE  operation.discharges SET \"X_discharge_command\"=E'`cat $SHMP/CommandLine|sed "s/'/\\\\\'/g"`' WHERE shot_no IN(SELECT max(shot_no) FROM operation.discharges)" -q -U golem golem_database
 }   

# TODO: Can it be written in a clearer way? with psql -c <sql_query>?
function InsertCurrentSessionDataBase()
{
    $psql_password;
    echo "INSERT INTO operation.sessions $1" | psql -q -U golem golem_database
    
}

# TODO: Can it be written in a clearer way? with psql -c <sql_query>?
function UpdateCurrentSessionDataBase()
{
    $psql_password;
    echo "UPDATE
               operation.sessions
          SET
               $1
          WHERE start_shot_no IN(
               SELECT max(start_shot_no) FROM operation.sessions
                            )"|psql -q -U golem golem_database 
}

# TODO: Can it be written in a clearer way? with psql -c <sql_query>?
function CurrentSessionDataBaseQuerry()
{
    $psql_password;
    echo "SELECT
                $1
          FROM
                operation.sessions
          ORDER BY
                start_shot_no DESC
         LIMIT 1;"|psql -qAt -U golem golem_database 
}

# TODO: Can it be written in a clearer way? with psql -c <sql_query>?
function WebShotDataBaseQuerry()
{
    WebRec $1: $($psql_password;echo "SELECT $2 FROM shots ORDER BY shot_no  DESC LIMIT 1;"|psql -qAt -U golem golem_database) $3
    #WebRec $1: $(export PGPASSWORD='rale';echo "SELECT $2 FROM shots ORDER BY shot_no  DESC LIMIT 1;"|psql -qAt -U golem golem_database) $3
}

#Problem lowercase etc.:
#psql -q -U golem golem_database -c 'UPDATE shots SET "Ip_mean"=3 WHERE shot_no IN(SELECT max(shot_no) FROM shots)'

#CurrentShotDataBaseQuerry '"D_integral_dose"'

# Time management
# **********************************************************
function mRelax() { sleep 0.1; }

function uRelax() { sleep 0.01; }

function Relax() { sleep 1; }

function CountDown {
local TimeLimit=$1;
local Purpose=$2

    echo Waiting $Purpose .. $TimeLimit s
    for i in $(seq  $TimeLimit -1 1); do echo -ne $i .., ;sleep 1;done 
    echo
}



# Audio
# **********************************************************

mplayer="ssh golem@Chamber.golem cvlc --play-and-exit "
# nutne pridat uzivatele: sudo usermod -a -G audio golem # aby sel i golem
#volume@Discharge: sudo amixer cset numid=1 -- 90 OR sudo alsamixer 
# files now locally at RASP
#sudo scp golem@golem:/golem/tools/sound_fx/*.mp3 /golem/tools/sound_fx/
#ssh pi@discharge 'sudo amixer cset numid=1 -- 90'
#mplayer="ssh golem@192.168.2.117 mplayer"
#mplayer=":"

#https://notevibes.com/cabinet.php
#English - Joanna

function Speaker()
{
local mp3file=$1

    if [[ $(<$SHM0/Operation/Dirigent/HigherPower/Parameters/voice) == 'off' ]]; then return;fi

    #if [[ -f "$SHM0/Operation/Discharge/Parameters/voice='off'" ]];then return;fi
    scp $SW/Management/Sounds/$mp3file.mp3 golem@Chamber.golem:audios/ >/dev/null
    ssh golem@Chamber.golem cvlc --play-and-exit audios/$(basename $mp3file).mp3 1>/dev/null 2>/dev/null 
}

# WWWs
# **********************************************************

function WWWmanagement
{
local what=$1

    bash -c "cd $SHM0/Infrastructure/Homepage/Basic; source Homepage.sh; MakeProgressingPage $what";
}

function GenerateDiagWWWs  
{
local instrument=$1
local setup=$2
local DASId=$3
local notebook=$4

#local diagpath="http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$diag_id"
local diagpath="Diagnostics/$instrument/$setup"
local devicepath="Devices/$DASId"
      
    echo "<tr>
    <td valign=bottom><a href=$diagpath/><img src=$diagpath/name.png  width='$namesize'/></a>&nbsp;</td>
    
    <!--Measurement start-->    
    
    <td valign=bottom>&nbsp;<a href=$diagpath/expsetup.svg><img src=$diagpath/setup.png /></a></td>
    <!--<td>
    <a href=$GWdiagpath title="GW path"><img src=$imgpath/logos/golem.png  width='$linkiconsize'/></a><br></br>
    <a href=$googlephotospath title="Photogallery@Google">$googlephotosicon</a><br></br>
    </td>-->

    <!--Digitatization start-->

 
    <td valign=bottom>&nbsp;<a href=$devicepath/das.html><img src=$devicepath/das.jpg  width='$iconsize'/></a></td>
    <!--<td>
    <a href=$devicepath/das.html title="Manual$devicepath">$manualicon</a><br></br>
    </td>-->
    

    
    <td valign=bottom>&nbsp;<a href=$devicepath/ScreenShotAll.png><img src=$devicepath/rawdata.jpg  width='$iconsize'/></a></td>
    <td>&nbsp;
    <a href=$gitlabpath/$devicepath/ title="Gitlab4$devicepath">$gitlabicon</a><br></br>
    <a href=$devicepath/ title="Data directory">$diricon</a><br></br>
    </td>">diagrow_$instrument.html;
    
    
    if [ -e $SHM0/Diagnostics/$instrument/$setup/icon-fig.png ]; then 
        echo "<!--Analysis start-->
        
        <td valign=bottom><a href=$diagpath/icon-fig.png><img src=$diagpath/graph.png  width='$iconsize'/></a></td>
        <td>&nbsp;<a href=$diagpath/Parameters/ title="Parameters">$parametersicon</a><br></br>
        
        <a href=$gitlabpath/Diagnostics/$diag_id/$setup_id/$notebook title="JupyterNotebook$diagpath">$pythonicon</a>
        $rightarrowicon
        <a href=$diagpath/analysis.html title="Analysis$diagpath">$resultsicon</a><br></br>
        <a href=$gitlabpath/$diagpath/ title="Gitlab4$diagpath">$gitlabicon</a>
        
        <a href=$diagpath/ title="Directory">$diricon</a>
        <a href=$dbpath/Diagnostics/$diag_id/ title="Database">$sqlicon</a>
        </td>
        <!--Analysis end-->" >>diagrow_$instrument.html;
    fi
    echo "</tr>" >>diagrow_$instrument.html;
    
    
    echo "<center><h1>The GOLEM tokamak Basic Diagnostics setup</h1>
    <h2>Photo</h2>
    <img src="/_static/figs/DAS-d_s.jpg" width="25%"></img><br/>
    <h2>Relevant excerpt from the manual</h2>" > setup.html;
    for j in $(seq 0 2); do
        echo "<img src=http://golem.fjfi.cvut.cz/wiki/Education/GMinstructions/extracts/Extracts/Diagnostics/output-$j.jpg width='50%'></img>" >> setup.html;
    done
    
    
}

function GenerateAnalysisWWWs  
{

local forwhom=$1
local diagpath=$(dirname $forwhom)
local notebook=$2
      
    echo "<tr>
    <td valign=bottom><a href=$diagpath/><img src=$diagpath/name.png  width='$namesize'/></a></td>
    
    <td valign=bottom><a href=$diagpath/analysis.html><img src=$diagpath/graph.png  width='$iconsize'/></a></td>
    <td>
    <a href=$gitlabpath/$diagpath/$notebook title="JupyterNotebook$diagpath">$pythonicon</a>
    $rightarrowicon
    <a href=$diagpath/analysis.html title="Analysis$diagpath">$resultsicon</a><br></br>
    <a href=$diagpath/ title="Directory">$diricon</a>
    <a href=$dbpath/Diagnostics/$diag_id/ title="Database"><img src=$imgpath/postgresql.webp  width='$linkiconsize'/></a>
    </td></tr>" >diagrow_$(basename $forwhom .sh).html;
    
    
}






# Others
# **********************************************************

function WebRecDas()
{
    echo $1 >> das.html
}

function Flag() # Just for SW tuning
{
    echo Waving from ${FUNCNAME[2]} 
}



function rsyncRASPs()
{
       for Dev in RasPi3-c/Bt_Ecd RasPi4-a/Chamber;do 
       if timeout 1 ping -c 1 -n $(dirname $Dev).golem &> /dev/null
        then
          echo Doing: $Dev
          cd /golem/Dirigent/Devices/ITs/$Dev; rsync -v *.sh ../../Drivers/Rasp-Commons.sh $SW/Commons.sh $(dirname $Dev):
 #         cd /golem/Dirigent/Devices/ITs/$Dev;source  $Dev.sh;PrepareSessionEnv@SHM;cd $OLDPWD  
        fi
    done
}

function KillAllGMs(){
#./Dirigent.sh -k
    killall -u golem
}



sanb()
{
sleep 3;ls
}

# source Commons.sh ;DischargeEntityTest /golem/Dirigent/Diagnostics/BasicDiagnostics/DetectPlasma.sh

DischargeEntityTest()
{
local SW_dir=/golem/Dirigent
#local Where=$(dirname $1|sed 's/\/golem\/svoboda\/Dirigent\///g')
local Where=${1#*"Dirigent/"}
local Where=${Where#*"ActualShot/"}
local Where=$(dirname $Where)
local What=$(basename $1)


cd $SHM0/$Where
echo pwd: $(pwd)
echo Copying $What $Where ...
echo ====================
cp -v $SW/$Where/*.* .
source $What
echo "Copy DAS $DAS ... (in case)"
echo ====================
cp -v $SW/Devices/$(dirname $DAS)/*.* $SHM0/Devices/$(dirname $DAS)/

GetReadyTheDischarge
TriggerManagement GetReadyTheDischarge
Relax
GeneralDAScommunication $DAS Arming
Arming
Relax
for i in $(seq 1 2); do echo wait $i;sleep 1;done # in NI case
TriggerManagement Trigger
Relax
TriggerManagement SecurePostDischargeState
PostDischargeAnalysis;
ll /dev/shm/golem/ActualShot/Devices/$(dirname $DAS)/; 
ll

cd $OLDPWD
echo pwd: $(pwd)
}

#including ".sh"
# e.g. source Commons.sh ;OpenSessionSomewhere /golem/Dirigent/Devices/Oscilloscopes/RigolMSO5104-a/Stabilization.sh
# e.g. source Commons.sh ;OpenSessionSomewhere /golem/Dirigent/Devices/Oscilloscopes/TektrMSO56-a/BasicDiagnostics.sh 
# e.g. source Commons.sh ;OpenSessionSomewhere /golem/Dirigent/Devices/Oscilloscopes/TektrMSO58-a/21_RunAways_JCetal.sh 
OpenSessionSomewhere()
{
local SW_dir=/golem/Dirigent
local Where=${1#*"Dirigent/"}
local Where=${Where#*"ActualShot/"}
local Where=$(dirname $Where)
local What=$(basename $1)
#local Where=$(dirname $1|sed 's/\/golem\/svoboda\/Dirigent\///g')
#local What=$(basename $1)

echo $Where
echo $What
#return

cd $Where
source $What;OpenSession;

cd $OLDPWD

}

#cp /golem/Dirigent/Commons.sh .;source Commons.sh ;PostDischargeAnalysisTest file:///golem/shm_golem/ActualShot/Diagnostics/BasicDiagnostics4TrainCourses/StandardDAS-b.sh

PostDischargeAnalysisTest()
{
local SW_dir=/golem/Dirigent
local Where=${1#*"Dirigent/"}
local Where=${Where#*"ActualShot/"}
local Where=$(dirname $Where)
local What=$(basename $1)

cd $SHM0/$Where
echo pwd: $(pwd)
echo Copying $What $Where ...
echo ====================
cp -v -r $SW/$Where/* .
source $What

PostDischargeAnalysis;
ll

cd $OLDPWD
#echo pwd: $(pwd)
}


#e.g. source Commons.sh ;AnalysisReconstruction Diagnostics/PetiProbe/21_PetiProbe_KHetal.sh 35468

function AnalysisReconstruction()
{
    cd $SW

    local SW_dir=/golem/Dirigent
    local What=$1
    local shot_no=$2
    
    local PureWhat=${What#*"Dirigent/"}
    echo $PureWhat
    
    cd /golem/database/operation/shots/$shot_no/$(dirname $PureWhat)
    echo cd /golem/database/operation/shots/$shot_no/$(dirname $PureWhat)
    cp -r $SW/$(dirname $PureWhat)/* .
    source $(basename $PureWhat); Analysis
    
    cd $SW




}



#cd /golem/database/operation/shots/$(cat /golem/database/operation/shots/0/shot_no)/Devices/Oscilloscopes/RigolMSO5104-a/;source Stabilization.sh ;GetOscScreenShot ;ll

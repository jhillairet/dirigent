BasePath=../..;source $BasePath/Commons.sh

locality=Diagnostics/PetiProbe
filetoreplace=21_PetiProbe_KHetal.ipynb
script=21_PetiProbe_KHetal.sh
command=Analysis 

do=echo #to see/test
do= #to run

  for i in 35464 35466 35467 35468 35469 35470 35471 35472 35473; do
#    for i in 35472 ; do
    echo Doing ... $i;
    echo '###############################'
    echo Backup the old version ....
    $do cp $shot_dir/$i/$locality/$filetoreplace $shot_dir/$i/$locality/$filetoreplace.VersionUpTo`date '+%y%m%d'`
    $do cp $shot_dir/$i/$locality/$script $shot_dir/$i/$locality/$script.VersionUpTo`date '+%y%m%d'`
    echo Replacement act ...
    $do cp $dirigent_dir/$locality/$filetoreplace $shot_dir/$i/$locality/$filetoreplace
    $do cp $dirigent_dir/$locality/$script $shot_dir/$i/$locality/$script
    echo  And run a new version ...
    $do cd $shot_dir/$i/$locality/
    $do source `basename $script`;$do $command
    $do cd ~-
    echo '-------------------------------------------------------------------------------------'

  done

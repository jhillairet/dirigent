#!/usr/bin/python2

import re

#Constants
pi =  3.1415927
Mu0=4*pi*1e-7
kB=1.3806503e-23
eV=1.60217646e-19
RoomTemperature=300  #[K]
pressure = 0  # neznam !!! 

#Basics
MajorRadius=0.4
MinorRadius=0.1
Aspect = MinorRadius/MajorRadius

LimiterPosition=0.085  # [m] max plasma radius    0.05 for ring with Mach probes
MeanPlasmaRadius=0.060   # [m] mean plasma radius without any stabilization 
PlasmaVolume=pi*LimiterPosition**2*2*pi*MajorRadius
ToroidalMagneticFieldCoilInductance=2.8e-3  #[H] PhD. thesis J. Brotankova

#Energetics (1.07.14):
TororoidalMagneticFieldCapacitor=s=re.findall('\\\\def\\\\value\{(.*)\}', open('/golem/database/wiki/wikidata/Infrastructure/ToroidalMagneticFieldCircuit/Capacitor/ActualCapacity/PhysQuantDescript.page', 'r').read())[0]
#print (TororoidalMagneticFieldCapacitor)
BreakDownElectricFieldCapacitor=8*450e-6  #[F]
CurrentDriveElectricFieldCapacitor=re.findall('\\\\def\\\\value\{(.*)\}', open('/golem/database/wiki/wikidata/Infrastructure/CurrentDriveFieldCircuit/Capacitor/ActualCapacity/PhysQuantDescript.page', 'r').read())[0]
StabilizationCapacitor=6*450e-6  #[F]

TororoidalMagneticFieldTrigger = 5e-3  #[s]


  #DAS calibration
  #LineCount=40000    #scopes DAS file line count
  #TriggerLine=3500   #plasma discharge is triggered at data row
  #DeltaT=1e-3       #[ms] DAS time
  #DeltaTs=1e-6       #[s] DAS time
  #LineCount=4000    #scopes DAS file line count
  #TriggerLine=350   #plasma discharge is triggered at data row
  #DeltaT=10e-3       #[ms] DAS time
  #DeltaTs=10e-6       #[s] DAS time
#LineCount=40000    #scopes DAS file line count
#TriggerLine=3500   #plasma discharge is triggered at data row
#DeltaT=1e-3       #[ms] DAS time
#DeltaTs=1e-6       #[s] DAS time

Bt_calibration=70.42   #[T/Vs] 0311 podle Tomase Markovice
RogowskiCalibration=5.3e6   #[A/V(10us)] 03/2010, CASTOR calibration (Mr. F.Zacek), 10 us vzorkovani, p. Zacek uvadi 5.3 e6 A/Vs
UloopCalibration=5.5
#ChamberResistance=7.44e-3   #[Ohm]
#ChamberInduktance=0.60e-6   #[H]

#$ChamberResistance=7.44e-3; #[Ohm]
ChamberResistance=  9.7e-3  # 5e-4; #  9.7e-3  #9.44e-3; #[Ohm]
ChamberInduktance=0.55e-6; #[H]

MaxTransformatorSaturation = 0.17  # V.s

Zeff = 3  # stupid estimation of Z effective 
nu = 2 # current profile is j_O*(1-(r/a)^2)^nu

  #PlasmaDetect
#PlCurrentRise=500   #[A]
#PlCurrentIncrTime=1e-3
#PlCurrentSurv=1e-3

  #Others
shotdir='/golem/database/operation/shots'



#calibration data of the mirnov coils for plasma position [Tomas Odstrcil
#calib_mirnov_coils = [-234.9, -234.9, -234.9, -234.9]  #T/(V*s)   1.1.2013 zmeneno 
calib_mirnov_coils = [ 0, 0, 0, 0 ]  #T/(V*s) 

    

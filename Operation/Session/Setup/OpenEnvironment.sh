
kate -n /golem/Dirigent/Dirigent.sh /golem/Dirigent/Commons.sh /golem/Dirigent/session.setup /golem/shm_golem/ActualSession/Operation/Dirigent/HigherPower/operation.dirigent &
google-chrome --password-store=basic --user-data-dir=$HOME/.config/google-chrome-Dirigent --new-window http://golem.fjfi.cvut.cz/shots/0 http://golem.fjfi.cvut.cz/wiki/ &
konsole --title "Dirigent" --profile Dirigent --tabs-from-file /golem/Dirigent/Operation/Session/Setup/DirigentTabs & 
krusader --left /golem/Dirigent --right /golem/Dirigent/Setups  &
# Stay chrome on the main display
sleep 3
wmctrl -i -r `wmctrl -l|awk '{if ( $2 == 1 ){print $0}}'|grep Chrome|awk '{print $1}'` -e 0,1366,1920,1920,1080
sleep 1

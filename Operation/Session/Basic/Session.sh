#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh


DeviceList="ITs/NAS/Golem ITs/RasPi4-c/ControlPanel"
#Drivers="remote_web_app"
#Analysis="Homepage"


# Session stuff
# **********************************************************







function SetupSession ()
{
      cd $SW;
      cp session.setup $SHM/session.setup
      #BasePath=../../..;source $BasePath/Commons.sh
      cp `realpath session.setup` $SHM/
      mkdir -p $SHMS;$SHM/Tags
      { echo "Default GOLEM setup (sourced below 'source Default.setup'):"; cat Default.setup;echo "Current session setup:";cat session.setup; } > $SHM/whole.setup
      PrepareEnvironment@SHM $SHMS # Get ready the whole dir struct @SHMS
      Broadcast "$Ensemble" PrepareSessionEnv@SHM
      cd $SHMS;
      Broadcast  "$Ensemble" OpenSession
      SubmitTokamakState "idle" 
      tail -f $SHML/GlobalLogbook
}

function PrepareSessionEnv@SHM()
{
   PrepareEnvironment@SHM $SHMS
   cp $SW/Dirigent.sh $SW/Commons.sh $SHMS/
   cp $SHM/*.* $SHM/* $SHMS/ 
   mkdir -p $SHML; #Logbooks
   mkdir -p $SHMS/Production;touch Production/FunctionTimes
   chmod -R g+rwx $SHM
   mkdir -p $SHMS/SessionLogBook
   SubmitTokamakState "idle" 
   rsync  -r $SW/Management/ActualParameters $SW/Management/DefaultParameters $SHM/Management/

}	



function OpenSession(){


    echo $((`CurrentShotDataBaseQuerry "shot_no"`+1))> $SHM/session_id 
    cat $SHM/session_id > $SHMS/session_id
    date "$date_format" > $SHMS/session_date
    echo $Mission > $SHMS/session_mission
    cp $SHM/session_setup_name $SHML/
    
    

    $LogFunctionStart
    LogIt Date: $(< $SHMS/session_date);
    LogIt "Session setup:"
    LogIt "=================="
    LogIt Session ID: $(< $SHM/session_id);
    LogIt "=================="

    zip -qr $SHMS/GolemCntrl * -x '*.git*'

    # pSQL Database 
    InsertCurrentSessionDataBase "(start_shot_no, start_timestamp, session_mission, session_setup, onstage_wave, offstage_wave, staff_list, infrastructure, analysis) VALUES ($(< $SHM/session_id),'$(< $SHMS/session_date)', '$Mission', '$(<$SHM/session_setup_fullname)',  '$Diagnostics_OnStage', '$Diagnostics_OffStage','$Staff','$Infrastructure','$Analysis');" 

      
    EchoItColor 1 "Do not forget to make Test dlouhodobe stability (start Radiometr)"
    EchoItColor 1 "Do not forget to open gas valves at every session beginning! (naprogramuj test rozdilu ocekavani proti realite)"
    EchoItColor 1 "When necessary, do WG calibration !!"
    #FakeWGcalH2 # Fake calibration ToDo ...

    OpenJson
    
    SubmitTokamakState "idle" 
       
    # ssh pi@discharge 'sudo amixer cset numid=1 -- 90' #set volume of the speaker (je to nekde jinde)
  
}

function ResetSession ()
{
      ssh -Y golem@Chamber.golem  "killall -u golem"
      ssh -Y golem@golem "killall xterm"
      rm -rf /dev/shm/golem;
}


function OpenJson()
{
    python3 /golem/Dirigent/Operation/RemoteOperation/remote_web_app/json_status4remote.py &
    disown `echo $!`
}




function CloseSession(){
    UpdateCurrentSessionDataBase "end_shot_no=`cat $SHM/shot_no`, end_timestamp='`date "$date_format"`', end_chamber_pressure=`cat $SHML/ActualChamberPressuremPa`"
    
    rsync -a $SHM/ActualSession/ /golem/database/operation/sessions/`CurrentSessionDataBaseQuerry "start_shot_no"`
    rm nohup.out 
    
    SubmitTokamakState "offline" 
    Relax
    pkill -f json_status4remote.py 
    killall tail >/dev/null
    killall python3 >/dev/null
    rm -rf $SHM; # Final farewell
    

}



function ShutDown ()
{      
        cd $SW/;./Dirigent.sh --poff
        Broadcast "$Ensemble" Sleep-downCall
        KillAllGMtasksEverywhere
        pkill -f json_status4remote.py
        rm -rf $SHM;
}

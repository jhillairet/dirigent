Diag=$1
ShotNo=$2
DoIt=$3 #true or false
Verbose=$4 #true or false

colspan=4
BP=/golem/database
nb_id="phase_detection_density"
icon_size="200x150" 



cp /golem/Dirigent/$Diag/* . 2>/dev/null
#Vacuum_shot=$(</golem/database/operation/shots/$ShotNo/Production/Parameters/vacuum_shot)

if $Verbose; then
    echo Diagnostics: $Diag 
    echo ShotNo: http://golem.fjfi.cvut.cz/shots/$ShotNo
    echo RetroRow: http://golem.fjfi.cvut.cz/shots/$ShotNo/$Diag/RetroRow.html
fi

DATE=`date '+%d%m%y%H%M'`

mkdir -p RetroBackup.$DATE
cp *.*  RetroBackup.$DATE/



echo "<tr>
<td colspan=$colspan>\
<h2 id=$ShotNo><a href=/shots/$ShotNo>#$ShotNo</a></h2> \
(<a href=/shots/$ShotNo/$Diag>$ShotNo/$Diag</a>),
($(< /golem/database/operation/shots/$ShotNo/shot_date),
$(< /golem/database/operation/shots/$ShotNo/shot_time)),
</td></tr>">RetroRow.html
echo "<tr><td colspan=$colspan>$(< /golem/shots/$ShotNo/Production/Parameters/CommandLine)</td></tr>" >>RetroRow.html
echo "<tr><td colspan=$colspan>t<sub>pl</sub>=$(< /golem/shots/$ShotNo/Diagnostics/BasicDiagnostics/Results/t_plasma_duration) ms</td></tr>" >>RetroRow.html
echo '<tr><td>' >>RetroRow.html
echo "
<a href=/shots/$ShotNo/Diagnostics/BasicDiagnostics/icon-fig.png><img src=/shots/$ShotNo/Diagnostics/BasicDiagnostics/graph.png alt='Basic Diagnostics' ></a></td>
<!--<td><img src=/shots/$ShotNo/$Diag/graph.png></td>-->" \
>>RetroRow.html


if $DoIt; then
    sed -i "s/shot_no = 0/shot_no = $ShotNo/g" $nb_id.ipynb
   jupyter-nbconvert --execute $nb_id.ipynb --to html --output analysis.html|& tee -a Output.log
    convert -resize $icon_size icon-fig.png graph.png
fi

echo "<td><a href=/shots/$ShotNo/$Diag/icon-fig.png><img src=/shots/$ShotNo/$Diag/graph.png ></a></td>" >>RetroRow.html
echo "<td><a href=/shots/$ShotNo/$Diag/RetroBackup.$DATE/icon-fig.png><img src=/shots/$ShotNo/$Diag/RetroBackup.$DATE/graph.png></a></td>" >>RetroRow.html


echo "<td>\
<a href=/shots/$ShotNo/$Diag/analysis.html>analysis</a><br></br>\
<a href=/shots/$ShotNo/$Diag/Output.log>Output.log</a>\
</td>" >>RetroRow.html
echo "</tr>" >>RetroRow.html
#cat RetroRow.html















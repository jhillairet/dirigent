#Nutno volat http://golem.fjfi.cvut.cz/RetroActions/XY, a ne https:// (kvuli obrazkum)
shots=$2
RunName=$1
DoMatlab=$3
id=0123MHDs
BP=/golem/database
diag=Diagnostics/MHDring_TM
cesta=$BP/www/RetroActions/$id/"$RunName"_`date '+%d%m%y'`
soubor=index.html
echo Je to zde: http://golem.fjfi.cvut.cz/RetroActions/$id/`basename $cesta`


mkdir -p $cesta;cd $cesta
mkdir -p Resources
cp /golem/Dirigent/$diag/* ../*.* Resources/ 2>/dev/null

echo "<html><body><h2>$diag <a href=Resources>Retroaction</a> <a href=http://golem.fjfi.cvut.cz/RetroActions/$id>Base dir: $id</a></h2>" > $soubor; 
echo "Command line: bash ${BASH_SOURCE} $@<br/>" >> $soubor;
echo "Date:  `date`<br/>" >> $soubor;
echo "Parameters: From:#$from, To:#$to<hr><table>" >> $soubor;
echo "<tr><th>Basic Diag</th><th>offset</th><th>MHD_activity</th><th>Spectrogram</th><th>q</th><th>animation</th><th>pictures</th><th>MHD link</th></tr>" >> $soubor;

function IndivShot ()
{
local ShotNo=$1
    cmd=$(< /golem/shots/$ShotNo/Production/Parameters/CommandLine);cmd=`echo ${cmd##*vacuum_shot=}`;Vacuum_shot=`echo ${cmd%%\"*}`
    if \
    [[ -d "$BP/operation/shots/$ShotNo/$diag" ]] && \
    [[ -d "$BP/operation/shots/$Vacuum_shot/$diag" ]] && \
    [[ -f "/golem/shots/$ShotNo/Diagnostics/BasicDiagnostics/Results/b_plasma" ]] && \
    [[ $(< /golem/shots/$ShotNo/Diagnostics/BasicDiagnostics/Results/b_plasma) != "0.000" ]] && \
    [[ $(< /golem/shots/$ShotNo/Production/Parameters/CommandLine) =~ "vacuum_shot" ]] && \
    [[ ! $(< /golem/shots/$ShotNo/Production/Parameters/CommandLine) =~ "vacuum_shot=\"" ]]; then 
        cp ../DischargeScript.bash $BP/operation/shots/$ShotNo/$diag/
        cd $BP/operation/shots/$ShotNo/$diag
        bash DischargeScript.bash $diag $ShotNo $DoMatlab
        cd $OLDPWD
        cat $BP/operation/shots/$ShotNo/$diag/RetroRow.html >> $soubor; 
    fi
}


for ShotNo in $shots; do echo $ShotNo:; 
    IndivShot $ShotNo
done


echo "</table><hr/><h3><u>Global script:</u></h3><pre>">> $soubor;
cat ../GlobalScript.bash|sed 's/</\&lt;/g'|sed 's/>/\&gt;/g' >> $soubor;
echo "</pre><hr/><h3><u>Discharge script:</u></h3><pre>">> $soubor;
cat ../DischargeScript.bash|sed 's/</\&lt;/g'|sed 's/>/\&gt;/g' >> $soubor;
echo "</pre></body></html>" >> $soubor;
cd $OLDPWD


#bash GlobalScript.bash snb1 "`seq 39421 39773`"

Diag=$1
ShotNo=$2
DoIt=$3 #true or false
Verbose=$4 #true or false
wwwpath=$5

colspan=9
BP=/golem/database


cp /golem/Dirigent/$Diag/* . 2>/dev/null
#Vacuum_shot=$(</golem/database/operation/shots/$ShotNo/Production/Parameters/vacuum_shot)
cmd=$(< /golem/shots/$ShotNo/Production/Parameters/CommandLine);cmd=`echo ${cmd##*vacuum_shot=}`;Vacuum_shot=`echo ${cmd%%\"*}`
#Correction:
echo $Vacuum_shot >/golem/database/operation/shots/$ShotNo/Production/Parameters/vacuum_shot

if $Verbose; then
    echo Diagnostics: $Diag 
    echo ShotNo: http://golem.fjfi.cvut.cz/shots/$ShotNo
    echo Vacuum shot: http://golem.fjfi.cvut.cz/shots/$Vacuum_shot
    echo RetroRow: http://golem.fjfi.cvut.cz/shots/$ShotNo/$Diag/RetroRow.html
fi


echo "<tr>
<td colspan=$colspan>\
<h2 id=$ShotNo><a href=/shots/$ShotNo>#$ShotNo</a> (<a href=http://golem.fjfi.cvut.cz/RetroActions/$wwwpath\#$ShotNo>link here</a>)</h2> \
(<a href=/shots/$ShotNo/$Diag>$ShotNo/$Diag</a>),
($(< /golem/database/operation/shots/$ShotNo/shot_date),
$(< /golem/database/operation/shots/$ShotNo/shot_time)),
<a href=/shots/$Vacuum_shot>Vacuum shot: $Vacuum_shot</a><br>
</td></tr>">RetroRow.html
echo "<tr><td colspan=$colspan>$(< /golem/shots/$ShotNo/Production/Parameters/CommandLine)</td></tr>" >>RetroRow.html
echo "<tr><td colspan=$colspan>t<sub>pl</sub>=$(< /golem/shots/$ShotNo/Diagnostics/BasicDiagnostics/Results/t_plasma_duration) ms</td></tr>" >>RetroRow.html
echo '<tr><td>' >>RetroRow.html
echo "
<a href=/shots/$ShotNo/Diagnostics/BasicDiagnostics/icon-fig.png><img src=/shots/$ShotNo/Diagnostics/BasicDiagnostics/graph.png alt='Basic Diagnostics' ></a></td>
<!--<td><img src=/shots/$ShotNo/$Diag/graph.png></td>-->" \
>>RetroRow.html

FIGS="offset.png MHD_activity.png Spectrogram.png 	q.png animation.gif[0] pictures.png"

if $DoIt; then
    cp Script.m Script.sf
    sed "s/ShotNo = 0/ShotNo = $ShotNo/g;s/vacuum_shot = 0/vacuum_shot = $Vacuum_shot/g" Script.sf > Script.m
    matlab -nosplash -nodesktop -r Script_publish |& tee -a MatlabOutput.log
fi

for fig in $FIGS; do
    #echo $fig
    if $DoIt; then convert -resize 180 $fig ${fig%%.*}-icon.png;fi
    echo '<td><a href='/shots/$ShotNo/$Diag/$fig'><img src='/shots/$ShotNo/$Diag/${fig%%.*}-icon.png' alt='$fig'/></a></td>' >>RetroRow.html
done   

if $DoIt; then cp pictures-icon.png graph.png; cp pictures.png icon-fig.png; fi

sed -i 's/\[0\]//g' RetroRow.html #animation.gif[0]->animation.gif

echo "<td>\
<a href=/shots/$ShotNo/$Diag/html/Script.html>Matlab html output</a><br></br>\
<a href=/shots/$ShotNo/$Diag/MatlabOutput.log>MatlabOutput.log</a>\
</td>" >>RetroRow.html
echo '<meta http-equiv="Refresh" content="0; url=/shots/'$ShotNo'/'$Diag'/html/Script.html"/>' >analysis.html
echo "</tr>" >>RetroRow.html
#cat RetroRow.html















/**
 * This JavaScript file is used for dynamic manipulation 
 * of web interface and virtual 3D model.
 *
 * Here are implemented all necessary JS functions.
 * Every function has its description.
 *
 * @author Tatiana Okonechnikova <okonetat@fel.cvut.cz>
 * @author Ondrej Grover <ondrej.grover@gmail.com>
 */


// Namespace object
var TokamakModel = {
    viewPrefix: "building__view_",
    animatedViewPrefix: "building__time_",
    activeAnimatedViewId: "",
    tokamakSwitchPrefix: "tokamak__switch-",
	  components: ["bigcrossN",
	                "bigcrossS",
	                "chamber",
	                "coilsconnection",
	                "interferwave",
	                "magelectrons",
	                "magelectronses",
	                "stand",
	                "smallcrossE",
	                "smallcrossN",
	                "smallcrossS",
	                "smallcrossW",
	                "toroidcoils",
	                "transcoils",
	                "transcore",
	                "gas_green",
	                "gas_red",
	                "airPump",
	                "ring_yellow",
	                "ring_red",
	                "ring_red_transp",
	                "ring_grey"]
}

//---------
// Utility functions
//---------

// activate given view
// @param viewId id of view without 'view_' prefix
TokamakModel.setView = function (viewId) {
	  document.getElementById(TokamakModel.viewPrefix
                            + viewId).setAttribute('set_bind', 'true');
    if (TokamakModel.activeAnimatedViewId.length > 0) {
        document.getElementById(TokamakModel.animatedViewPrefix +
                                TokamakModel.activeAnimatedViewId
                               ).setAttribute('enabled', 'false');
    }
}

// activate animated view time loop
// @param timeLoopId of animated view without 'time_' pefix
TokamakModel.activateAnimatedView = function (timeLoopId) {
	  var timeSensor = document.getElementById(TokamakModel.animatedViewPrefix
                                             + timeLoopId);
    timeSensor.setAttribute('enabled', 'true');
    timeSensor.setAttribute('startTime', Date.now()/1000);
    TokamakModel.activeAnimatedViewId = timeLoopId;
}

// select component by switching desired component in Switch
// @param componentId id of Switch with tokamak components without 'switch-' prefix
// @param switchIndex index of component to switch to (show) in the Switch element, "0" is first, "-1" is none
TokamakModel.switchComponent = function (componentId, switchIndex) {
	  document.getElementById(TokamakModel.tokamakSwitchPrefix
                            + componentId).setAttribute("whichChoice", switchIndex);
}

// show component by switching to first component in Switch
// @param componentId id of Switch with tokamak components without 'switch-' prefix
TokamakModel.showComponent = function (componentId) {
	  TokamakModel.switchComponent(componentId, "0");
}

// hide component by switching to -1 component in Switch
// @param componentId id of Switch with tokamak components without 'switch-' prefix
TokamakModel.hideComponent = function (componentId) {
	  TokamakModel.switchComponent(componentId, "-1");
}


//--------------
// Views
//---------------


TokamakModel.viewTokamak = function () {
    TokamakModel.setView("tokamak");
}


TokamakModel.viewTokamakFront = function() {
    TokamakModel.setView('front_tokamak');
}

TokamakModel.viewTokamakControl = function () {
    TokamakModel.setView("control_tokamak");
}


//-----------------------------------------------------------
// Animation walks using X3D nodes: <Viewpoint />
//									<TimeSensor />
// 									<PositionInterpolator />
// 									<Route />
//-----------------------------------------------------------


TokamakModel.walkAround = function () {
	  TokamakModel.setView("around");
    TokamakModel.activateAnimatedView("animatedView");
}

TokamakModel.walkFromToInfra = function () {
    TokamakModel.setView("way");
    TokamakModel.activateAnimatedView("animatedView");
}

TokamakModel.walkInsideInfra = function () {
	  TokamakModel.setView('aroundInfr');
    TokamakModel.activateAnimatedView("animatedView1");
}


//-----------------------------------------------------------
// Animation walk inside the chamber - switching viewpoints
// X3D node: <Viewpoint />
// @method JS setTimeout()
//-----------------------------------------------------------
TokamakModel.walkInsideChamber = function () {
    for (i = 1; i <= 11; i++) {
        setTimeout(function () {
            TokamakModel.setView("inside_chamber" + (i < 10 ? "0" : "") + i.toString(), i*1000);
        })
    }
}


// ------------
// Showing and hiding component selections
// ------------

TokamakModel.hideTokamak = function () {
    TokamakModel.components.forEach(TokamakModel.hideComponent);
}


TokamakModel.showTokamak = function () {
    TokamakModel.components.forEach(TokamakModel.showComponent);
	  TokamakModel.hideComponent("interferwave");
}


//-----------------------------------------------------------
// Manipulation with avatar navigation modes
// X3D node: <NavigationInfo />
//-----------------------------------------------------------
function chooseNavigModes() {
	var setup_navigmodes = document.getElementById("setup-navigmodes");
	var setup_avatar = document.getElementById("building__avatar");
	if (setup_navigmodes.value == 'any') { setup_avatar.setAttribute("type", "ANY"); }
	if (setup_navigmodes.value == 'examine') { setup_avatar.setAttribute("type", "EXAMINE"); }
	if (setup_navigmodes.value == 'fly') { setup_avatar.setAttribute("type", "FLY"); }
	if (setup_navigmodes.value == 'game') { setup_avatar.setAttribute("type", "GAME"); }
	if (setup_navigmodes.value == 'lookat') { setup_avatar.setAttribute("type", "LOOKAT"); }
	if (setup_navigmodes.value == 'none') { setup_avatar.setAttribute("type", "NONE"); }
	if (setup_navigmodes.value == 'turntable') {
		setup_avatar.setAttribute("type", "TURNTABLE");
		setup_avatar.setAttribute("typeParams", "0.0 0.0 0.2 1.4");
	}
	if (setup_navigmodes.value == 'walk') { setup_avatar.setAttribute("type", "WALK"); }
}

//-----------------------------------------------------------
// Manipulation with avatar transition types between viewpoints
// X3D node: <NavigationInfo />
//-----------------------------------------------------------
function chooseTransTypes() {
	var setup_transtypes = document.getElementById("setup-transtypes");
	var avatar_transtypes = document.getElementById("building__avatar");
	if (setup_transtypes.value == 'animate') { avatar_transtypes.setAttribute("transitionType", "ANIMATE"); }
	if (setup_transtypes.value == 'linear') { avatar_transtypes.setAttribute("transitionType", "LINEAR"); }
	if (setup_transtypes.value == 'teleport') { avatar_transtypes.setAttribute("transitionType", "TELEPORT"); }
}

//-----------------------------------------------------------
// Manipulation with avatar walking speed
// X3D node: <NavigationInfo />
//-----------------------------------------------------------
function chooseAvatarSpeed() {
	var setup_speed = document.getElementById("setup-speed");
	var avatar_speed = document.getElementById("building__avatar");
	if (setup_speed.value == 'zero_two') { avatar_speed.setAttribute("speed", "0.2"); }
	if (setup_speed.value == 'zero_five') { avatar_speed.setAttribute("speed", "0.5"); }
	if (setup_speed.value == 'one_zero') { avatar_speed.setAttribute("speed", "1.0"); }
}

//-----------------------------------------------------------
// Manipulation with avatar size
// X3D node: <NavigationInfo />
//-----------------------------------------------------------
function chooseAvatarSize() {
	var setup_size = document.getElementById("setup-size");
	var avatar_size = document.getElementById("building__avatar");
	if (setup_size.value == 'human') { avatar_size.setAttribute("avatarSize", "0.25 1.6 0.8"); }
	if (setup_size.value == 'dwarf') { avatar_size.setAttribute("avatarSize", "0.2 0.9 0.4"); }
	if (setup_size.value == 'point') { avatar_size.setAttribute("avatarSize", "0.1 0.2 0.1"); }
}


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
 <body>
 <h1>List of all tGOLEM sessions during last year</h1>
<h2>SQL code:</h2>
 <pre>psql -qAt -U golem golem_database -c "SELECT start_shot_no,to_timestamp(start_timestamp, \'YY-MM-DD HH24:MI\'),session_setup,session_mission,staff_list,<br>replace(onstage_wave,\' \',E\'<"br">\'),replace(offstage_wave,\' \',E\'<"br">\'),replace(analysis,\' \',E\'<"br">\'),replace(infrastructure,\' \',E\'<"br">\') <br>FROM operation.sessions WHERE to_timestamp(start_timestamp, \'YY-MM-DD HH24:MI:SS\' ) > now() - INTERVAL \'360 DAYS\' ORDER BY start_shot_no DESC LIMIT 1000"
</pre>

<h2>Table</h2>
 <table border=1>
 <tr><th>Session id(The first shot of the Session)</th><th>Date</th><th>Setup name</th><th>Mission</th><th>Staff</th><th>On stage diagnostics</th><th>Off stage diagnostics</th><th>Analysis</th><th>Infrastructure</th></tr>
<?php
$out = array();
exec ('\
export PGPASSWORD=`cat /golem/production/psql_password`;psql -qAt -U golem golem_database -c "SELECT start_shot_no,to_timestamp(start_timestamp, \'YY-MM-DD HH24:MI\'),session_setup,session_mission,staff_list,replace(onstage_wave,\' \',E\'<br>\'),replace(offstage_wave,\' \',E\'<br>\'),replace(analysis,\' \',E\'<br>\'),replace(infrastructure,\' \',E\'<br>\') FROM operation.sessions WHERE to_timestamp(start_timestamp, \'YY-MM-DD HH24:MI:SS\' ) > now() - INTERVAL \'360 DAYS\' ORDER BY start_shot_no DESC LIMIT 1000"|awk -F\'|\' \'{print "<tr><td><a href=\"http://golem.fjfi.cvut.cz/shots/"$1"\">"$1"</a></td><td>"$2"</td><td>"$3"</td><td>"$4"</td><td>"$5"</td><td>"$6"</td><td>"$7"</td><td>"$8"</td><td>"$9"</td></tr>"}\'', $out);
foreach($out as $line) {
    echo $line;
    echo "\n";
}

echo '</table>';
echo '<a href=https://gitlab.com/golem-tokamak/dirigent/-/blob/master/Analysis/Homepage/psql/ShotsOfTheDay.php><img src="/_static/logos/GitLab.png" width="80px"></img></a>'
?>


</body></html>


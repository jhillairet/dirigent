#!/bin/bash

source /dev/shm/golem/Commons.sh
source commons/RemoteTrainingOsc.sh

line="d"


diag_id=BasicDiagnostics4TrainCourses
setup_id="StandardDAS-$line"
whoami="Diagnostics/$diag_id/$setup_id"



DAS="Oscilloscopes/RigolMSO5204-$line/RemoteTrainingOsc-$line"

DeviceList="$DAS"

diags=("" "U_Loop-$line" "U_BtCoil-$line" "U_RogCoil-$line" "U_photod-$line")

function PostDischargeAnalysis()
{
  CommonPostDischargeAnalysis
}


# Tuning:

#cd "/golem/shm_golem/ActualShot/Diagnostics/BasicDiagnostics/"; cp /golem/svoboda/Dirigent/Diagnostics/BasicDiagnostics/StandardDAS.* .;source StandardDAS.sh ;PostDischargeAnalysis

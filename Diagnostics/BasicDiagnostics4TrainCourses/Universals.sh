#golem@Dirigent>CO=Kvartet;source Commons.sh;Call /golem/Dirigent/Diagnostics/BasicDiagnostics4TrainCourses/$CO TeacherScopesSetting
#golem@Dirigent>CO=Kvartet;source Commons.sh;Call /golem/Dirigent/Diagnostics/BasicDiagnostics4TrainCourses/$CO OpenSession

function DoItEverywhere()
{
DoWhat=$2
Where=$1
    for i in $DeviceList; do 
    (echo Doing $DoWhat @ $i: ;cd $Where/Devices/$i;source $(dirname $i|xargs basename).sh;$DoWhat  ;cd - ) &
    sleep 0.2s
    done
    wait

}

function OpenSession()
{
    DoItEverywhere  $SW OpenSession
}


function GetReadyTheDischarge
{
    mkdir -p $SHM0/Diagnostics/BasicDiagnostics4TrainCourses/commons/
    cp $SW/Diagnostics/BasicDiagnostics4TrainCourses/commons/*.* $SHM0/Diagnostics/BasicDiagnostics4TrainCourses/commons/
    cp $SW/Diagnostics/BasicDiagnostics4TrainCourses/*.* $SHM0/Diagnostics/BasicDiagnostics4TrainCourses/
}


function Arming()
{
    DoItEverywhere $SW Arming
}


function CommonPostDischargeAnalysis()
{
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir-$line
    Analysis
    convert -resize $icon_size icon-fig-$line.png graph-$line.png
}

function NoAnalysis()
{
    cp $SW/Management/imgs/Commons/WithoutAnalysis.png icon-fig-$line.png
}

function Analysis()
{
local ThisDev=$1

    sed "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" /golem/Dirigent/Diagnostics/BasicDiagnostics4TrainCourses/commons/StandardDAS.ipynb > StandardDAS.ipynb
    sed -i "s/Rigol_line\ =\ 'xy'/Rigol_line\ =\ \'$ThisDev\'/g" StandardDAS.ipynb
    jupyter-nbconvert --execute StandardDAS.ipynb --to html --output analysis-$ThisDev.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)

}

# golem@Dirigent>BP=Diagnostics/BasicDiagnostics4TrainCourses;cp /golem/Dirigent/$BP/Universals.sh /golem/database/operation/shots/41503/$BP/;cp /golem/Dirigent/$BP/commons.* /golem/database/operation/shots/41503/$BP/commons/;Dg -r pda 41503 Diagnostics/BasicDiagnostics4TrainCourses/Kvartet




function TeacherScopesSetting()
{
    DoItEverywhere  $SW CommonSampleSetting
}


function HtmlGenerat()
{
echo "" > diagrow_$diag_id.html

for i in $DeviceList; 
do TheDevice=`echo $i|xargs dirname|xargs basename`
SHOT_NO=`cat $SHM/shot_no`
#echo $TheDevice

ln -s ../../../Devices/Oscilloscopes/$TheDevice/RemoteTrainingOsc DAS_raw_data_dir-$TheDevice


Analysis $TheDevice


echo "<tr>
<td><img src='http://golem.fjfi.cvut.cz/shotdir/`cat $SHM/shot_no`/Diagnostics/BasicDiagnostics4TrainCourses/$TheDevice.png'/></td>
<td><img src='http://golem.fjfi.cvut.cz/shotdir/`cat $SHM/shot_no`/Diagnostics/BasicDiagnostics4TrainCourses/setup.png'/></td>
<td><img src='http://golem.fjfi.cvut.cz/shotdir/`cat $SHM/shot_no`/Devices/"`dirname $i`"/das.jpg'/></td>
<td><a href=http://golem.fjfi.cvut.cz/shotdir/`cat $SHM/shot_no`/Devices/"`dirname $i`"/RemoteTrainingOsc/ScreenShotAll.png>
<img src='http://golem.fjfi.cvut.cz/shotdir/`cat $SHM/shot_no`/Devices/"`dirname $i`"/RemoteTrainingOsc/rawdata.jpg'/></a></td>
<td><a href=http://golem.fjfi.cvut.cz/shotdir/`cat $SHM/shot_no`/Devices/"`dirname $i`"/RemoteTrainingOsc>$diricon</a></td>">>diagrow_$diag_id.html;


if [ -f /golem/database/operation/shots/$SHOT_NO/Diagnostics/BasicDiagnostics4TrainCourses/Kvartet/icon-fig-$TheDevice.png ]; then 
echo "<td><a href=http://golem.fjfi.cvut.cz/shotdir/`cat $SHM/shot_no`/Diagnostics/BasicDiagnostics4TrainCourses/Kvartet/icon-fig-$TheDevice.png>
<img src='http://golem.fjfi.cvut.cz/shotdir/`cat $SHM/shot_no`/Diagnostics/BasicDiagnostics4TrainCourses/Kvartet/icon-fig-$TheDevice.png' width=120/></a></td>">>diagrow_$diag_id.html;
fi

echo "</tr>">>diagrow_$diag_id.html;

done



}


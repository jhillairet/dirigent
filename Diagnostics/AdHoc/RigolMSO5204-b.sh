#!/bin/bash

BasePath=../..;source $BasePath/Commons.sh
source commons/RigolMSO.sh

line="b"


diag_id=`basename $PWD`
setup_id=RigolMSO5204-b
whoami="Diagnostics/$diag_id/$setup_id"

DAS="Oscilloscopes/RigolMSO5204-$line/AdHoc-$line"

DeviceList="$DAS"

diags=("" "ch1-$line" "ch2-$line" "ch3-$line" "ch4-$line")

function PostDischargeAnalysis()
{
  CommonPostDischargeAnalysis
}


# Tuning:

#source Commons.sh ;OpenSessionSomewhere file:///golem/svoboda/Dirigent/Devices/Oscilloscopes/RigolMSO5204-b/AdHoc-b.sh
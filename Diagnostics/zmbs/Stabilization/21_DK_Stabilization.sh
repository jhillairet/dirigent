#!/bin/bash

source /dev/shm/golem/Commons.sh



diag_id=Stabilization
setup_id="21_DK_Stabilization"
whoami="Diagnostics/$diag_id/$setup_id"

DAS="Oscilloscopes/RigolMSO5104-a/Stabilization"

DeviceList="RelayBoards/Energenie_LANpower-b/AdHocPowerSupplyUnderTokamak FunctionGenerators/RigolDG1032Z-a/FG4Stabilization $DAS"

LastChannelToAcq=2
diags=('U_trigger' 'U_fg' 'U_xy' 'U_yx')


 function GetReadyTheDischarge ()
{

    GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh
}
   
   
function Arming()
{
    raw=`cat $SHM0/Production/Parameters/diagnostics_stabilization`
    
    raw=$(echo $raw | tr -s ',' ' ')
raw=$(echo $raw | tr -s ';' ' ')

time=()
value=()
a=0
for i in $raw
    do
      if [[ a%2 -eq 0 ]]    
      then
        time+=($i)
      else
        value+=($i)
      fi
    let "a+=1"
    done

echo Time points: ${time[@]}
echo Values: ${value[@]}


sign () { echo "$(( $1 < 0 ? -1 : 1 ))"; } #absolute value

#looks for the max value
Max=0
for m in ${value[@]} 
    do
      m=$(( $m * $(sign "$m") ))    
      if [[ $m -gt $Max ]]
      then 
         Max=$m 
      fi
    done
echo Max value: $Max

#generate sequence of points for the frequency generator
sequence=()
j=0
l=1
for ((i=${time[0]};i<=${time[-1]};i+=100))
   do
     k=${value[$l-1]}
     sequence+=$(python -c "print(round(${k}.0 / ${Max}.0,1),',')") #strange but functional
     if [[ $i -eq ${time[$l]} ]]
     then
        let "l+=1"
     fi
    let "j+=1"
   	done
c=${sequence[@]}
echo Final sequence: $c


#========Frequency generator settings===========

echo "*IDN?"|netcat -w 1 192.168.2.171 5555
echo ":OUTP2 OFF"|netcat -w 1 192.168.2.171 5555 #Turn off output off ch2
sleep 0.1;
#First, set the waveform parameters
echo ":SOUR2:APPL:ARB 10000,$Max,0"|netcat -w 1 192.168.2.171 5555 #Arbitrary waveform, Samples rate:10000Sa/s (1point = 100us); amplitude [V], offset:0V 
echo ":SOUR2:DATA VOLATILE, $c"|netcat -w 1 192.168.2.171 5555 #predefined function

#Now set the burst mode
echo ":SOUR2:BURS ON"|netcat -w 1 192.168.2.171 5555
echo ":SOUR2:BURS:MODE:TRIG;:SOUR2:BURS:TRIG:SOUR EXT;:SOUR2:BURS:NCYC 1 "|netcat -w 1 192.168.2.171 5555 #Set trigger to manual/external and number of cycle to ...
sleep 0.2;

#echo ":SOUR2:BURS:TRIG:SLOP POS"|netcat -w 1 192.168.2.171 5555 #Set CH2 to type of trigger input on the falling edge (use with external trigger)

echo ":SOUR2:BURS:TDEL 0.00${time[0]}"|netcat -w 1 192.168.2.171 5555 #Set time delay (in seconds)

echo ":OUTP2 ON"|netcat -w 1 192.168.2.171 5555 #Turn on output of ch2

#echo ":SOUR2:BURS:TRIG"|netcat -w 1 192.168.2.171 5555 #Manual trigger
    
}
   
   
   function PostDischargeAnalysis
{

    GeneralDAScommunication $DAS RawDataAcquiring
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir

    for i in `seq 2 $LastChannelToAcq` ; do
       awk '{print $1","'\$$i'}' DAS_raw_data_dir/NIdata_6133.lvm > ${diags[$i-2]}.csv; 

    done 
    Analysis

      GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` Infrastructure/Stabilization/
      
}      
      
function Analysis
{

#Workarround
    echo "<HTML><HEAD><TITLE></TITLE><BODY>
    <H1>Diagnostics analysis is not ready yet</H1>
    <!-- <img src="icon-fig.png"></img>-->
    <br></br></BODY></HTML>" > analysis.html
    cp $SW/Management/imgs/Commons/ScriptNotReadyYet.png icon-fig.png
#Finals      
    convert -resize $icon_size icon-fig.png graph.png
}



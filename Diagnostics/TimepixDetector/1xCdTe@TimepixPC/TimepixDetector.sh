#!/bin/bash

SHM="/dev/shm/golem"
source $SHM/Commons.sh
#rPi=192.168.2.92 ## rPI IP adress 
shot_no=`CurrentShotDataBaseQuerry shot_no`

#diag_id="TimepixDetector"
#setup_id="1xCdTe1xSi@TimepixPC"
setup_id="TimepixDetector"
#whoami="Diagnostics/$diag_id/$setup_id"

#DeviceList="Radiation/AdvaPix_CdTe-a/device Radiation/AdvaPix_CdTe-b/device Computers/RasPi4-c/TimepixDetector Computers/RasPi4-b/TimepixDetector "

DeviceList="ITs/TimepixPC/Timepix Radiation/AdvaPix_CdTe-a"

#account="pi@RasPi4-c"
#Rasps="RasPi4-c RasPi4-b"
Rasps="TimepixPC"

function Arming()
{
ssh amos@TimepixPC "rm /home/amos/measGolem/F10-W0049/*"
ssh amos@TimepixPC "rm /home/amos/measGolem/H03-W0051/*"
ssh amos@TimepixPC "python3 arming1903_23.py $shot_no > /dev/null 2>/dev/null &"
}



function PostDischargeAnalysis()
{
	sleep 5
	echo "PostDischargeAnalysis @ F10"
		mkdir -p F10
		scp amos@TimepixPC:measGolem/F10-W0049/* F10/
		sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" RasPi4-b.ipynb
		jupyter-nbconvert  --ExecutePreprocessor.timeout=20 --to html --execute RasPi4-b.ipynb --output RasPi4-b.html         > >(tee -a jup-nb_stdout-RasPi4-b.log) 2> >(tee -a jup-nb_stderr-RasPi4-b.log >&2)
		convert -resize $icon_size icon-fig.png RasPi4-b.png
	#done
	cp RasPi4-b.png graph.png
	
	#cp H03.png graphH03.png
#	echo "PostDischargeAnalysis @ H03"
#		mkdir -p H03
#		scp amos@TimepixPC:measGolem/H03-W0051/* H03/
#		sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" RasPi4-c.ipynb
#		jupyter-nbconvert  --ExecutePreprocessor.timeout=20 --to html --execute RasPi4-c.ipynb --output RasPi4-c.html         > >(tee -a jup-nb_stdout-RasPi4-c.log) 2> >(tee -a jup-nb_stderr-RasPi4-c.log >&2)
#		convert -resize $icon_size icon-fig.png RasPi4-c.png
	#done
#	cp RasPi4-c.png graph.png
	#cp H03.png graphH03.png


	HtmlGenerat

}

function HtmlGenerat()
{
SHOT_NO=`cat $SHM/shot_no`
echo "" > diagrow_$setup_id.html
echo "<tr>
    <td valign=bottom><img src=Diagnostics/TimepixDetector/1xCdTe@TimepixPC/name.png  width='100'/>&nbsp;</td>
    <td valign=bottom><img src=Diagnostics/TimepixDetector/1xCdTe@TimepixPC/setup.png  width='200'/>&nbsp;</td>
    <td></td>
    <td valign=bottom><img src=Diagnostics/TimepixDetector/1xCdTe@TimepixPC/device.png  width='200'/>&nbsp;</td>
    <td></td>
    <td valign=bottom><img src=Diagnostics/TimepixDetector/1xCdTe@TimepixPC/RasPi4-b.png  width='200'/>&nbsp;</td>
    #<td valign=bottom><img src=Diagnostics/TimepixDetector/1xCdTe@TimepixPC/RasPi4-c.png  width='200'/>&nbsp;</td> #Si H03-W0051 detector
    <td></td>
    <td valign=bottom><a href=Diagnostics/TimepixDetector/1xCdTe@TimepixPC/ title=Directory><img src=http://golem.fjfi.cvut.cz/_static/direct.png  width='30px'/></a></td>
    <td></td>
    <td></td>
    </tr>" >> diagrow_$setup_id.html
}

#function HtmlGenerat_zmb()
#{
#	<td valign=bottom><img src=Diagnostics/TimepixDetector/1xCdTe@TimepixPC/RasPi4-b.png  width='200'/>&nbsp;</td>
#	#<td valign=bottom><img src=Diagnostics/TimepixDetector/1xCdTe@TimepixPC/RasPi4-b.png  width='200'/>&nbsp;</td>
#}


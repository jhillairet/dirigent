function [result1, t, f, MN] = Spectrs(signal, wind, num_av, step, f_ACP)

%��������� ������� �������
%signal - ����������� ������
%wind - �� ������� ����������� ������
%num_av - ����� ���������� ��������
%step - ��� �������� ����
%f_ACP - ������� ��� ���

f_ACP = f_ACP*1e6;

n = fix((1 + max(size(signal))-wind)/step); %����� ��������

t = zeros(n,1);

wind_fun = 0.5*(1-cos(2*pi*(1:wind)/(wind-1)));
Msignal = zeros(wind*2,n);
for j = 1:n
    t(j) = step*j*1e3/f_ACP; % ������ ��� step = wind/2!!!!!!
    Msignal(1:wind,j) = signal(1 + (j-1)*step:(j-1)*step + wind).*wind_fun';      %��������� �� ����
end
t = average(t, num_av);

fm = f_ACP/2;
f_step = fm/wind;
f = zeros(2*wind,1);
for j = 1:wind;
    f(wind + 1 + j) = f(wind+j) + f_step;
    f(wind + 1 - j) = f(wind-j + 2) - f_step;
end
if size(f, 1) > wind
    f(end) = [];
end
f = f/1e6; %� ���

Msignal(1:wind,:) = Msignal(1:wind,:) - ones(wind,1)*mean(Msignal); %������� �������
fourier = fft(Msignal);

fourier = power(abs(fourier),2); %��������

result = average(fourier', num_av)';

if sum(abs(imag(signal))) > 0
    result1 = swap(result);
else
    result1 = result(1:fix(wind),:);
    f(f<0) = [];
end

end

function [ b] = swap(a)
%������ ������� ������ � ������ �������� a �� ������� �������

b(1:size(a,1)/2, :) = a(size(a,1)/2+1:size(a,1),:);
b(size(a,1)/2+1:size(a,1), :) = a(1:size(a,1)/2, :);

end

function [ out ] = average(in, num_av )
%��������� �� num_av �����

num_av = fix(num_av);

if num_av == 1
    
    out = in;
    
else
    out = zeros(fix(size(in,1)/num_av), size(in,2));
    
    for k = 1: fix(size(in,1)/num_av)
        out(k,:) = mean(in((k-1)*num_av + 1:k*num_av, :));
    end
end

end


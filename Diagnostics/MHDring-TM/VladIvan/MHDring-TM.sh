#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh 


DAS="DASs/2NI_PC-VoSv/MHDring-TM"

DeviceList="$DAS"



function DefineDiagTable()
{
    CreateDiagnosticsTable $diag_id
    
}


function GetReadyTheDischarge ()
{
    :
    #GeneralTableUpdateAtDischargeBeginning diagnostics.mhdring-tm #@Commons.sh
}




function PostDischargeAnalysis()
{
#    GeneralDAScommunication $DAS RawDataAcquiring
     
    ln -s ../../../Devices/$DAS DAS_raw_data_dir
    
    for i in `seq 2 17`; do awk '{print $1","'\$$i'}' DAS_raw_data_dir/ToPlot.lvm > ring_$((i-1)).csv; done;
    Analysis
    GenerateDiagWWWs $instrument $setup $DAS 
}


function NoAnalysis
{
   cp $SW/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
    convert -resize $icon_size icon-fig.png graph.png

}


function Analysis
{
local ShotNo=$(<$SHM0/shot_no)
local Vacuum_shot=$(<$SHM0/Operation/Discharge/Basic/Parameters/vacuum_shot)


cp Script.m Script.sf
sed "s/ShotNo = 0/ShotNo = $ShotNo/g;s/vacuum_shot = 0/vacuum_shot = $Vacuum_shot/g" Script.sf > Script.m 
matlab -nosplash -nodesktop -r Script_publish

for fig in offset.png MHD_activity.png Spectrogram.png 	q.png offset.png animation.gif[0] pictures.png;do
    convert -resize 200 $fig ${fig%%.*}-icon.png  
done    

cp pictures-icon.png icon-fig.png

}



















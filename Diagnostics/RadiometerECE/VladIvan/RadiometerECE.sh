#!/bin/bash
BasePath=../../..;source $BasePath/Commons.sh

DAS="DASs/Papouch-Ji/Radiometer"

DeviceList="$DAS LabInstruments/RadiometerECE/VladiIvan"


LastChannelToAcq=12
diags=('Uloop' 'rm1' 'rm2' 'rm3' 'rm4'  'rm5' 'rm6' 'rm7' 'rm8'  'rm9' 'rm10' 'rm11')





function PostDischargeAnalysis() 
{

    #GeneralDAScommunication $DAS RawDataAcquiring $LastChannelToAcq
    ln -s ../../../Devices/$DAS DAS_raw_data_dir

    for i in `seq 1 $LastChannelToAcq` ; do
        cp DAS_raw_data_dir/ch$i.csv ${diags[$i-1]}.csv
    done    

     GenerateDiagWWWs $instrument $setup $DAS 
     NoAnalysis
}




function NoAnalysis
{
    cp $SW/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
    convert -resize $icon_size icon-fig.png graph.png
}

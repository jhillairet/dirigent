# FastCamGOLEM
Camera model: Photron FASTCAM MINI UX50

## Before starting 
Due to the neccessary dll libraries, the cameras are opereted from a Windows computer. This means that it is not as easy as on any Linux computer to control them from a Linux terminal. It is possible to use `wakeonlan`, in the mounted folder simple copy, read, write commands, but it is not possible (yet) to run/end/restart or even control the program that operates the cameras. Hence the program (FastCamGOLEM.exe) was added to startup programs, so that it starts immediately after Windows is ready.

> **Warning**
> The program assumes that the cameras are already running and if it does not detect any of these cameras, it will immediately exit without any warning and the program must then be started "manually" (= double-click /FastCamGOLEM/FastCamGOLEM.exe)

## Standard mode
The following parameters are set in default mode (for standard discharges):
| Parameter     | Value         | 
| ------------- |:-------------:| 
| Height      | 56 | 
| Width      | 1200 | 
| fps|      | 40000 |
|No. of saved frames| 1200 |

These (and others) parameters are listed in the [CamsConfig.json](./cpp/CamsConfig.json) file, which is sent to the Windows computer (probably in an OpenSession).

## Documetation 
If you want to write your own new program, fix/upgrade an existing program, or just get more details on how it works, you can check out the [documentation](./cpp/doc/html/index.html).

>**Note**
To view the documentation, I recommend downloading the [html](./cpp/doc/html) folder and opening the index.html file in your browser, or you can view the lightweight pdf version [here](./cpp/doc/refman.pdf).

## Troubleshooting
There was an occasional issue with the vertical camera, which stops responding correctly to the trigger. When this occurs, it is recommended to restart the cameras and the program.

>**Note**
> It is required that the executable file (FastCamGOLEM.exe) be in the same folder as the dll.

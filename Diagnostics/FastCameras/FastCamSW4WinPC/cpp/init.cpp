/** @file init.cpp
*	@brief Function used in the initialization part.
*/
#include "FastCamGOLEM.h"

void init(void) {
    cout << "Initialization..." << endl << endl;
    unsigned long nRet;
    unsigned long nErrorCode;

    nRet = PDC_Init(&nErrorCode);

    checkStatus("PDC_Init", nRet, nErrorCode);
}

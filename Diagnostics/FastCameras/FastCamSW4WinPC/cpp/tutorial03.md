@page tutorial03 Tutorial 03 - Creating a simple (default) main
@tableofcontents

### How to build new main.cpp
In this tutorial, we will provide you the steps that can help you to create your own main.cpp for camera control purposes. 

### Dependencies
First, we need to include a few header files that are necessary for the cameras to work.

@code
#include <iostream>
#include "FastCamGOLEM.h"
@endcode

It is also useful to predefine some constants.
@code
using namespace std;
const std::pair<std::string, unsigned long> InputFunct[4] = { {"SyncPos",PDC_EXT_IN_SYNC_POS},{"SyncNeg",PDC_EXT_IN_SYNC_NEGA},{"TrigPos", PDC_EXT_IN_TRIGGER_POSI},{"TigNeg", PDC_EXT_IN_TRIGGER_NEGA} };
const std::pair<std::string, unsigned long> TriggerModes[3] = { {"TrigStart",PDC_TRIGGER_START }, { "TrigMan",PDC_TRIGGER_MANUAL} };
const std::pair<std::string, unsigned long> SettingsModes[2] = { {"VarChannel",0 }, {"SetAll", 1} };
@endcode


### Create and/or read json file
In another step it is important to have CamsConfig.json file, which includes some parameters neccessary to run program, such as camera settings, AVI path name, etc. (For more detail explanation of how to create your own json file, see @subpage tutorial02. 

First values from json file are read and saved in variable r and print in the terminal.
Then comes the initialization step where the external PDCLIB libraries are initialized. After that is called OpenDevice() function which makes connention between the computer and the cameras. If previous steps are successful, required values are set and cameras  are waiting for triggers. After the recording is finished, the data are saved in AVI format and the whole loop starts again by connecting to the cameras. An Output.json file containing the actual set values is also generated at the end of each loop (for cases the CamsCpnfig.json configuration file contained incorrect values).

@code 
int main()
{
    vector<camera> cams;

    ifstream file("CamsConfig.json");
    json r;
    file >> r;
    file.close();

    for (unsigned long i = 0; i < r.size(); i++) { camera cam; cams.push_back(cam); }

    for (unsigned long i = 0; i < r.size(); i++)
    {
        cams[i].ipAddr = IPtoLong(r[i]["ipAddr"].get<string>());
        cams[i].name = r[i]["name"].get<string>();
        cams[i].videoFileName = r[i]["videoFileName"].get<string>();
        cams[i].BMPdataFolderPath = r[i]["BMPdataFolderPath"].get<string>();
        cams[i].framesToSave = r[i]["framesToSave"].get<unsigned long>();
        cams[i].shotDurToSave = r[i]["shotDurToSave"].get<unsigned long>();
        cams[i].variableChannel = r[i]["variableChannel"].get<unsigned long>();
        for (int j = 0; j < 3; j++) { if (TriggerModes[j].first == r[i]["TriggerMode"].get<string>()) { cams[i].TriggerMode = TriggerModes[j]; } }
        for (int j = 0; j < 2; j++) { if (SettingsModes[j].first == r[i]["SettingsMode"].get<string>()) { cams[i].SettingsMode = SettingsModes[j]; } }

        cams[i].width = r[i]["width"].get<unsigned long>();
        cams[i].height = r[i]["height"].get<unsigned long>();
        cams[i].recordRate = r[i]["recordRate"].get<unsigned long>();
        //cams[i].shutterSpeed = r[i]["shutterSpeed"].get<unsigned long>();

    }

    cout << "Raw parameters from JSON: " << endl; 
    printCamsPar(cams);

    init();
    OpenDevices(cams);
    
    CreateSetVariableChannel(cams);

    while (1) {
        printCamsPar(cams);
        OpenDevices(cams);

        cout << "\nStatus..." << endl;
        SetStatus(cams, PDC_STATUS_LIVE); // Cameras must be in Live mode to record

        cout << "\nRecording part..." << endl;
        recording(cams);
        SaveAVI(cams);
        outputJSON(cams);    
    }

    return 0;
}

 @endcode
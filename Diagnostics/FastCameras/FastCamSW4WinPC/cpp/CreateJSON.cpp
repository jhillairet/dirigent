/** @file CreateJSON.cpp 
*   @brief Functions used to create and modify a JSON file.
*/

#include "FastCamGOLEM.h"

void createJSON(void) {
    json w;

    for (int i = 0; i < 2; i++) {
        w[i]["framesToSave"] = 1200;
        w[i]["shotDurToSave"] = 30; //in ms
        w[i]["TriggerMode"] = "TrigMan"; //Options: {"TrigStart",PDC_TRIGGER_START }, {"TrigRand",PDC_TRIGGER_RANDOM}, { "TrigMan",PDC_TRIGGER_MANUAL} //Trigger Random nefunguje
        w[i]["input1"] = "TrigPos"; //Options: {"SyncPos",PDC_EXT_IN_SYNC_POS},{"SyncNeg",PDC_EXT_IN_SYNC_NEGA},{"TrigPos", PDC_EXT_IN_TRIGGER_POSI},{"TigNeg", PDC_EXT_IN_TRIGGER_NEGA}
        w[i]["input2"] = "SyncPos";
        w[i]["variableChannel"] = 1;
        w[i]["SettingsMode"] = "SetAll"; //Options:  {"VarChannel",0 }, {"SetAll", 1} 

        w[i]["width"] = 1280;
        w[i]["height"] = 56;//56;
        w[i]["recordRate"] = 40000;
        //w[i]["shutterSpeed"] = 40000;//???
    }

    w[0]["ipAddr"] = "192.168.2.10";
    w[1]["ipAddr"] = "192.168.2.11";
    w[0]["name"] = "Camera Vertical";
    w[1]["name"] = "Camera Radial";
    w[0]["videoFileName"] = "AVI\\CamVert.avi";
    w[1]["videoFileName"] = "AVI\\CamRad.avi";
    w[0]["BMPdataFolderPath"] = "BMP\\Vertical\\";
    w[1]["BMPdataFolderPath"] = "BMP\\Radial\\";

    //write to .json file
    ofstream WriteFile("CamsConfig.json");
    WriteFile << w.dump(2);
    WriteFile.close();
    cout << "The JSON file CamsConfig.json has been overwritten.\n" << endl;
    return;
}


void outputJSON(vector<camera>& cams) {
    json w;
    for (int i = 0; i < 2; i++) {
        w[i]["framesToSave"] = cams[i].framesToSave;
        w[i]["shotDurToSave"] = cams[i].shotDurToSave; //in ms
        w[i]["width"] = cams[i].width;
        w[i]["height"] = cams[i].height;//56;
        w[i]["recordRate"] = cams[i].recordRate;
        w[i]["shutterSpeed"] = cams[i].shutterSpeed;
    }

    w[0]["name"] = "Camera Vertical";
    w[1]["name"] = "Camera Radial";

    //write to .json file
    ofstream WriteFile("OutputCamsConfig.json");
    WriteFile << w.dump(2);
    WriteFile.close();
    cout << "The JSON file OutputCamsConfig.json has been overwritten.\n" << endl;
    return;

}

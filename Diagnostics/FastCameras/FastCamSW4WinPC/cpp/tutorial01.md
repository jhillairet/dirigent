@page tutorial01 Tutorial 01 - Settings Visual Studio Project

The screenshots below can help you to include external libraries in your VS project.

<img src="../screenshot_VS_Project_Properties.png" alt="Project properties" style=" width:1200px;"/>


<img src="../screenshot_include.png" alt="Include directories" style=" width:600px;"/>



<img src="../screenshot_LibPath.png" alt="Include library directories" style=" width:600px;"/>



<img src="../screenshot_PDCLib.png" alt="" style=" width:600px;"/>
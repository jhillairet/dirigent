/** @file main.cpp */

#include <iostream>
#include "FastCamGOLEM.h"

using namespace std;
const std::pair<std::string, unsigned long> InputFunct[4] = { {"SyncPos",PDC_EXT_IN_SYNC_POS},{"SyncNeg",PDC_EXT_IN_SYNC_NEGA},{"TrigPos", PDC_EXT_IN_TRIGGER_POSI},{"TrigNeg", PDC_EXT_IN_TRIGGER_NEGA} };
const std::pair<std::string, unsigned long> TriggerModes[3] = { {"TrigStart",PDC_TRIGGER_START }, {"TrigRand",PDC_TRIGGER_RANDOM}, { "TrigMan",PDC_TRIGGER_MANUAL} }; /** @warning For uknown reason Trigger_RANDOM does not work. */
const std::pair<std::string, unsigned long> SettingsModes[2] = { {"VarChannel",0 }, {"SetAll", 1} };

int main()
{
    //createJSON(); //Supposed to be created only once in while

    /** @note CamsConfig.json has to be in folder of the VS project. */
    vector<camera> cams;

    //get data from JSON file (to be implemented)
    ifstream file("CamsConfig.json");
    json r;
    file >> r;
    file.close();

    for (unsigned long i = 0; i < r.size(); i++) { camera cam; cams.push_back(cam); }

    for (unsigned long i = 0; i < r.size(); i++)
    {
        cams[i].ipAddr = IPtoLong(r[i]["ipAddr"].get<string>());
        cams[i].name = r[i]["name"].get<string>();
        cams[i].videoFileName = r[i]["videoFileName"].get<string>();
        cams[i].BMPdataFolderPath = r[i]["BMPdataFolderPath"].get<string>();
        cams[i].framesToSave = r[i]["framesToSave"].get<unsigned long>();
        cams[i].shotDurToSave = r[i]["shotDurToSave"].get<unsigned long>();
        cams[i].variableChannel = r[i]["variableChannel"].get<unsigned long>();
        for (int j = 0; j < 3; j++) { if (TriggerModes[j].first == r[i]["TriggerMode"].get<string>()) { cams[i].TriggerMode = TriggerModes[j]; } }
        for (int j = 0; j < 2; j++) { if (SettingsModes[j].first == r[i]["SettingsMode"].get<string>()) { cams[i].SettingsMode = SettingsModes[j]; } }

        cams[i].width = r[i]["width"].get<unsigned long>();
        cams[i].height = r[i]["height"].get<unsigned long>();
        cams[i].recordRate = r[i]["recordRate"].get<unsigned long>();
        //cams[i].shutterSpeed = r[i]["shutterSpeed"].get<unsigned long>();


        //Hopefully it is not necessary
//        for (int j = 0; j<4; j++){ //update 10.9.
//            if(InputFunct[j].first==r[i]["input1"].get<string>()){cams[i].input1 = InputFunct[j];};
//            if(InputFunct[j].first==r[i]["input2"].get<string>()){cams[i].input2 = InputFunct[j];};
//        }



    }

    cout << "Raw parameters from JSON: " << endl; 
    printCamsPar(cams);

    //init.cpp
    init();

    //OpenDevice.cpp
    OpenDevices(cams);
    
    
    //if (cams[0].SettingsMode.second == 0) {
    //    SetVariableChannel(cams);
    //    cout << "Settings Mode: Variable Channel" << endl;
    //}
    //else{
    //    SetAll(cams);
    //    cout << "Settings Mode: SetAll" << endl;
    //    checkNumFrameToSave(cams);
    //}
    
    //better not to use
    //SetExternal_IO_signal(cams);//TODO: An error uccurred when trying to set - kdyz se to zkusi jen pro jednu, tak se musi nasledne kamery restartovat

    /** @todo implement and try new seting function*/
    CreateSetVariableChannel(cams);

    while (1) {
        printCamsPar(cams);
        OpenDevices(cams);

        cout << "\nStatus..." << endl;
        SetStatus(cams, PDC_STATUS_LIVE); // Cameras must be in Live mode to record

        //Recording.cpp
        cout << "\nRecording part..." << endl;
        recording(cams);

        //getData.cpp
        SaveAVI(cams);
        
        outputJSON(cams);
       
    }

    

    cout << "end" << endl;

    return 0;
}


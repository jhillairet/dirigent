#!/bin/bash

 
BasePath=../../..;source $BasePath/Commons.sh

DAS="Vacuums/PfeifferPrismaPro/Basic"

DeviceList=$DAS

N_SCANS=10
MASS_SPECT_URL="http://192.168.2.178"
SESSION_COOKIE="cookie.txt"



function DefineDiagTable()
{
    CreateTable diagnostics.$diag_id
    
}


 

function GetReadyTheDischarge ()
{

    #GeneralTableUpdateAtDischargeBeginning $diag_id #@Commons.sh
    GeneralTableUpdateAtDischargeBeginning diagnostics.massspectrometer #@Commons.sh
}



function PostDischargeAnalysis()
{
#    Call $SHM0/Devices/$DAS RawDataAcquiring

     
    ln -s ../../../Devices/$DAS DAS_raw_data_dir

    GenerateDiagWWWs $instrument $setup $DAS 
   #GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` Zrc6YenW4gfLxAjJ8 # @Commons.sh
   
        No_Analysis

}


function Analysis
{
 python3 massSpectrometer.py -n $N_SCANS -s -p
}

function No_Analysis
{

#Workarround
    echo "<HTML><HEAD><TITLE></TITLE><BODY>
    <H1>Diagnostics analysis is not ready yet</H1>
    <!-- <img src="icon-fig.png"></img>-->
    <br></br></BODY></HTML>" > analysis.html
    cp $SW/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
#Finals      
    convert -resize $icon_size icon-fig.png graph.png
}


#####################

function send_command ()
{   
    if [ -z $1 ]; then
        return
    fi
    local command=$1
    url="$MASS_SPECT_URL""$command" 
    echo "sending $url"
    curl -s --cookie $SESSION_COOKIE $url > /dev/null
    #curl $url >/dev/null
}



function start_scan ()
{
    # establish connection
    curl -s --cookie-jar $SESSION_COOKIE $MASS_SPECT_URL  > /dev/null
    if [ ! -f "$SESSION_COOKIE" ]; then
        echo "No cookie-jar cannot establish session"
        return
    fi
    
    # take controll
    send_command "/mmsp/communication/control/set?force"

    # stop running scans (required for setup)
    send_command "/mmsp/scanSetup/set?scanstop=1"

    # -------------------
    # scan configuration
    # 0 - timestamp
    send_command "/mmsp/scanSetup/set?@channel=0&channelMode=timestamp&dwell=8&enabled=True"
    
    # 1 - baseline
    send_command "/mmsp/scanSetup/set?@channel=1&channelMode=baseline&dwell=8&enabled=True"
    
    # 2 - totalpressure
    send_command "/mmsp/scanSetup/set?@channel=2&channelMode=totalpressure&dwell=8&enabled=True"
    
    # 3 - sweep 0-50 amu, 10 ppamu, dwell
    send_command "/mmsp/scanSetup/set?@channel=3&channelMode=Sweep"
    send_command "/mmsp/scanSetup/set?@channel=3&enabled=True"
    send_command "/mmsp/scanSetup/set?@channel=3&startmass=0.0"
    send_command "/mmsp/scanSetup/set?@channel=3&stopmass=100.0" 
    send_command "/mmsp/scanSetup/set?@channel=3&dwell=32"
    send_command "/mmsp/scanSetup/set?@channel=3&ppamu=1"

    send_command "/mmsp/scanSetup/set?startchannel=1&stopchannel=3"

    # number of scans (-1 starts loop)
    send_command "/mmsp/scanSetup/set?scancount=$N_SCANS"
    # -------------------


    # start emission and RF generator
    send_command "/mmsp/generalControl/set?setEmission=on"
    sleep 10

    # start scan
    send_command "/mmsp/scanSetup/set?scanInterval=0&scanstart=1"

}

function stop_scan ()
{
    send_command "/mmsp/scanSetup/set?scanstop=1"
    
    # turn off filament and RF generator
    send_command "/mmsp/generalControl/set?setEmission=off"
}

function release_control () 
{
   send_command "/mmsp/communication/control/set?release"
   rm $SESSION_COOKIE
}

function is_scanning () {
    curl  $MASS_SPECT_URL"/mmsp/scanInfo/get?currentScan&scanning"

}


# -*- coding: utf-8 -*-
"""
Scrip for Mass Spectrometer PrismPro 250 setting and readout
"""


import argparse
import numpy as np
import pandas as pd
import sys, os
import time
from datetime import datetime
import json
import http.client
import pprint
import re
import matplotlib.pyplot as plt

VERBOSE = False
CAPTURE_FILE = "my-scan.csv"
TARGET_IP_ADDR = "192.168.2.178"
SHOW_SCANS = False


class SingleMass:
    def __init__(self, mass, dwell=None):
        self.mass = mass
        self.dwell = dwell

class MassSweep:
    def __init__(self, start, stop, ppamu, dwell=None):
        self.start = start
        self.stop  = stop
        self.ppamu = ppamu
        self.dwell = dwell

class MMSP:
    '''
    Modular Mass Spec from Pfeiffer API example
    '''
    def __init__(self, target_ip, port=80):
        self.conn = http.client.HTTPConnection('%s:%d' % (target_ip, port), timeout=10)

    def send(self, request, verbose=None):
        if verbose is None:
            verbose = VERBOSE

        if verbose:
            print(("Request: %s" % request))

        self.conn.request('GET', request)
        resp = self.conn.getresponse()
        http_status = resp.status

        if http_status != 200:
            if not verbose:
                print(("Request: %s" % request))

            print(("HTTP Status:", http_status))
            print(("Response:", resp))
            return (http_status, resp.read())
        
        resp_txt = resp.read()
        try:
            resp_rtn = json.loads(resp_txt)
        except:
            resp_rtn = resp_txt
        if verbose:
            self._printResponse(resp_rtn)

        return (http_status, resp_rtn)

    def _printResponse(self, resp):
        if not isinstance(resp, dict):
            print("Response:")
            print("   ", resp)
            return

        data = resp.get('data', None)
        conditions = resp.get('conditions')
        status = resp.get('name', None)
        if data is None:
            print("Response:")
            print("### Couldn't find 'data' entry in response")
            for key,value in list(resp.items()):
                print((" - %s:" % (key), value)) # pprint.pformat(value))
        else:
            outstr = pprint.pformat(data,2)
            line_cnt = outstr.count("\n")
            if line_cnt > 1:
                outstr = re.sub( '^',' '*4, outstr ,flags=re.MULTILINE )
                print(("Response: [%s]" % status))
                print(outstr)
            else:
                print(("Response: [%s]" % status, outstr))
            if conditions is not None:
                outstr = pprint.pformat(conditions, 2)
                outstr = re.sub( '^',' '*4, outstr ,flags=re.MULTILINE )
                print("Conditions:")
                print(outstr)
        print("")
    
    def getFile(self, remote_fname, local_fname="output.bin"):
        self.conn.request('GET', remote_fname)
        resp = self.conn.getresponse()

        if resp.status == 404:
            resp.read()
        else:
            with open(local_fname, 'wb') as f:
                f.write(resp.read())
        return resp.status, resp.length, remote_fname

###############################################################################

def wait_for_scans(mmsp):
    last_report = -1
    while True:
        time.sleep(1)
        http_code, resp = mmsp.send("/mmsp/scanInfo/get?currentScan&scanning")
        current_scan = resp['data']['currentScan']
        scanning = resp['data']['scanning']
        if not scanning:
            print("\rAll Done, finished scanning")
            break
        elif current_scan != last_report:
            msg = "Scan %d" % current_scan
            print("\r\t%-20s" % msg, end='')
            last_report = current_scan

###############################################################################

def dump_scans(mmsp):
    last_report = -1
    while True:
        time.sleep(1)
        http_code, resp = mmsp.send("/mmsp/measurement/nextScan/get")
        data = resp['data']
        system_status = data['systemStatus']
        current_scan  = data['currentScan']
        scan_num      = data['scannum']
        scan_values   = data['values']

        if scan_values is not None:
            print(scan_num, scan_values)

        if (system_status & 0x2) == 0:
            # Scanning Inactive
            if current_scan == scan_num:
                # All scans have be processed
                break

def get_scan_setup(mmsp):
    http_code, resp = mmsp.send("/mmsp/scanSetup/startChannel/get")
    start_channel = resp['data']
    
    http_code, resp = mmsp.send("/mmsp/scanSetup/stopChannel/get")
    stop_channel = resp['data']
    
    channel_setup = list()
    for chan_no in range(start_channel, stop_channel+1):
        http_code, resp = mmsp.send("/mmsp/scanSetup/%d/get" % chan_no)
        data = resp['data']
        if isinstance(data, list):
            data = data[0]
        enabled = data['enabled'] == "True"
        channelMode  = data['channelMode']
        if enabled and channelMode != "Sweep" and channelMode != "Single":
            channel_setup.append(channelMode)
        elif enabled and channelMode == "Single":
            mass  = data['startMass']
            dwell = data['dwell']
            single = SingleMass(mass, dwell)
            channel_setup.append(single)
        elif enabled and channelMode == "Sweep":
            mass_start  = data['startMass']
            mass_stop  = data['stopMass']
            ppamu  = data['ppamu']
            dwell = data['dwell']
            sweep = MassSweep(mass_start, mass_stop, ppamu, dwell)
            channel_setup.append(sweep)
        else:
            channel_setup.append(None)
    
    return channel_setup
            
        
def get_scan(mmsp, num_of_scans=1):
    if num_of_scans < 0:
        return
    channels_setup = get_scan_setup(mmsp)
    
    channels_setup.insert(0, 'scannum')
    all_scans_values = list()
    for scan_no in range(num_of_scans):
        # -1 corresponds to last scan
        # get scan in range -num_of_scans to -1 
        
        scan_no = scan_no - num_of_scans
        http_code, resp = mmsp.send("/mmsp/measurement/scans/%d/get" % scan_no)
        data = resp['data']
        scan_num      = data['scannum']
        scan_values   = data['values']
        scan_values = [scan_num] +  scan_values
        
        if "Timestamp" in channels_setup:
            index = channels_setup.index("Timestamp")
            #scan_values[index] = datetime.fromtimestamp(scan_values[index])
        
        all_scans_values.append(scan_values)
        
    return channels_setup, all_scans_values

def save_scan(mmsp, num_of_scans=1, filename='massSpec.csv'):
    header, vals = get_scan(mmsp, num_of_scans)
    
    columns = list()
    with open(filename, 'w') as f:
        f.write('# PrismPro Mass Spectrometer')
        for i, channel in enumerate(header):
            i=+1
            if isinstance(channel, str):
                f.write('# Ch%d: %s' % (i, channel) )
                columns.append(channel)
            if isinstance(channel, SingleMass):
                f.write('# Ch%d: Single' % i )
                f.write('#     Mass %.1f' % channel.mass )
                f.write('#     Dwell %d' % channel.dwell )
                
                columns.append(channel.mass)
            elif isinstance(channel, MassSweep):
                f.write('# Ch%d: Sweep' % i )
                f.write('#     Mass Start %.1f' % channel.start )
                f.write('#     Mass Stop %.1f' % channel.stop )
                f.write('#     ppamu %d' % channel.ppamu )
                f.write('#     Dwell %d' % channel.dwell )
                
                masses = np.arange(channel.start, channel.stop + 1/channel.ppamu, 1/channel.ppamu)
                columns += list(masses)
                
        df = pd.DataFrame(data=vals, columns = columns)
        df.set_index('scannum', inplace= True)
        df.to_csv(f)
        return df
    
def plot(df):
    last_scan = df.iloc[-1]
    
    last_scan = last_scan.transpose()
    fig, ax = plt.subplots()
    last_scan[3:].plot(ax =ax)
    plt.savefig('icon-fig.png')
    
    

def main():
    parser = argparse.ArgumentParser(
                        prog = 'Mass Spectrometer Readout')
    parser.add_argument('-n', '--num', type=int)
    parser.add_argument('-s', '--save', action='store_true')
    parser.add_argument('-p', '--plot', action='store_true')
    
    args = parser.parse_args()
    mmsp = MMSP(TARGET_IP_ADDR)
    if args.save:
        df = save_scan(mmsp,args.num)
        
        if args.plot:
            plot(df)


if __name__ == "__main__":
    main()

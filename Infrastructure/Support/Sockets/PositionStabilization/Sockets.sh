#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh

DEV=$SW/Devices/RelayBoards/Quido16-b/Infras_KepcosGalvanics

#BAcha, je to Martinem pojmenovane od 0!

function SocketON() { LogIt "Relay@$DEV $1 goes ON .."; Call $DEV RelayON $1; }
function SocketOFF() { LogIt "Relay@$DEV $1 goes OFF .."; Call $DEV RelayOFF $1; }

#Pripojeni Kepco k Tomakaku a pusteni Flukes
function StabilizationON() { SocketON 12; }

function StabilizationOFF() { SocketOFF 12; }

function KepcosON()
{ for i in `seq 13 16`; do SocketON $i; sleep 2;done; }

function KepcosOFF()
{ for i in `seq 13 16`; do SocketOFF $i; sleep 2;done; }


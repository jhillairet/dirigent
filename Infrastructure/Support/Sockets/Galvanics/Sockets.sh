#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh

DEV=$SW/Devices/RelayBoards/Quido16-b/Infras_KepcosGalvanics

function SocketON() { LogIt "Relay@$DEV $1 goes ON .."; Call $DEV RelayON $1; }
function SocketOFF() { LogIt "Relay@$DEV $1 goes OFF .."; Call $DEV RelayOFF $1; }

#Zasuvka nad tokamakem
function RigolMSO5204-d_ON() { SocketON 3; }

function RigolMSO5204-d_OFF() { SocketOFF 3; }

#ECE radiometer stuff
function RadiometerECEDiagnosticsON() { SocketON 4; }

function RadiometerECEDiagnosticsOFF() { SocketOFF 4; }


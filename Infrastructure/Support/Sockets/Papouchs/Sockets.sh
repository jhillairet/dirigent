#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh

DEV=$SW/Devices/RelayBoards/Quido16-b/Infras_KepcosGalvanics

#BAcha, je to Martinem pojmenovane od 0!

function SocketON() { LogIt "Relay@$DEV $1 goes ON .."; Call $DEV RelayON $1; Relax; }
function SocketOFF() { LogIt "Relay@$DEV $1 goes OFF .."; Call $DEV RelayOFF $1; }

function Papouch-KoON() { SocketON 6; }
function Papouch-KoOFF() { SocketOFF 6; }

function Papouch-ZaON() { SocketON 7; }
function Papouch-ZaOFF() { SocketOFF 7; }

function Papouch-JaON() { SocketON 8; }
function Papouch-JaOFF() { SocketOFF 8; }

function Papouch-StON() { SocketON 9; }
function Papouch-StOFF() { SocketOFF 9; }

function Papouch-JiON() { SocketON 10; }
function Papouch-JiOFF() { SocketOFF 10; }



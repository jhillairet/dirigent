#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh

DEV=Quido8-b/SwBo_TokamakPowGrid

#BAcha, je to Martinem pojmenovane od 0!

function SocketON() { Call $SW/Devices/RelayBoards/$DEV RelayON $1; }
function SocketOFF() { Call $SW/Devices/RelayBoards/$DEV RelayOFF $1; }


function SouthWallON(){ SocketON 1; } #MH F30
function SouthWallOFF(){ SocketOFF 1; } 
function InterferometryON(){ SouthWallON; } 
function InterferometryOFF(){ SouthWallOFF; }

function NorthWestDownON(){ SocketON 2; } #MH F31
function NorthWestDownOFF(){ SocketOFF 2; } 
function MassSpectrometerON(){ NorthWestDownON; } 
function MassSpectrometerOFF(){ NorthWestDownOFF; }

function EastUpON(){ SocketON 3; } #MH F32 Bacha, toto je i severni stena
function EastUpOFF(){ SocketOFF 3; } 
function FG-KepcoON(){ EastUpON; } 
function FG-KepcoOFF(){ EastUpOFF; }

function NorthEastDownON(){ SocketON 4; } 
function NorthEastDownOFF(){ SocketOFF 4; } 
function ManipulatorON(){ NorthEastDownON; } 
function ManipulatorOFF(){ NorthEastDownOFF; }

function SouthWestDownON(){ SocketON 5; } 
function SouthWestDownOFF(){ SocketOFF 5; } 
function FastCamerasON(){ SouthWestDownON; } 
function FastCamerasOFF(){ SouthWestDownOFF; }

function SouthEastDownON(){ SocketON 6; } 
function SouthEastDownOFF(){ SocketOFF 6; } 
function DRPprobeON(){ SouthEastDownON; } 
function DRPprobeOFF(){ SouthEastDownOFF; }

function SouthUpON(){ SocketON 7; } 
function SouthUpOFF(){ SocketOFF 7; } 
function TimepixON(){ SouthUpON; } 
function TimepixOFF(){ SouthUpOFF; }

function NortWestUpON(){ SocketON 8; } 
function NorthWestUpOFF(){ SocketOFF 8; } 


function RPprobeON(){ echo "Missing command to turn on RP";}
function RPprobeOFF(){ echo "Missing command to turn off RP";}


#Development issues
####################
#Dgshm;Call $SW/Infrastructure/Support/PowerGrid/Tokamak  FastCamerasON
#Dgshm;Call $SW/Infrastructure/Support/PowerGrid/Tokamak  InterferometryON

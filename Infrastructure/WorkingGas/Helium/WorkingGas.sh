#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh


RelayBoard=$SW/Devices/RelayBoards/Quido16-a/Racks_Vacuum

function Engage() { Call $RelayBoard RelayON 13; }
function DisEngage() { Call $RelayBoard RelayOFF 13; }

function GasFlowSetup ()
{
local Voltage=$1
    Call $SW/Devices/PowerSupplies/GWInstekPSW-a/Working_Gas GasFlowSetup $Voltage
}

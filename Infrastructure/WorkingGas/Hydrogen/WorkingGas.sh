#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh

DirToDischargeParams=$SHM0/Operation/Discharge


#WG relay:
RelayBoard=$SW/Devices/RelayBoards/Quido16-a/Racks_Vacuum
#WG PowSup for piezoelectric valve:
PowSup4PiezoValve=$SW/Devices/PowerSupplies/GWInstekPSW-a/Working_Gas


function Engage() { Call $RelayBoard RelayON 12; }
function DisEngage() { Call $RelayBoard RelayOFF 12; }


function OpenSession(){
    LogItColor 1 "Uvodni (fake) WG calibration ... "
    cp $SW/Infrastructure/WorkingGas/CalibrationDepot/WG_calibration_table4H2 $SHM/ActualSession/SessionLogBook/WG_calibration_table4H2
}

function SetVoltage@GasValveTo ()
{
local Voltage=$1
    Call  $PowSup4PiezoValve GasFlowSetup "$Voltage"
}

function SetVoltage@GasValveOFF()
{
    SetVoltage@GasValveTo "0"
    DisEngage
}

function GetReadyTheDischarge()
{ 

 

    GeneralTableUpdateAtDischargeBeginning

        p_working_gas_discharge_request=$(<$SHM0/Infrastructure/WorkingGas/Hydrogen/Parameters/p_h)
        p_working_gas_type_request=$(<$SHM0/Infrastructure/WorkingGas/Hydrogen/Parameters/s_gas)
        
    echo $p_working_gas_discharge_request > $DirToDischargeParams/p_working_gas_discharge_request
    LogIt "Going with gas pressure $p_working_gas_discharge_request"
        
    echo $p_working_gas_type_request > $DirToDischargeParams/X_working_gas_discharge_request
    LogIt "Going with gas type $p_working_gas_type_request"
        
    cp $SHML/ActualChamberPressuremPa $DirToDischargeParams/p_chamber_pressure_before_discharge
    UpdateCurrentShotDataBase "p_chamber_pressure_before_discharge=$(< $DirToDischargeParams/p_chamber_pressure_before_discharge)"
    
    
    if [[ $(<$SHM0/Infrastructure/WorkingGas/Hydrogen/Parameters/s_mode) == 'vacuum' ]] 
    then 
        LogIt "Vacuum discharge !"
    else
        echo "Discharge Preparation" >>$SHML/PressureLog
        Engage
        if [[ $(<$SHM0/Infrastructure/WorkingGas/Hydrogen/Parameters/s_mode) == 'p_wg' ]]; then
            ValveVoltage=`python3 -c "from numpy import loadtxt;from scipy.interpolate import interp1d;CalData = loadtxt('/dev/shm/golem/ActualSession/SessionLogBook/WG_calibration_table4H2');Voltage=CalData[:,1];pressure=CalData[:,0];print(interp1d(Voltage,pressure)("$p_working_gas_discharge_request"))"`
        fi
        if [[ $(<$SHM0/Infrastructure/WorkingGas/Hydrogen/Parameters/s_mode) == 'U_v' ]]; then
            ValveVoltage=$(<$SHM0/Infrastructure/WorkingGas/Hydrogen/Parameters/u_v)
        fi
        SetVoltage@GasValveTo $ValveVoltage; sleep 5 
        #echo "From this WG calibr"
        #cat /dev/shm/golem/ActualSession/SessionLogBook/WG_calibration_table4H2
        LogIt "We have got $ValveVoltage V for $p_working_gas_discharge_request mPa request"
        #SetVoltage@GasValveTo 20; sleep 5 # pouzit primy zpusob nastaveni  napeti na ventilu
    fi
    

    
    $LogFunctionPassing;
}

function LogActualChamberPressure
{
    cp $SHML/ActualChamberPressuremPa $DirToDischargeParams/p_chamber_pressure_predischarge
    UpdateCurrentShotDataBase "p_chamber_pressure_predischarge=$(< $DirToDischargeParams/p_chamber_pressure_predischarge)"


}

function WGtest
{
local Voltage=$1
local Relax=$2

    $LogFunctionStart
    Engage
    echo "WGtest start" >>$SHML/PressureLog
    SetVoltage@GasValveTo $Voltage; sleep $Relax
    SetVoltage@GasValveOFF
    echo "WGtest end" >>$SHML/PressureLog
    DisEngage
    $LogFunctionEnd
}


function SecurePostDischargeState()
{
    SetVoltage@GasValveOFF
    DisEngage
    
    if [[ $(<$SHM0/Infrastructure/WorkingGas/Hydrogen/Parameters/s_mode) == 'p_wg' ]]; then
        LogIt "WG resumé:"
        LogIt "Request: $(<$SHM0/Infrastructure/WorkingGas/Hydrogen/Parameters/p_h)"
        LogIt "Achievement: $(<$SHM0/Operation/Discharge/p_chamber_pressure_predischarge)"
        Difference=`echo "($(<$SHM0/Infrastructure/WorkingGas/Hydrogen/Parameters/p_h)-$(<$SHM0/Operation/Discharge/p_chamber_pressure_predischarge)+0.5)/1"|bc|tr -d -`;
        LogIt "Difference:$Difference" 
        
        for i in 10 5 2; do 
            if [[ $Difference -gt $i ]]; 
            then 
                if [[ $(<$SHM0/Operation/Dirigent/HigherPower/Parameters/wg_voice) != 'off' ]]; 
                then 
                    Speaker Problems/The-difference-in-WG-over-"$i"-mPa;
                fi
                LogIt "WG Difference over "$i" mPa"
                return
            fi
        done
        LogIt "WG Difference OK"
    fi
    
}


function H2Calibration()
{
local LowerLimit=$1
local Step=$2
local UpperLimit=$3
local RelaxTime=$4

    $psql_password;psql -c "INSERT into Infrastructure.chamber (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('H2 valve calibration:do','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
    rm -f $SHML/WG_calibration_table4H2
    rm -f $SHML/WG_calibration_table4H2
    SetVoltage@GasValveTo "0";
    sleep $RelaxTime
    Engage
    #for i in `seq 15 0.4 29`; do
    for i in `seq $LowerLimit $Step $UpperLimit|sed 's/,/\./g'`; do
    #for i in `seq 27 0.2 34`; do 
        SetVoltage@GasValveTo "$i";
        sleep $RelaxTime
        echo Calibrating at $i V \($LowerLimit $Step $UpperLimit\): `cat $SHML/ActualChamberPressuremPa` mPa
        echo $i "  "  `cat $SHML/ActualChamberPressuremPa`  >> $SHML/WG_calibration_table4H2
        SetVoltage@GasValveTo "0";
        sleep $RelaxTime
    done    
    cp $SHML/WG_calibration_table4H2 /golem/Dirigent/Infrastructure/WorkingGas/CalibrationDepot/WG_calibration_table4H2_`date "+%d%m%y-%H:%M:%S"`
    SetVoltage@GasValveOFF
    Relax
    DisEngage

}



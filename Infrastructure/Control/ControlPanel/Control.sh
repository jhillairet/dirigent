#!/bin/bash
#SHM="/dev/shm/golem"
#mkdir -p $SHM
#cp Dirigent.sh Commons.sh $SHM/

BasePath=../../..;source $BasePath/Commons.sh

xtermpars='-fn "-misc-fixed-medium-r-normal--20-*-*-*-*-*-iso8859-15" +sb -geometry 150x50+620+2 -fg yellow -bg blue '
drigentcommand='cd /golem/Dirigent/;echo Welcome > '$SHM'/nohup.out;nohup bash Dirigent.sh >> $SHM/nohup.out'

whoami="Operation/Session/Controlling"

DeviceList=""

Chamber="ssh -Y golem@Chamber bash --login -c"
Charger="ssh golem@Charger"

#https://wiki.tcl-lang.org/page/Colors+with+Names

function BasicManagementCore ()
{
echo 'option add *font "Helvetica 15";\
wm title . Dirigent@Operator
frame .session;
set setup current.setup;\
set SHM /dev/shm/golem;\
label .syst -text {Session: broadcast} -background blue;\
button .pingcheck -text "ping" -command {exec xterm -title "Ping Check" '$xtermpars' -e "'$drigentcommand' -s p & tail -f $SHM/nohup.out"};\
button .shutdownsession -text "shutdown" -command {exec xterm -title "Shutdown session" '$xtermpars' -e "'$drigentcommand' --session shutdown & tail -f $SHM/nohup.out" };\
button .emergency -text "emergency" -command {exec xterm -title "Emergency" '$xtermpars' -e "'$drigentcommand' -e & tail -f $SHM/nohup.out" } -background red;\
button .sleeponlan -text "sleep" -command {exec xterm -title "Sleep on line devices" '$xtermpars' -e "'$drigentcommand' --sleep & tail -f $SHM/nohup.out" };\
pack  .syst  .pingcheck  .sleeponlan .emergency .shutdownsession -in .session -side left;pack .session;\
;\
frame .vacuum;\
label .lab_vacuum -text {Vacuum:} -background blue;\
button .v_on -text "Pumping START" -command {exec bash -c "'$drigentcommand' --chamber pumping='on'"} -background cyan;\
button .v_off -text "Pumping END" -command {exec bash -c "'$drigentcommand' --chamber pumping='off'"} -background cyan;\
pack .lab_vacuum .v_on .v_off -in .vacuum -side left;\
pack .vacuum;\
;\
frame .chamber_cond_bk;\
label .lab_chamber_cond_bk -text {Chamber cond.:} -background blue;\
label .lab_bk -text {Baking} -background purple;\
button .etoff -text "Et off" -command {exec '$Charger' "source Charger.sh;EtCommutatorOFF" 2>/dev/null}  -background purple;\
button .bak_on -text "ON" -command {exec bash -c "'$drigentcommand' --chamber bon"} -background purple;\
button .bak_off -text "OFF" -command {exec bash -c "'$drigentcommand' --chamber boff"} -background purple;\
frame .chamber_cond_gd;\
label .lab_chamber_cond_gd -text {Chamber cond.:} -background blue;\
label .lab_gd -text {Glow disch} -background orchid;\
button .gd_init -text "Init" -command {exec bash -c "'$drigentcommand' --chamber gdi"} -background orchid;\
button .gdwg_i -text "Start" -command {exec bash -c "'$drigentcommand' --chamber gdwg_i"} -background orchid;\
button .gdwg_o -text "Oper" -command {exec bash -c "'$drigentcommand' --chamber gdwg_o"} -background orchid;\
button .gdw -text "Wait 10" -command {exec xterm -title "Dummy discharge" '$xtermpars' -e "'$drigentcommand' --chamber gdw 10 & tail -f $SHM/nohup.out"} -background orchid;\
button .gdwg_s -text "Stop" -command {exec bash -c "'$drigentcommand' --chamber gds"} -background orchid;\
pack .lab_chamber_cond_bk .lab_bk .etoff .bak_on .bak_off -in .chamber_cond_bk -side left
pack .chamber_cond_bk;\
pack  .lab_chamber_cond_gd .lab_gd .gd_init .gdwg_i .gdwg_o .gdw .gdwg_s -in .chamber_cond_gd -side left;\
pack .chamber_cond_gd;\
;\
frame .wg;\
label .lab_wg -text {Working gas:} -background blue;\
button .wg_cal_h2 -text "Calibration" -command {exec bash -c "'$drigentcommand' --chamber wgcal-def"} -background green;\
button .wg_test_h2 -text "WG H2 test " -command {exec '$Chamber' WGtestH2};\
pack .lab_wg .wg_cal_h2 .wg_test_h2 -in .wg -side left;\
pack .wg;\
;\
frame .discharge;\
label .lab_discharge -text {Discharge:} -background blue;\
button .d_dummy -text "dummy" -command {exec xterm -title "Dummy discharge" '$xtermpars' -e "'$drigentcommand' --dummy & tail -f $SHM/nohup.out"} -background green;\
button .d_modest -text "modest" -command {exec xterm -title "Modest discharge" '$xtermpars' -e "'$drigentcommand' --modest & tail -f $SHM/nohup.out"} -background orange;\
button .d_standard -text "standard" -command {exec xterm -title "Standard discharge" '$xtermpars' -e "'$drigentcommand' --standard & tail -f $SHM/nohup.out"}  -background red;\
button .d_sujb -text "sujb" -command {exec xterm -title "Standardní test dlouhodobé stability" '$xtermpars' -e "'$drigentcommand' --sujb & tail -f $SHM/nohup.out"}  -background red;\
pack .lab_discharge .d_dummy .d_modest .d_sujb .d_standard  -in .discharge -side left;\
pack .discharge;\
frame .production;\
label .lab_edit -text {Production:} -background blue;\
button .prod_nano_this -text "nano this" -command {exec xterm -title "Edit this" '$xtermpars' -e "nano /golem/Dirigent/Infrastructure/Control/ControlPanel/Control.sh"} -background green;\
button .prod_kate_this -text "Kate this" -command {exec kate /golem/Dirigent/Infrastructure/Control/ControlPanel/Control.sh} -background green;\
pack .lab_edit .prod_nano_this .prod_kate_this -in .production -side left;\
pack .production;\
'|wish
}

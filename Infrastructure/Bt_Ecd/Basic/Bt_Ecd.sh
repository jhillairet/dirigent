#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh

DAS="Oscilloscopes/RigolMSO5204-c/Bt_Ecd_Monitor"

DeviceList="ITs/RasPi3-c/Bt_Ecd RelayBoards/Quido8-a/SwBo_VaccGdHV  $DAS"



function SecurePostDischargeState
{
    #due to backwards compatibility:
    cp $SHM0/Infrastructure/Bt_Ecd/Basic/Parameters/t_bt $SHM0/Production/Parameters/TBt
    cp $SHM0/Infrastructure/Bt_Ecd/Basic/Parameters/t_cd $SHM0/Production/Parameters/Tcd
    cp $SHM0/Infrastructure/Bt_Ecd/Basic/Parameters/o_bt $SHM0/Production/Parameters/Bt_orientation
    cp $SHM0/Infrastructure/Bt_Ecd/Basic/Parameters/o_cd $SHM0/Production/Parameters/CD_orientation
}


function PostDischargeAnalysis()
{

Call $SHM0/Devices/$DAS PostDischargeAnalysis

local cesta=$family/$instrument/$setup
 
echo "`cat $SHM0/$cesta/Parameters/WholeParametersDefinition`" >$cesta/command.html
echo "
<tr><td colspan="100%"><b>Bt, Ecd:</b>`cat $SHM0/Production/Parameters/infrastructure.bt_ecd`</td></tr>
<tr><td colspan="50%"><img src="$cesta/name.png"><img src="$cesta/setup.png" width="$iconsize"> <a href="Devices/$DAS/ScreenShot.png"><img src="Devices/$DAS/rawdata.jpg"></a>
<a href=$cesta/ title="Data directory">$diricon</a>" >> include.html
cat $SHM0/Diagnostics/InfrastructureCurrents/Default/include.html >> include.html
echo "</td></tr>" >> include.html

}














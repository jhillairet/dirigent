#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh

RelayBoard=$SW/Devices/RelayBoards/Quido8-a/SwBo_VaccGdHV

function Engage() { Call $RelayBoard RelayON 3; }
function DisEngage() { Call $RelayBoard RelayOFF 3; }


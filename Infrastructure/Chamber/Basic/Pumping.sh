function xtermPumpingON(){
    xterm -fg yellow -bg blue -title "Golem pumping start" -hold -e /bin/bash -l -c "PumpingON"
    }

function PumpingON(){
    if [ -e $SHMS/session_date ]; then
        $LogFunctionStart
        $psql_password;psql -c "INSERT into infrastructure.chamber (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('pumping:start','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
        Call $SW/Devices/Vacuums/RotaryPump/Basic PumpingON
        LogIt "sleep 2 s to run TMPs ...";sleep 2
        Call $SW/Devices/Vacuums/TMP-a/Basic PumpingON
        Call $SW/Devices/Vacuums/TMP-b/Basic PumpingON
        LogIt "sleep 20 s to open all valves ...";sleep 20
        Call $SW/Devices/Vacuums/Valves-a/Basic OpenValves
        Call $SW/Devices/Vacuums/Valves-b/Basic OpenValves
        $LogFunctionEnd
    else
        critical_error "Please, first open a session"
    fi
}


function xtermPumpingOFF(){
    xterm -fg yellow -bg blue -title "Golem pumping end" -hold -e /bin/bash -l -c "PumpingOFF"
    }


function PumpingOFF()	
{ 
    $LogFunctionStart
    $psql_password;psql -c "INSERT into infrastructure.chamber (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('pumping:end','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
    Relax
    #echo "UPDATE chamber_pumping SET end_time='`date +%H:%M:%S`',end_pressure=`cat $SHML/ActualChamberPressuremPa`,end_temperature=`cat $SHML/ActualChamberTemperature`  WHERE id IN(SELECT max(id) FROM chamber_pumping)"|ssh Dg "cat - |psql -q -U golem golem_database"
    LogIt "Closing Valves and disengaging TMPs"
    Call $SW/Devices/Vacuums/Valves-a/Basic CloseValves
    Call $SW/Devices/Vacuums/Valves-b/Basic CloseValves
    Call $SW/Devices/Vacuums/TMP-a/Basic PumpingOFF
    Call $SW/Devices/Vacuums/TMP-b/Basic PumpingOFF
    LogIt "sleep 5 s to stop Rotary pump ...";sleep 5
    Call $SW/Devices/Vacuums/RotaryPump/Basic PumpingOFF
    $LogFunctionEnd
}

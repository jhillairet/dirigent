#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh

DeviceList="Vacuums/RotaryPump/Basic ITs/RasPi4-a/Chamber Interfaces/PapaGo2TC_ETH-a/ChamberTemperature Interfaces/Gnome232-a/VacuumGauge RelayBoards/Quido16-a/Racks_Vacuum RelayBoards/Quido8-a/SwBo_VaccGdHV"


for Service in Baking GlowDischarge Pumping; do
        source $Service.sh 
done

function OpenSession()
{
    Call $SW/Devices/ITs/RasPi4-a/Chamber VacuumLog  
    sleep 2
    UpdateCurrentSessionDataBase "start_chamber_pressure=`cat $SHML/ActualChamberPressurePa`,start_for_vacuum_pressure=`cat $SHML/ActualForVacuumPressurePa`"
    
}

function SecurePostDischargeState()
{
    $LogFunctionGoingThrough
    echo "Discharge end" >>$SHML/PressureLog
    #cp $SHML/ActualChamberPressuremPa $SHM0/$SUBDIR/$ThisDev/final_chamber_pressure
    #echo 25> $SHM0/$SUBDIR/$ThisDev/final_chamber_temperature
    UpdateCurrentShotDataBase "p_chamber_pressure_after_discharge=`cat $SHML/ActualChamberPressuremPa`"
}

function PostDischargeAnalysis()
{

    gnuplot  -e "set xdata time;set timefmt '%H:%M:%S';set xtics format '%tH:%tM' time;set xlabel 'Time [h:m]';set ylabel 'chamber pressure p_{ch} [mPa]';set xrange [*:*];set title 'Session chamber logbook';set yrange [*:50];set y2range [0:200];set y2tics 0, 20,200;set y2label'Chamber temperature T_{ch} [^o C]';set ytics nomirror;set terminal jpeg size 1080,700;plot '$SHM/ChamberLog' using 1:2 title 'pressure' with lines axis x1y1,'$SHM/ChamberLog' using 1:3 title 'temperature' with lines axis x1y2" > SessionChamber_p_T_Logbook.jpg

    local cesta=$family/$instrument/$setup
 
echo "
<tr><td colspan="100%"><b>Working gas:</b>`cat $SHM0/Production/Parameters/infrastructure.workinggas`</td></tr>
<tr><td colspan="100%"><a href="$cesta/chamber.html"><img src="$cesta/name.png"><img src="$cesta/setup.png" width="$iconsize"></a><a href="$cesta/SessionChamber_p_T_Logbook.jpg"><img src="$cesta/SessionChamber_p_T_Logbook.jpg" width="$iconsize"></a></td></tr>
" >> include.html

echo "<html>
  <head>
    <title></title>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <style></style>
  </head>
<body>
<h1>Tokamak GOLEM chamber status</h1>
<img src="SessionChamber_p_T_Logbook.jpg">
</body>
</html>" >chamber.html
  


}






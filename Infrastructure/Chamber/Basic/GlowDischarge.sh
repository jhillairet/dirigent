function GlowDischInitiate()
{
    $LogFunctionPassing;
    Call $SW/Devices/Vacuums/TMP-a/Basic StandbyON
    Call $SW/Devices/Vacuums/TMP-b/Basic StandbyON
    Call $SW/Infrastructure/Support/PowerGrid/GlowDischargePowSup Engage
    #Call $SW/Devices/PowerSupplies/GWInstekGPR-a/GlowDischarge PowerON
    Call $SW/Devices/Vacuums/Valves-a/Basic CloseValves
    Call $SW/Infrastructure/WorkingGas/Helium Engage
    Call $SW/Devices/RelayBoards/Quido16-a/Racks_Vacuum GDmeterON
    $psql_password;psql -c "INSERT into infrastructure.chamber (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('glow discharge:init','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
    echo "!!ENGAGE HV PS & Open gas reservoir !!"
    echo './Dirigent.sh --gdfs 65/40 # Set-up flow'
    echo './Dirigent.sh --gds # Stop it'
    
}    


function GlowDischGasFlowSetup ()
{
    Voltage=$1
    Call $SW/Infrastructure/WorkingGas/Helium GasFlowSetup $Voltage
}    
    
    

function GlowDischWaitForStop()
{
Time=$1
    $psql_password;psql -c "INSERT into infrastructure.chamber (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('glow discharge:start','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
    echo "PROCES je @ golem@Dirigent>ps -Af|grep golem|grep GlowDischWaitForStop (awkkill pripadne)"
    echo " ============ ============ "
    for i in `seq 1 $Time`; do echo Waiting for GD to stop $i/$Time; sleep 1m;done
    GlowDischStop
    
}    

function GlowDischStop()
{
    $LogFunctionPassing;
    GlowDischGasFlowSetup 0
    sleep 10s
    Call $SW/Infrastructure/Support/PowerGrid/GlowDischargePowSup DisEngage
    #Call $SW/Devices/PowerSupplies/GWInstekGPR-a/GlowDischarge PowerOFF
    Call $SW/Devices/Vacuums/TMP-a/Basic StandbyOFF
    Call $SW/Devices/Vacuums/TMP-b/Basic StandbyOFF
    sleep 5s
    Call $SW/Devices/Vacuums/Valves-a/Basic OpenValves
    Call $SW/Infrastructure/WorkingGas/Helium DisEngage
    Call $SW/Devices/RelayBoards/Quido16-a/Racks_Vacuum GDmeterOFF
    echo "!!Close He reservoir !!"
    $psql_password;psql -c "INSERT into infrastructure.chamber (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('glow discharge:end','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
}  

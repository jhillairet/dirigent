BasePath=../../..;source $BasePath/Commons.sh

#whoami="Analysis/PlasmaParameters/OndGrov/PlasmaParameters"


notebook=PlasmaParameters.ipynb

function PostDischargeAnalysis() 
{
    if [ -e $BasePath/Production/Tags/b_plasma-NO ]; then return;fi
    #export SHOT_NO=`cat ../../shot_no` #linux only
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" $notebook
    mkdir Results
    jupyter-nbconvert  --ExecutePreprocessor.timeout=30 --to html --execute $notebook --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)

    convert -resize $icon_size icon-fig.png graph.png
    
    GenerateAnalysisWWWs $whoami $notebook # @Commons.sh



}


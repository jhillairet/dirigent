<h2>&nbsp;&nbsp;&nbsp;&nbsp;
<a href='Operation/'>Technological parameters </a>
<!--<a href="<?php echo $gitlabpath; ?>/Devices/RASPs"><?php echo $gitlabicon; ?></a>  -->
</h2>

<ul>
    <?php 
    echo "<li><a href='Operation/Discharge/'>Working Gas</a>: ";
    printf_href_express_quantity('Operation/Discharge','p_chamber_pressure_before_discharge','%3.2f', 'operation.discharges');
    echo "; ";
    printf_href_express_quantity('Operation/Discharge','p_chamber_pressure_predischarge','%3.2f', 'operation.discharges');
    echo " (";
    printf_href_express_quantity('Operation/Discharge','p_working_gas_discharge_request','%3.0f', 'operation.discharges');
    echo "@";
    printf_href_express_quantity('Operation/Discharge','X_working_gas_discharge_request','%s', 'operation.discharges');
    echo ")";
    echo "</li>";
    echo "<li><a href='Operation/Discharge/'>Toroidal magnetic field</a>: ";
    printf_href_express_quantity('Operation/Discharge','U_bt_discharge_request','%3.0f', 'operation.discharges');
    echo "@";
    printf_href_express_quantity('Operation/Discharge','t_bt_discharge_request','%3.1f', 'operation.discharges');
    echo "</li>";
    echo "<li><a href='Operation/Discharge/'>Current drive field</a>: ";
    printf_href_express_quantity('Operation/Discharge','U_cd_discharge_request','%3.0f', 'operation.discharges');
    echo "@";
    printf_href_express_quantity('Operation/Discharge','t_cd_discharge_request','%3.1f', 'operation.discharges');
    echo "</li>";
//     parameters_quantity_item ('preionization_request', 'operation.discharges');
        ?>
    </ul>

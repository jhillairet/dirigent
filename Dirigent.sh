#!/bin/bash
BasePath="."

SHM="/dev/shm/golem"
#SW_dir=`pwd`;export SW_dir

function Prerequisities()
{
    mkdir -p $SHM $SHM/Production $SHM/Tags    
    hostname > $SHM/Production/hostname # migration to SuperMicro
    cp Dirigent.sh Commons.sh $SHM/
    #if [ -e /dev/shm/golem/session.setup ]; 
    #then 
        # setup aktualizace ...
        cp session.setup $SHM/
        for i in $(grep include session.setup|grep -v '#'|sed 's/source //g');do cp Setups/$i $SHM/;cp Setups/$i .;done
        cp `realpath session.setup` $SHM/
    #fi
        echo `realpath session.setup |sed 's/\/golem\/Dirigent\/Setups\///g;s/\.setup//g'` > $SHM/session_setup_fullname
        echo `realpath session.setup |xargs basename -s .setup` > $SHM/session_setup_name
        

    source $BasePath/Commons.sh
    echo $((`CurrentShotDataBaseQuerry "shot_no"`)) > $SHM/shot_no 
    
    Orchestra='Operation Infrastructure Diagnostics_Basic Diagnostics_OnStage Diagnostics_OffStage Analysis'


    
    AllInstruments="";AllDevices=""
for Family in $Orchestra;
do 
    for Instrument in ${!Family};do
        export $Family="${!Family}"
        AllInstruments="$AllInstruments ${Family%_*}/$Instrument"
        cd  $SW/${Family%_*}/$Instrument/; source `dirname $Instrument`.sh
        for Dev in $DeviceList;do
            AllDevices="$AllDevices $Dev"
        done
    done
done




cd $SW


#make lists unique:
AllDevices=$(echo $AllDevices | tr ' ' '\n' | awk '!x[$0]++') #remove duplicate rows with awk
AllInstruments=$(echo $AllInstruments | tr ' ' '\n' | awk '!x[$0]++')

export Orchestra="$Orchestra"; echo $Orchestra > $SHM/Production/Orchestra
export Ensemble="$Orchestra Devices";echo $Ensemble > $SHM/Production/Ensemble
export Devices="$AllDevices";echo $AllDevices > $SHM/Production/AllDevices
export AllInstruments="$AllInstruments";echo $AllInstruments > $SHM/Production/AllInstruments
instrument=Dirigent;
    


    }


Prerequisities

if (( $VerboseFlag )); then 
LogIt "All instruments:"
LogIt "=============================="
LogIt $'\n'"$AllInstruments"
LogIt "All devices:"
LogIt "=============================="
LogIt $'\n'"$AllDevices"
LogIt "Orchestra:"
LogIt "=============================="
LogIt $'\n'"$Orchestra"
fi
#return
#exit

# SaveCommandLineParams()
#    if [[ $1 == "--discharge" ]]; then 

#    fi


function SandBox()
{
######### Test part #############
        exit
########### End test part ###################
}


#Main command interaction with user, here it starts
#============================================

TASK=$1
#COMMANDLINE=`echo $@|sed 's/-r //g'`
# ToDo replace special chars: echo '!@#[]$%^&*()-' | sed 's/[]!@#[$%^&*()-]/\\&/g' ..... \!\@\#\[\]\$\%\^\&\*\(\)\-


case "$TASK" in
   "") 
      echo "Usage:
      *** Hotkeys:
      -e                        :Emergency
      -pV  <<Voltage>>          :GasValveTo \$Voltage 
      -sdc                      :Sleep-downCall
      *** Standard:
      ============ Session issues ============
      --session|-s SetupSetup <<PathToSetup>>       :ln -s \$FromRootPathToSetup session.setup
      --session|-s ping|p                 :PingAllDevices
      --session|-s wake|w                 :Broadcast Devices Wake-upCall
      --session|-s sleep|sdc              :Broadcast Devices Sleep-downCall
      --session|-s open|o                 :Call Operation/Session/Basic SetupSession
      --session|-s os@d                   :Broadcast Devices OpenSession
      --session|-s reset|r                :Call Operation/Session/Basic SetupSession
      --session|-s shutdown|s             :Broadcast Devices CloseSession
      ============ Discharge issues ============
      --discharge|-d <<Parameters>>                   :make the discharge with all possible Parameters
      --discharge|-d dummy|d              :bash Operation/Discharge/Basic/Styles/dummyCMD.html
      --discharge|-d modes|m              :bash Operation/Discharge/Basic/Styles/modestCMD.html
      --discharge|-d standard|s           :bash Operation/Discharge/Basic/Styles/standardCMD.html
      --discharge|-d high|h               :bash Operation/Discharge/Basic/Styles/highCMD.html      
      --discharge|-d vacuum|v             :bash Operation/Discharge/Basic/Styles/vacuumCMD.html      
      --discharge|-d sujb|dds             :bash Operation/Discharge/Basic/Styles/sujbCMD.html      
      --discharge|-d fullsteam|fs         :bash Operation/Discharge/Basic/Styles/fullsteamCMD.html
      ============ Chamber issues ============
      --chamber|-ch pumping='on'|pon      :Call Chamber/Basic PumpingON
      --chamber|-ch pumping='off'|poff    :Call Chamber/Basic PumpingOFF
      --chamber|-ch baking='on'|bon       :Call Chamber/Basic Baking_ON <<TempLimit>> <<PressLimit>>, eg. Dg -ch bon 200 20
      --chamber|-ch bon-def               :Call Chamber/Basic Baking_ON (200 5)
      --chamber|-ch baking='off'|boff     :Call Chamber/Basic Baking_OFF
      --chamber|-ch wgsv <<Voltage>>      :SetVoltage@GasValveTo \$Voltage @ChamberRASP
      --chamber|-ch wgcal <<U_LowerLimit>> <<U_Step>> <<U_UpperLimit>> <<t_Relax>>, e.g. Dg --ch wgcal 16 0.25 22 25
      --chamber|-ch wgcal-def             :(default) ~ Dg -ch wgcal 16 0.5 22 25
      --chamber|-ch gdi                   :Call Chamber/Basic GlowDischInitiate
      --chamber|-ch gdwg_i                :Call Chamber/Basic GlowDischGasFlowSetup START
      --chamber|-ch gdwg_o                :Call Chamber/Basic GlowDischGasFlowSetup OPERATE
      --chamber|-ch gdfs <<Voltage2Valve>>:Call Chamber/Basic GlowDischGasFlowSetup \$Voltage2Valve
      --chamber|-ch gds                   :Call Chamber/Basic GlowDischStop
      --chamber|-ch gdw <<Time>>          :Call Chamber/Basic GlowDischWaitForStop \$Time
      ============ Retro actions ============
      --retro|-r WWWsReconstruction|wr <<ShotNo>>        : www for the \$ShotNo reconstruction;; e.g. Dg -r wr 41370
      --retro|-r PostDischargeAnalysis|pda <<ShotNo>> <<WHAT>>   : PostDischargeAnalysis for \$What@\$ShotNo; e.g. Dg -r pda 41370 Diagnostics/BasicDiagnostics/DetectPlasma/
      ============ Tools ============
      --tools|-t goto|g <<ShotNo>>        : cd /golem/database/operation/shots/\$ShotNo; e.g. Dg -t g 41338
      --tools|-t gotolastshot|gls        : cd /golem/database/operation/shots/<<LastShot>>
      --tools|-t gotointhousands|git <<XXShotNo>>        : cd /golem/database/operation/shots/((shot_no/1000*1000))+\$ShotNo; e.g. Dg -t git 338
      --tools|-t call|c <<FullPathToScript>> <<Function>>: e.g. Dg -t c /golem/database/operation/shots/41370/Diagnostics/BasicDiagnostics/DetectPlasma GetReadyTheDischarge
      --tools|-t trigger|t                             :Make a trigger
      --tools|-t backup|b                              : Backup
      --tools|-t raspsreboot|rr         :Reboot rasps
      --broadcast|-b opensession|os     :Broadcast Devices OpenSession
      --broadcast|-b emergency|e        :Broadcast Ensemble SecurePostDischargeState
      "
      RETVAL=1
      ;;
      -e)
        Broadcast "$Ensemble" SecurePostDischargeState
      ;; 
      -pV)
      Voltage=$2
      Call $SW/Infrastructure/WorkingGas/Hydrogen SetVoltage@GasValveTo $Voltage
      ;; 
      -sdc)
        (Broadcast Devices Sleep-downCall)
      ;; 

      --tools|-t)
      case "$2" in 
        raspsreboot|rr)
            ssh golem@Chamber "sudo /sbin/reboot";
            ssh golem@Charger "sudo /sbin/reboot";
            ping Chamber
        ;;
        goto|g)
            cd /golem/database/operation/shots/$3;pwd
        ;;

        gotointhousands|git)
            cd /golem/database/operation/shots/$(($(</golem/shots/0/shot_no)/1000*1000+$3));pwd
        ;;
        gotolastshot|gls)
            cd /golem/database/operation/shots/$(($(</golem/shots/0/shot_no)));pwd
        ;;
        call|c)
            Call $3 $4  
        ;;
        trigger|t) #basic trigger test
            Call $SW/Infrastructure/Triggering/Basic TriggerTest
        ;;
        rs) 
        source Commons.sh ;rsyncRASPs 
        ;;
        backup|b) #backup
            $psql_password;pg_dump golem_database > golem_database.sql; 
            rsync -r -u -v -K -e ssh --exclude '.git' $PWD svoboda@bn:backup/Dirigent/`date "+%y%m%d"`
            zip golem_database golem_database.sql;mpack -s "GM database `date`" golem_database.zip tokamakgolem@gmail.com;
            rm golem_database.*
      ;;
      esac
      return
      ;;
      --session|-s)
      case "$2" in 
        SetupSetup)
        cd /golem/Dirigent/
        rm session.setup;ln -s $3 session.setup
        ;;
      ping|p)
        (for i in $(seq 1 4); do echo Ping $i/4; if PingAllDevices; then return; fi;done)
        #(PingAllDevices) #@Commons
        ;;
      ping_exit)
        (for i in $(seq 1 8); do echo Ping $i/8; if PingAllDevices; then exit; fi;done)
        ;;
      wake|w)
        (Broadcast Devices Wake-upCall)
        ;;
      sleep|sdc)
        (Broadcast Devices Sleep-downCall)
        ;;
      reset|r)
        Call $SW/Operation/Session/Basic ResetSession
        ;;
      open|o)
        Call $SW/Operation/Session/Basic SetupSession
        ;;
      os@d) 
        (Broadcast Devices OpenSession)
      ;;

      shutdown|s)
      ./Dirigent.sh --chamber pumping='off'
      Broadcast Devices Sleep-downCall
      Call $SW/Operation/Session/Basic ShutDown
      ;;

      esac
      ;;
      --chamber|-ch)
      case "$2" in 
        pumping='on')
        xterm -fg yellow -bg blue -title "Golem pumping start" -e "source Commons.sh;Call Infrastructure/Chamber/Basic PumpingON"
        echo pumpingON
        ;;
        pumping='off')
        xterm -fg yellow -bg blue -title "Golem pumping end" -e "source Commons.sh;Call Infrastructure/Chamber/Basic PumpingOFF" 
        ;;
        baking-def='on'|bon-def) #baking ON
        xterm -fg yellow -bg blue -title "Baking status" -hold -e "cd $SW/Infrastructure/Chamber/Basic/; source Chamber.sh; Baking_ON 200 15" #Final Temperature Pressure
        ;;
        baking='on'|bon) #baking ON
        TempLimit=${3:-200};PressLimit=${4:-15};
        xterm -fg yellow -bg blue -title "Baking status" -hold -e "cd $SW/Infrastructure/Chamber/Basic/; source Chamber.sh; Baking_ON $TempLimit $PressLimit" #Final Temperature Pressure
        ;;
        baking='off'|boff) #baking OFF
        Call $SW/Infrastructure/Chamber/Basic Baking_OFF
        ;;
        wgsv)
        Voltage=$3
        Call $SW/Infrastructure/WorkingGas/Hydrogen SetVoltage@GasValveTo $Voltage
        ;;
        wg0)
        ssh Chamber "source Rasp-WorkingGas.sh ;GasH2OFF;GasHeSetVoltage@GasValveTo $3";
        ;;
        wgcal) #With Lower $1 and Uppper $2 limits 
        # e.g. ./Dirigent.sh --wgcal 16 0.5 24 25
            LowerLimit=${3:-16.5};Step=${4:-0.25};UpperLimit=${5:-21};RelaxTime=${6:-15}
            xterm -fg yellow -bg blue -title "Golem WG calibration" -e "source Commons.sh;
            Call $SW/Infrastructure/WorkingGas/Hydrogen H2Calibration $LowerLimit $Step $UpperLimit $RelaxTime"
        ;;
        wgcal-def) 
            xterm -fg yellow -bg blue -title "Golem WG calibration" -e "source Commons.sh;
            Call $SW/Infrastructure/WorkingGas/Hydrogen H2Calibration 16 0.5 22 25"
        ;;


        # ============ Glow discharge issues START ============
        gdi) #Glow discharge init
        Call $SW/Infrastructure/Chamber/Basic GlowDischInitiate
        ;;
        gds) #Glow discharge stop 
        Call $SW/Infrastructure/Chamber/Basic GlowDischStop
        ;;
        gdwg_i) #Glow discharge init 
        Call $SW/Infrastructure/Chamber/Basic GlowDischGasFlowSetup $(<$SW/Infrastructure/Chamber/Basic/Parameters/U_GlowDisch_StartPressure)
        ;;
        gdwg_o) #Glow discharge operate
        Call $SW/Infrastructure/Chamber/Basic GlowDischGasFlowSetup $(<$SW/Infrastructure/Chamber/Basic/Parameters/U_GlowDisch_OperationPressure)
        ;;
        gdfs) 
        Voltage=$3
        Call $SW/Infrastructure/Chamber/Basic GlowDischGasFlowSetup $Voltage
        ;;
        gdw) 
        Time=$3 #[min]
        echo "PROCES je @ golem@Dirigent>ps -Af|grep golem|grep GlowDischWaitForStop (awkkill pripadne)"
        Call $SW/Infrastructure/Chamber/Basic GlowDischWaitForStop $Time
        ;;
      # ============ Glow discharge END ============
      esac
      ;;
      --retro|-r)
      case "$2" in
        WWWsReconstruction|wr)
        bash -c "
        cd /golem/database/operation/shots/$3/Analysis/Homepage/Basic;\
        zip `date '+%d%m%y-%H%M'`_WWWsReconstruction * -x *.zip;\
        cp -r $SW/Analysis/Homepage/Basic/* .;\
        source Homepage.sh; MakeProgressingPage Finalization";
        ;;
        PostDischargeAnalysis|pda)
        ShotNo=$3;What=$4
        bash -c "\
        cd /golem/database/operation/shots/$ShotNo/$What;\
        zip `date '+%d%m%y-%H%M'`_PostDischargeReconstruction * -x *.zip;\
        cp -r $SW/$What/* .;\
        source $(echo $What|xargs dirname|xargs basename).sh; PostDischargeAnalysis;
        ls -all;pwd";
        ;;
      esac
      ;;
      --broadcast|-b)
      case "$2" in
      opensession|os) 
        (Broadcast Devices OpenSession)
      ;;
      emergency|e)
        Broadcast "$Ensemble" SecurePostDischargeState
      ;; 
      esac
      ;;
      # ============ Tests START ============
      --discharge|-d)
      case "$2" in 
        style='dummy'|dummy|d)
        echo 'dummy' > $SHM/Production/style
        bash Operation/Discharge/Basic/Styles/dummyCMD.html
        return
        ;;
        style='modest'|modest|m)
        pwd
        bash Operation/Discharge/Basic/Styles/modestCMD.html
        return
        ;;
        style='standard'|standard|s)
        bash Operation/Discharge/Basic/Styles/standardCMD.html
        return
        ;;
        style='sujb'|sujb|dds)
        bash Operation/Discharge/Basic/Styles/sujbCMD.html
        return
        ;;

      esac   
        rm -rf  $SHM0 $SHMCLP/* 
        # Necessary Inicialization:
        mkdir -p $SHM0/Operation/Discharge/Basic $SHMCLP $SHM0TAGS
        cp Dirigent.sh Commons.sh $SHM0/ 
        cp $SW/Operation/Discharge/Basic/Discharge.sh $SHM0/Operation/Discharge/Basic/ 
        mv $SHM/Tags/* $SHM0TAGS/ 2>/dev/null
        cd $SHM0


        args=("$@")
        
        CL='./Dirigent.sh --discharge'
        #CL='./Dirigent.sh'

        while [ $# -gt 0 ]; do
        if [[ $1 == *"--"* ]]; then
            v="${1/--/}"
            if [ $v == "comment" ] || [[ "$v" =~ "diagnostics." ]] || [[ "$v" =~ "infrastructure." ]] || [ $v == "ScanDefinition" ] || [[ "$v" =~ "operation." ]]; then
                CL="$CL --$v \"$2\"";
                echo $2 > $SHMCLP/$v
            elif [ $v == "discharge" ]; then
                :
            else
                declare $v="$2"
                CL="$CL --$v $2"
                echo $2 > $SHMCLP/$v
            fi
        fi
    shift
    done
    echo "$CL" > $SHMCLP/CommandLine

    set -- $args
        Call $SHM0/Operation/Discharge/Basic Discharge
    ;;
          --dischange|--dc)
        rm -rf  $SHM0 
        # Necessary Inicialization:
        mkdir -p $SHM0/Operation/Discharge/Basic $SHMCLP $SHM0TAGS
        cp Dirigent.sh Commons.sh $SHM0/ 
        cp $SW/Operation/Discharge/Basic/Discharge.sh $SHM0/Operation/Discharge/Basic/ 
        mv $SHM/Tags/* $SHM0TAGS/ 2>/dev/null

        args=("$@")
        
        CL='./Dirigent.sh --dischange'
        #CL='./Dirigent.sh'

        while [ $# -gt 0 ]; do
        if [[ $1 == *"--"* ]]; then
            v="${1/--/}"
            declare $v="$2"
            echo $2 > $SHMCLP/$v
    #       echo $v=$2
            if [ $v == "comment" ] || [[ "$v" =~ "diagnostics." ]] || [[ "$v" =~ "infrastructure." ]] || [ $v == "ScanDefinition" ] || [[ "$v" =~ "operation." ]]; then
                CL="$CL --$v \"$2\"";
            elif [ $v == "dischange" ]; then
            echo ;
            else
            CL="$CL --$v $2"
            fi
        fi
    shift
    done
    echo "$CL" > $SHMCLP/CommandLine

    set -- $args
        Call $SW/Operation/Discharge/Basic Discharge
    
#      ;;
esac


#Tuning ...
